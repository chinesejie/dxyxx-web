package org.shiro.demo.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.shiro.demo.entity.Administrator;
import org.shiro.demo.entity.ResponseResult;
import org.shiro.demo.service.IAdminService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 
 * 应该会作废
 * 
 * @author Administrator
 *
 */
@Controller
@RequestMapping(value = "/adminInfo")
public class AdminInfoController {

	@Resource
	private IAdminService adminService;

	/***
	 * 获取当前登录管理员的信息.
	 */
	@RequestMapping(value = "/getInfo")
	@ResponseBody
	@RequiresAuthentication
	public Administrator getInfo(HttpServletRequest request) {
		// Administrator administrator = (Administrator)
		// request.getSession().getAttribute("administrator");
		org.apache.shiro.subject.Subject currentUser = SecurityUtils.getSubject();
		Object user = currentUser.getPrincipal();
		if (user instanceof Administrator) {

			return adminService.getByPhone(((Administrator) user).getPhone());
		} else {
			return null;
		}
	}

	/***
	 * 
	 * 修改 自己的信息
	 * 
	 * @throws ParseException
	 */
	@RequestMapping(value = "/updateInfo")
	@ResponseBody
	@RequiresAuthentication
	public Object updateInfo(HttpServletRequest request, @RequestParam(value = "id") long id, @RequestParam(value = "name") String name, @RequestParam(value = "sex") String sex, @RequestParam(value = "birthday") String birthday, @RequestParam(value = "job") String job, @RequestParam(value = "image") String image, @RequestParam(value = "phone") String phone, @RequestParam(value = "address") String address, @RequestParam(value = "income") String income) throws ParseException {
		ResponseResult rr = new ResponseResult();
		try {
			org.apache.shiro.subject.Subject currentUser = SecurityUtils.getSubject();
			Administrator user = (Administrator) currentUser.getPrincipal();

			// 再从数据库 查一遍
			Administrator persistent = adminService.getByPhone(user.getPhone());

			SimpleDateFormat sim = new SimpleDateFormat("yyyy-MM-dd");
			Date dateBirthday = sim.parse(birthday);
			// set new property
			persistent.setName(name);
			persistent.setSex(sex);
			persistent.setBirthday(dateBirthday);
			persistent.setJob(job);
			persistent.setImage(image);
			// persistent.setPhone(phone);
			persistent.setIncome(income);
			persistent.setAddress(address);
			System.out.print(birthday);

			// update
			adminService.update(persistent);
		} catch (Exception e) {
			e.printStackTrace();
			rr.setCode(0);
			return rr;
		}
		rr.setCode(1);
		rr.setInfo("success");
		rr.setObject(null);
		return rr;
	}

}
