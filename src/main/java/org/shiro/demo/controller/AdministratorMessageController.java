package org.shiro.demo.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.shiro.demo.entity.AdminitratorMessage;
import org.shiro.demo.entity.ResponseResult;
import org.shiro.demo.service.IBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/admin_message")
public class AdministratorMessageController {
	@Autowired
	private ApplicationContext applicationContext;
	@Resource(name = "baseService")
	private IBaseService baseService;

	@Resource(name = "taskExecutor")
	private TaskExecutor taskExecutor;

	/**
	 * 管理员群发消息
	 * 
	 * @param title
	 * @param content
	 * @param website
	 * @return
	 */
	@RequestMapping(value = "/add")
	@ResponseBody
	@RequiresAuthentication
	@RequiresRoles("super-administrator")
	public Object add(String title, String content, String website) {
		ResponseResult rr = new ResponseResult();
		try {
			// 检测 title 、 content 、website 合法。
			if (!website.startsWith("http://") && !website.startsWith("https://")) {
				rr.setInfo("website不合法");
				return rr;
			}
			AdminitratorMessage adminMessage = new AdminitratorMessage();
			adminMessage.setContent(content);
			adminMessage.setTitle(title);
			adminMessage.setWebsite(website);
			adminMessage.setCreateTime(new Date());
			baseService.save(adminMessage);
			// ① 启动线程池 开始往数据库 加数据，然后 发送ios消息。
			// 得到所有的id
			List<Integer> result = new ArrayList<Integer>();
			String jpql = "Select u.id from User u";
			result = baseService.getByJpql(jpql);
			for (int id : result) {
				PrintTask printTask = (PrintTask) applicationContext.getBean("printTask");
				printTask.setTask(id, adminMessage);
				taskExecutor.execute(printTask);
			}
			// ② 发送ios 消息
			List<String> result2 = new ArrayList<String>();
			String jpql2 = "Select o.token from AppleDeviceToken o";
			result2 = baseService.getByJpql(jpql2);
			OutpushForIOS outpushForIOS = (OutpushForIOS) applicationContext.getBean("outpushForIOS");
			if(adminMessage.getTitle().length()>10) {
                                outpushForIOS.setArgs(adminMessage.getTitle().substring(0,10), 1, result2);
                        }else{
                                outpushForIOS.setArgs(adminMessage.getTitle(), 1, result2);
                        }
			taskExecutor.execute(outpushForIOS); // 测试，，暂时不打开。。TODO
			rr.setObject(adminMessage);
			rr.setInfo("success");
			rr.setCode(1);
			return rr;
		} catch (Exception e) {
			e.printStackTrace();
			return rr;
		}

	}

	// 查询 所有消息
	@RequestMapping(value = "/get")
	@ResponseBody
	@RequiresAuthentication
	@RequiresRoles("super-administrator")
	public Object get() {
		ResponseResult rr = new ResponseResult();
		rr.setObject(baseService.getAll(AdminitratorMessage.class));
		rr.setCode(1);
		rr.setInfo("success");
		return rr;
	}

	// 应该不会用到 此函数，保留
	@RequestMapping(value = "/delete")
	@ResponseBody
	public Object delete(String uuid) {
		return null;
	}

}
