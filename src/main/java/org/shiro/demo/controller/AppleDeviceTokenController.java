package org.shiro.demo.controller;

import java.util.Date;

import javax.annotation.Resource;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.shiro.demo.entity.AppleDeviceToken;
import org.shiro.demo.entity.ResponseResult;
import org.shiro.demo.entity.User;
import org.shiro.demo.service.IBaseService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/appleid")
public class AppleDeviceTokenController {
	@Resource(name = "baseService")
	private IBaseService baseService;

	@RequestMapping(value = "/add")
	@ResponseBody
	@RequiresAuthentication
	@RequiresRoles("customer")
	public Object add( String token) {
		User user = (User) SecurityUtils.getSubject().getPrincipal();
		ResponseResult rr = new ResponseResult();
// 		if (uuid.length() != 40) {
// 			rr.setInfo("uuid不合法");
// 			return rr;
// 		}
		String uuid = token;
		// 查看是否已经存在。
		AppleDeviceToken oldAppleDevicetoken = baseService.getById(AppleDeviceToken.class, uuid);
		if (oldAppleDevicetoken!=null && oldAppleDevicetoken.getToken().equals(token)) {
			rr.setInfo("已经存在");
			return rr;
		}

		AppleDeviceToken appleDevicetoken = new AppleDeviceToken();
 		appleDevicetoken.setUuid(token);
		appleDevicetoken.setToken(token);
		appleDevicetoken.setUserId(user.getId());
		appleDevicetoken.setCreateTime(new Date());
		baseService.update(appleDevicetoken);// 可能是add 可能是update。
		rr.setInfo("success");
		rr.setObject(appleDevicetoken);
		rr.setCode(1);
		return rr;

	}

	// 应该不会用到 此函数，保留
	@RequestMapping(value = "/delete")
	@ResponseBody
	public Object delete(String uuid) {
		return null;
	}

}
