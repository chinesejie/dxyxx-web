package org.shiro.demo.controller;



import org.shiro.demo.entity.ResponseResult;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/b")
public class BController {

	@RequestMapping(value = "/in")
	@ResponseBody
	public Object unauthenticatedException() {
		ResponseResult rr = new ResponseResult();
		rr.setCode(-1);
		rr.setInfo("in");
		rr.setObject(null);
		return rr;

	}

	@RequestMapping(value = "/out")
	@ResponseBody
	public Object unauthorizedException() {
		ResponseResult rr = new ResponseResult();
		rr.setCode(-2);
		rr.setInfo("out");
		rr.setObject(null);
		return rr;

	}

}
