package org.shiro.demo.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.shiro.demo.dao.util.QueryCondition;
import org.shiro.demo.entity.Banner;
import org.shiro.demo.entity.HomePageBanner;
import org.shiro.demo.entity.TopBanner;
import org.shiro.demo.service.IHomePageService;
import org.shiro.demo.service.ITopBannerService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/banner")
public class BannerController {

	@Resource
	private IHomePageService homePageService;
	@Resource
	private ITopBannerService topBannerService;

	/**
	 * 把topbanner跟homepagebanner 打包上去
	 * 
	 * @return
	 */
	@RequestMapping(value = "/get")
	public @ResponseBody Object getObjects() {
		List<QueryCondition> queryConditions = new ArrayList<QueryCondition>();
		queryConditions.add(new QueryCondition("status", QueryCondition.EQ, true));

		List<HomePageBanner> homePageBanners = homePageService.get(HomePageBanner.class, queryConditions);
		List<TopBanner> topBanners = topBannerService.get(TopBanner.class, null);
		Banner banner = new Banner();
		banner.setHomePageBanners(homePageBanners);
		banner.setTopBanners(topBanners);

		return banner;
	}

}
