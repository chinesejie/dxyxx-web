package org.shiro.demo.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.shiro.demo.dao.util.Pagination;
import org.shiro.demo.dao.util.QueryCondition;
import org.shiro.demo.entity.DailySpecial;
import org.shiro.demo.entity.Product;
import org.shiro.demo.entity.ResponseResult;
import org.shiro.demo.service.IDailyService;
import org.shiro.demo.service.IProductService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/dailyspecial")
public class DailySpecialController {

	@Resource(name = "dailyService")
	private IDailyService dailyService;
	@Resource(name = "productService")
	private IProductService productService;

	/**
	 * 可以作废 得到所有的 每日特价
	 * 
	 * @return
	 */
	@RequestMapping(value = "/list")
	public @ResponseBody List<DailySpecial> getAllDailySpecial() {
		List<DailySpecial> dailySpecials = dailyService.getAll(DailySpecial.class);
		return dailySpecials;
	}

	/**
	 * 激活今日特价
	 * 
	 * @param object
	 * @param productId
	 * @return
	 */
	@RequestMapping(value = "/activation")
	@ResponseBody
	@RequiresAuthentication
	@RequiresRoles("specialPrice-administrator")
	public Object ActivationHomeBanner(DailySpecial object, @RequestParam(value = "productId", required = true) long productId) {
		ResponseResult rr = new ResponseResult();
		Product product = productService.getById(Product.class, productId);

		// 判断下 是否已经存在该产品的激活的产品 : 若已经存在则不能激活了
		List<QueryCondition> queryConditions = new ArrayList<QueryCondition>();
		queryConditions.add(new QueryCondition("product", QueryCondition.EQ, product));
		queryConditions.add(new QueryCondition("status", QueryCondition.EQ, true));
		List<DailySpecial> dailySpecials = dailyService.get(DailySpecial.class, queryConditions);

		if (!dailySpecials.isEmpty()) {
			rr.setCode(0);
			return rr;
		}

		object.setProduct(product);
		object.setModifyTime(new Date());

		try {
			dailyService.update(object);
		} catch (Exception e) {
			e.printStackTrace();
			rr.setCode(0);
			return rr;
		}
		rr.setCode(1);
		rr.setInfo("success");
		rr.setObject(null);
		return rr;
	}

	/**
	 * 拿到状态是激活的今日特价
	 * 
	 * @param currentPage
	 * @param pageSize
	 * @param orderBy
	 * @param asc
	 * @param status
	 *            是否激活
	 * @return
	 */
	@RequestMapping(value = "/get")
	public @ResponseBody Pagination<DailySpecial> getToVerify(@RequestParam(value = "cpage") Integer currentPage, @RequestParam(value = "pageSize") Integer pageSize, @RequestParam(value = "orderBy") String orderBy, @RequestParam("asc") Boolean asc, @RequestParam(value = "status") boolean status) {
		QueryCondition queryCondition = new QueryCondition("status", QueryCondition.EQ, status);
		List<QueryCondition> queryConditions = new ArrayList<QueryCondition>();
		queryConditions.add(queryCondition);
		Pagination<DailySpecial> pagination;
		if (orderBy != null && asc != null) {
			pagination = dailyService.getPagination(DailySpecial.class, queryConditions, " order by o." + orderBy + " " + (asc ? " ASC " : " DESC "), currentPage, pageSize);
		} else {
			pagination = dailyService.getPagination(DailySpecial.class, queryConditions, null, currentPage, pageSize);
		}

		return pagination;
	}

	/**
	 * 修改 每日特价
	 * 
	 * @return
	 */
	@RequestMapping(value = "/modify")
	@ResponseBody
	@RequiresAuthentication
	@RequiresRoles("specialPrice-administrator")
	public Object modifyDailySpecialById(DailySpecial object, @RequestParam(value = "productId", required = true) long productId) {

		ResponseResult rr = new ResponseResult();
		Product product = productService.getById(Product.class, productId);

		object.setProduct(product);
		object.setModifyTime(new Date());

		try {
			dailyService.update(object);
		} catch (Exception e) {
			e.printStackTrace();
			rr.setCode(0);
			return rr;
		}
		rr.setCode(1);
		rr.setInfo("success");
		rr.setObject(null);
		return rr;
	}

	/**
	 * 增加 每日特价
	 * 
	 * @return
	 */
	@RequestMapping(value = "/add")
	@RequiresAuthentication
	@RequiresRoles("specialPrice-administrator")
	@ResponseBody
	public Object addDailySpecial(@RequestParam(value = "description", required = true) String description, @RequestParam(value = "imageId", required = true) String imageId, @RequestParam(value = "status", required = true) boolean status, @RequestParam(value = "price", required = true) double price, @RequestParam(value = "productId", required = true) long product_id) {

		ResponseResult rr = new ResponseResult();
		DailySpecial obj = new DailySpecial();
		Product product = productService.getById(Product.class, product_id);

		// 判断是否已经存在一个今日特价的产品
		List<QueryCondition> queryConditions = new ArrayList<QueryCondition>();
		queryConditions.add(new QueryCondition("product", QueryCondition.EQ, product));
		queryConditions.add(new QueryCondition("status", QueryCondition.EQ, true));
		List<DailySpecial> dailySpecials = dailyService.get(DailySpecial.class, queryConditions);
		if (!dailySpecials.isEmpty()) {
			rr.setCode(0);
			return rr;
		}

		obj.setDescription(description);
		obj.setModifyTime(new Date());
		obj.setImageId(imageId);
		obj.setPrice(price);
		obj.setProduct(product);
		obj.setStatus(status);

		try {
			dailyService.save(obj);
		} catch (Exception e) {
			e.printStackTrace();
			rr.setCode(0);
			return rr;
		}
		rr.setCode(1);
		rr.setInfo("success");
		rr.setObject(null);
		return rr;
	}

	/**
	 * 删除 每日特价
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/delete")
	@RequiresAuthentication
	@RequiresRoles("specialPrice-administrator")
	public Object deleteDailySpecial(int id) {

		ResponseResult rr = new ResponseResult();

		try {

			dailyService.delete(DailySpecial.class, id);

		} catch (Exception e) {

			e.printStackTrace();
			rr.setCode(0);
			return rr;
		}
		rr.setCode(1);
		rr.setInfo("success");
		rr.setObject(null);
		return rr;
	}
}
