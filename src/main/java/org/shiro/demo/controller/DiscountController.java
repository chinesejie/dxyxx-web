package org.shiro.demo.controller;

import java.util.List;

import javax.annotation.Resource;

import org.shiro.demo.entity.Discout;
import org.shiro.demo.entity.ResponseResult;
import org.shiro.demo.service.IBaseService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping(value="/discount")
public class DiscountController {
	
	@Resource
	private IBaseService baseService;
	
	@ResponseBody
	@RequestMapping("/list")
	public Object list(){
		ResponseResult rr=new ResponseResult();
		List<Discout> discouts= baseService.getAll(Discout.class);
		
		rr.setObject(discouts);
		return rr;
	}
	
	

}
