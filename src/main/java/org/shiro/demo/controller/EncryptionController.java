package org.shiro.demo.controller;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.shiro.demo.entity.Category;
import org.shiro.demo.entity.ResponseResult;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.utils.AESUtil;

@Controller
@RequestMapping(value = "/encryption")
public class EncryptionController {

	/**
	 * 传入 明文， 输出 密文
	 */
	@RequestMapping(value = "/transfer")
	@ResponseBody
	public Object modifyByid(String plain) {
		ResponseResult rr = new ResponseResult();
		if (StringUtils.isEmpty(plain)) {
			rr.setInfo("明文为空");
			return rr;
		}
		if (plain.length() > 15) {
			rr.setInfo("明文超过15位");
			return rr;
		}
		try {
			String enString = AESUtil.Encrypt(plain);
			rr.setInfo("success");
			rr.setCode(1);
			rr.setObject(enString);
			return rr;
		} catch (Exception e) {
			e.printStackTrace();

			return rr;
		}
	}
}
