package org.shiro.demo.controller;



import org.shiro.demo.entity.ResponseResult;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/error")
public class ErrorController {

	@RequestMapping(value = "/UnauthenticatedException")
	@ResponseBody
	public Object unauthenticatedException() {
		ResponseResult rr = new ResponseResult();
		rr.setCode(-1);
		rr.setInfo("没有登陆");
		rr.setObject(null);
		return rr;

	}

	@RequestMapping(value = "/UnauthorizedException")
	@ResponseBody
	public Object unauthorizedException() {
		ResponseResult rr = new ResponseResult();
		rr.setCode(-2);
		rr.setInfo("没有授权");
		rr.setObject(null);
		return rr;

	}

}
