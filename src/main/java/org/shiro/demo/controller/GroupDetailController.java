package org.shiro.demo.controller;

import javax.annotation.Resource;

import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.shiro.demo.entity.Group;
import org.shiro.demo.entity.GroupDetail;
import org.shiro.demo.entity.ResponseResult;
import org.shiro.demo.service.IGroupDetailService;
import org.shiro.demo.service.IGroupService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/groupdetail")
public class GroupDetailController {

	@Resource(name = "groupDetailService")
	IGroupDetailService groupDetailService;

	@Resource(name = "groupService")
	IGroupService groupService;

	/**
	 * 查询 团详情
	 * 
	 * @param groupID
	 *            团id
	 * @param current
	 * @param count
	 * @return
	 */
	/*
	 * @RequestMapping("/getGroupDetail") public List<GroupDetail>
	 * getAllGroupDetail(int groupID,@RequestParam(value = "current", required =
	 * true) int current, @RequestParam(value = "count", required = true) int
	 * count) {
	 * 
	 * Group group = groupService.getById(Group.class, groupID);
	 * 
	 * if (group != null) { List<QueryCondition> list = new
	 * ArrayList<QueryCondition>(); QueryCondition queryConditions = new
	 * QueryCondition("group_id",QueryCondition.EQ, group.getId());
	 * list.add(queryConditions); Pagination<GroupDetail> pa =
	 * (Pagination<GroupDetail>)
	 * groupDetailService.getPagination(GroupDetail.class,
	 * list,"ORDER BY o.postDate DESC", current, count); return
	 * pa.getRecordList(); } return null; }
	 */

	/**
	 * 修改团详情
	 * 
	 * @param groupDetail
	 *            团详情
	 * @return
	 */
	@RequestMapping("/modifygroupdetail")
	@RequiresAuthentication
	@RequiresRoles("group-chief")
	@ResponseBody
	public Object modifyGroupDetailByGroupDetailId(GroupDetail groupDetail) {

		ResponseResult rr = new ResponseResult();

		try {
			groupDetailService.update(groupDetail);
		} catch (Exception e) {

			e.printStackTrace();
			rr.setCode(0);
			return rr;
		}
		rr.setCode(1);
		rr.setInfo("success");
		rr.setObject(null);
		return rr;
	}

	/**
	 * 删除团详情
	 * 
	 * @param id
	 *            团详情id
	 * @return
	 */
	@RequestMapping("/deletegroupdetail")
	@RequiresAuthentication
	@RequiresRoles("group-chief")
	@ResponseBody
	public Object deleteGroupDetail(int id) {

		ResponseResult rr = new ResponseResult();

		try {

			groupDetailService.delete(GroupDetail.class, id);

		} catch (Exception e) {
			e.printStackTrace();
			rr.setCode(0);
			return rr;
		}
		rr.setCode(1);
		rr.setInfo("success");
		rr.setObject(null);
		return rr;
	}

	/**
	 * 新增 团详情
	 * 
	 * @param groupId
	 *            团id
	 * @param personAmount
	 *            参团的人数
	 * @param personPrice
	 *            每人单价
	 * @return
	 */
	@RequestMapping("/addGroupDetail")
	@RequiresAuthentication
	@RequiresRoles("group-chief")
	@ResponseBody
	public Object addGroupDetail(@RequestParam(value = "group_id", required = true) int groupId, @RequestParam(value = "person_amount", required = true) int personAmount, @RequestParam(value = "person_price", required = true) double personPrice) {
		ResponseResult rr = new ResponseResult();
		GroupDetail groupDetail = new GroupDetail();

		Group group = groupService.getById(Group.class, groupId);

		groupDetail.setGroup(group);
		groupDetail.setPersonAmount(personAmount);
		groupDetail.setPersonPrice(personPrice);

		try {

			groupDetailService.save(group);

		} catch (Exception e) {
			e.printStackTrace();
			rr.setCode(0);
			return rr;
		}
		rr.setCode(1);
		rr.setInfo("success");
		rr.setObject(null);
		return rr;

	}
}
