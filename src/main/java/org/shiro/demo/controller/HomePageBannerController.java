package org.shiro.demo.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.shiro.demo.dao.util.QueryCondition;
import org.shiro.demo.entity.HomePageBanner;
import org.shiro.demo.entity.Product;
import org.shiro.demo.entity.ResponseResult;
import org.shiro.demo.entity.TopBanner;
import org.shiro.demo.service.IHomePageService;
import org.shiro.demo.service.IProductService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/*
 *  homebanner没有删除，只有修改
 *  一个product只有一个homebanner。
 */
@Controller
@RequestMapping(value = "/homepagebanner")
public class HomePageBannerController {

	@Resource(name = "homePageService")
	private IHomePageService homePageService;

	@Resource(name = "productService")
	private IProductService productService;

	/**
	 * 得到首页横幅
	 * 
	 * @return
	 */
	@RequestMapping(value = "/list")
	public @ResponseBody List<HomePageBanner> getAllHomepageBanners() {
		List<HomePageBanner> homePageBanners = homePageService.getAll(HomePageBanner.class);
		return homePageBanners;
	}

	/**
	 * 拿取激活的homebanner
	 * 
	 * @return
	 */
	@RequestMapping(value = "/listActive")
	public @ResponseBody List<HomePageBanner> getActive() {
		List<QueryCondition> queryConditions = new ArrayList<QueryCondition>();
		queryConditions.add(new QueryCondition("status", QueryCondition.EQ, true));
		List<HomePageBanner> homePageBanners = homePageService.get(HomePageBanner.class, queryConditions);
		return homePageBanners;
	}

	/**
	 * 只做激活
	 * 
	 * @param object
	 * @param productId
	 * @return
	 */
	@RequestMapping(value = "/activation")
	@ResponseBody
	@RequiresAuthentication
	@RequiresRoles("homepageBanner-administrator")
	public Object ActivationHomeBanner(HomePageBanner object, @RequestParam(value = "productId", required = true) long productId) {
		ResponseResult rr = new ResponseResult();
		Product product = productService.getById(Product.class, productId);
		
		if(product==null){
			rr.setCode(0);
			rr.setInfo("不存在的产品");
			return rr;
		}

		// 判断下 是否已经存在该产品的激活的homeBanner : 若已经存在则不能激活了
		List<QueryCondition> queryConditions = new ArrayList<QueryCondition>();
		queryConditions.add(new QueryCondition("product", QueryCondition.EQ, product));
		queryConditions.add(new QueryCondition("status", QueryCondition.EQ, true));
		HomePageBanner homePageBanners = (HomePageBanner) homePageService.getSingleResult(HomePageBanner.class, queryConditions);

		
		
		if (homePageBanners!=null) {
			rr.setCode(0);
			rr.setInfo("已经存在一个已经激活的该产品");
			return rr;
		}

		HomePageBanner oHomePageBanner = homePageService.getById(HomePageBanner.class, object.getId());


		if (oHomePageBanner == null) {
			rr.setCode(0);
			rr.setInfo("不存在该homePageBanner");
			return rr;
		}

		if (oHomePageBanner.getStatus() == true || oHomePageBanner.getProduct().getId() != product.getId()) {
			rr.setCode(0);
			rr.setInfo("该homePagebanner已激活，或者该homePageBanner不属于这个产品");
			return rr;
		}

		oHomePageBanner.setModifyTime(new Date());
		oHomePageBanner.setStatus(true);

		try {
			homePageService.update(oHomePageBanner);
		} catch (Exception e) {
			e.printStackTrace();
			rr.setCode(0);
			return rr;
		}
		rr.setCode(1);
		rr.setInfo("success");
		rr.setObject(null);
		return rr;
	}
	
	 	/**
	 	 * 取消激活
	 	 * @param productId
	 	 * @param homePageBannerId
	 	 * @return
	 	 */
		@RequestMapping(value = "/cancelActivation")
		@ResponseBody
		@RequiresAuthentication
		@RequiresRoles("homepageBanner-administrator")
	    public Object cancelActivation(@RequestParam(value="productId")long productId,@RequestParam(value="id")long homePageBannerId){
			ResponseResult rr=new ResponseResult();
			
			
			
			Product product= productService.getById(Product.class, productId);
			
			if(product==null){
				rr.setCode(0);
				rr.setInfo("不存在的产品");
				return rr;
			}
			
			HomePageBanner oHomePageBanner = homePageService.getById(HomePageBanner.class, homePageBannerId);

			if (oHomePageBanner == null) {
				rr.setCode(0);
				rr.setInfo("不存在该homePageBanner");
				return rr;
			}
			
			if (oHomePageBanner.getStatus() == false || oHomePageBanner.getProduct().getId() != product.getId()) {
				rr.setCode(0);
				rr.setInfo("homePagebanner已经是未激活，或者该homePageBanner不属于这个产品");
				return rr;
			}
			
			oHomePageBanner.setStatus(false);
			oHomePageBanner.setModifyTime(new Date());
			
			try {
				homePageService.update(oHomePageBanner);
			} catch (Exception e) {
				e.printStackTrace();
				rr.setCode(0);
				return rr;
			}
			rr.setCode(1);
			rr.setInfo("success");
			rr.setObject("");
			return rr;
		}
	

	/**
	 * 修改首页横幅
	 * 
	 * @return
	 */
	@RequestMapping(value = "/modify")
	@ResponseBody
	@RequiresAuthentication
	@RequiresRoles("homepageBanner-administrator")
	public Object modifyHomepageBannerById(HomePageBanner object, @RequestParam(value = "productId", required = true) long productId) {
		ResponseResult rr = new ResponseResult();
		Product product = productService.getById(Product.class, productId);

		object.setProduct(product);
		object.setModifyTime(new Date());

		try {
			homePageService.update(object);
		} catch (Exception e) {
			e.printStackTrace();
			rr.setCode(0);
			return rr;
		}
		rr.setCode(1);
		rr.setInfo("success");
		rr.setObject(null);
		return rr;
	}

	/**
	 * 增加首页横幅
	 * 
	 * @param description
	 *            横幅描述
	 * @param priority
	 *            优先级 数值越小，优先级越高
	 * @param imageId
	 *            图片id
	 * @param modifyTime
	 *            修改时间
	 * @param status
	 *            状态，是否处于激活状态
	 * @param price
	 *            价格，单价
	 * @param product_id
	 *            产品id
	 * @return
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@ResponseBody
	@RequiresAuthentication
	@RequiresRoles("homepageBanner-administrator")
	public Object addHomePageBanner(@RequestParam(value = "description", required = true) String description, @RequestParam(value = "priority", required = true) int priority, @RequestParam(value = "imageId", required = true) String imageId, @RequestParam(value = "price", required = false) double price, @RequestParam(value = "productId", required = false) long product_id) {

		ResponseResult rr = new ResponseResult();
		HomePageBanner obj = new HomePageBanner();

		Product product = productService.getById(Product.class, product_id);

		// 查询这个货物是否已经存在一个激活的homebanner
		QueryCondition queryCondition = new QueryCondition("product", QueryCondition.EQ, product);
		QueryCondition queryCondition2 = new QueryCondition("status", QueryCondition.EQ, true);
		List<QueryCondition> queryConditions = new ArrayList<QueryCondition>();
		queryConditions.add(queryCondition);
		queryConditions.add(queryCondition2);
		HomePageBanner homebanner = (HomePageBanner) homePageService.getSingleResult(HomePageBanner.class, queryConditions);

		// 判断下增加的货物是否存在
		if (product == null) {
			rr.setCode(0);
			rr.setInfo("产品不存在");
			return rr;
		}

		// 是否存在已经激活的homePageBanner
		if (homebanner != null) {
			rr.setCode(0);
			rr.setInfo("该产品已存在激活的homePageBanner");
			return rr;
		}

		obj.setProduct(product);
		obj.setPrice(price);
		obj.setModifyTime(new Date());
		obj.setPriority(priority);
		obj.setDescription(description);
		obj.setImageId(imageId);
		obj.setStatus(true);

		try {
			homePageService.save(obj);
		} catch (Exception e) {
			e.printStackTrace();
			rr.setCode(0);
			rr.setInfo("无权进行操作");
			return rr;
		}

		rr.setCode(1);
		rr.setInfo("success");
		rr.setObject(null);
		return rr;
	}

	/**
	 * 这个没有使用 删除首页横幅
	 * 
	 * @param id
	 *            首页横幅的id
	 * @return
	 */
	// @RequestMapping(value="/delete")
	@ResponseBody
	@RequiresAuthentication
	@RequiresRoles("homepageBanner-administrator")
	public Object deleteHomePageBanner(int id) {

		ResponseResult rr = new ResponseResult();

		try {
			homePageService.delete(TopBanner.class, id);
		} catch (Exception e) {
			e.printStackTrace();
			rr.setCode(0);
			return rr;
		}
		rr.setCode(1);
		rr.setInfo("success");
		rr.setObject(null);
		return rr;
	}

	/**
	 * 通过homebanner的id去拿homebanner
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/get")
	public @ResponseBody HomePageBanner getByid(@RequestParam(value = "id", required = true) long id) {
		HomePageBanner result = homePageService.getById(HomePageBanner.class, id);
		return result;
	}
}
