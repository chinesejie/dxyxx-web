package org.shiro.demo.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.shiro.demo.dao.util.Pagination;
import org.shiro.demo.dao.util.QueryCondition;
import org.shiro.demo.entity.Administrator;
import org.shiro.demo.entity.NotifyMessage;
import org.shiro.demo.entity.ResponseResult;
import org.shiro.demo.service.IBaseService;
import org.shiro.demo.service.INotifyMessageService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/message")
public class NotifyMessageController {

	@Resource
	private IBaseService baseService;

	@Resource
	private INotifyMessageService notifyMessageService;

	/***
	 * 得到店长 未读消息的数目
	 */
	@RequestMapping(value = "/getNum")
	@ResponseBody
	@RequiresAuthentication
	@RequiresRoles(value = "manager")
	public Object getNum(HttpServletRequest request) {
		ResponseResult rr = new ResponseResult();
		try {
			Administrator admin = (Administrator) SecurityUtils.getSubject().getPrincipal();
			int num = notifyMessageService.getNumByAdmin(admin.getId() + "");// 带有缓存的
			rr.setObject(num);
			rr.setInfo("success");
			rr.setCode(1);
			return rr;
		} catch (Exception e) {
			e.printStackTrace();
			return rr;
		}
	}

	/***
	 * 更新读取状态
	 * 
	 * 记得更新未读消息的 count
	 */
	@RequestMapping(value = "/update")
	@ResponseBody
	@RequiresAuthentication
	@RequiresRoles(value = "manager")
	public Object update(HttpServletRequest request, long mid) {
		ResponseResult rr = new ResponseResult();
		Administrator admin = (Administrator) SecurityUtils.getSubject().getPrincipal();
		NotifyMessage nm = baseService.getById(NotifyMessage.class, mid);
		if (nm == null) {
			rr.setInfo("消息不存在");
			return rr;
		}
		if (!nm.getAdmin_id().equals(admin.getId() + "")) {
			rr.setInfo("消息不属于您");
			return rr;
		}
		if (nm.isHreads()) {
			rr.setInfo("消息已读");
			return rr;
		}
		nm.setHreads(true);
		int num = notifyMessageService.updateByAdmin(admin.getId() + "", nm);// 改变状态，更新缓存
		rr.setInfo("success");
		rr.setObject(num);
		rr.setCode(1);
		return rr;
	}

	/***
	 * 得到 消息 分页列表
	 */
	@RequestMapping(value = "/get")
	@ResponseBody
	@RequiresAuthentication
	@RequiresRoles(value = "manager")
	public Object get(HttpServletRequest request, int cPage, int pageSize) {
		ResponseResult rr = new ResponseResult();
		Administrator admin = (Administrator) SecurityUtils.getSubject().getPrincipal();
		List<QueryCondition> queryConditions = new ArrayList<QueryCondition>();
		queryConditions.add(new QueryCondition("admin_id", QueryCondition.EQ, admin.getId() + ""));
		Pagination<NotifyMessage> nms = baseService.getPagination(NotifyMessage.class, queryConditions, "order by o.createTime desc", cPage, pageSize);
		rr.setObject(nms);
		rr.setInfo("success");
		rr.setCode(1);
		return rr;
	}
}
