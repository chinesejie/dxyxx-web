package org.shiro.demo.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.shiro.demo.dao.util.Pagination;
import org.shiro.demo.dao.util.QueryCondition;
import org.shiro.demo.entity.GroupOrder;
import org.shiro.demo.entity.Order;
import org.shiro.demo.entity.OrderComment;
import org.shiro.demo.entity.ResponseResult;
import org.shiro.demo.entity.Ticket;
import org.shiro.demo.entity.User;
import org.shiro.demo.service.IBaseService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/order_comment")
public class OrderCommentController {
	@Resource(name = "baseService")
	private IBaseService baseService;

	/**
	 * 
	 * 
	 * @param user
	 * @return
	 */
	@RequestMapping(value = "/add")
	@ResponseBody
	@RequiresAuthentication
	@RequiresRoles("customer")
	public Object add(OrderComment orderComment) {

		ResponseResult rr = new ResponseResult();
		if (orderComment.getScore() < 1 || orderComment.getScore() > 5) {
			rr.setCode(0);
			rr.setInfo("订单 评分区间 是[1,5]");
			return rr;
		}
		User currentUser = (User) SecurityUtils.getSubject().getPrincipal();

		String orderId = orderComment.getOrder_id();
		if (!StringUtils.isEmpty(orderId)) {
			List<QueryCondition> queryConditions = new ArrayList<QueryCondition>();
			queryConditions.add(new QueryCondition("uuid", QueryCondition.EQ, orderId));
			boolean wrong = false;
			char c = orderId.charAt(0);
			switch (c) {
			case 'g':// 开团吧
				GroupOrder g = (GroupOrder) baseService.getSingleResult(GroupOrder.class, queryConditions);
				if (!g.getUser().getPhone().equals(currentUser.getPhone())) {
					wrong = true;
				}
				break;
			case 't':// 代金券
				Ticket t = (Ticket) baseService.getSingleResult(Ticket.class, queryConditions);
				if (!t.getUser().getPhone().equals(currentUser.getPhone())) {
					wrong = true;
				}
				break;
			case 'o':// 正常订单
				Order o = (Order) baseService.getSingleResult(Order.class, queryConditions);
				if (!o.getUser().getPhone().equals(currentUser.getPhone())) {
					wrong = true;
				}
				break;
			default:
				wrong = true;
				break;
			}
			if (wrong) {
				rr.setCode(0);
				rr.setInfo("订单不属于您！");
				return rr;
			}
		} else {
			rr.setCode(0);
			rr.setInfo("订单号为空");
			return rr;
		}
		// 查下已经打分了
		List<QueryCondition> list = new ArrayList<QueryCondition>();

		list.add(new QueryCondition("order_id", QueryCondition.EQ, orderId));
		OrderComment orderCommentInDB = (OrderComment) baseService.getSingleResult(OrderComment.class, list);
		if (orderCommentInDB != null) {
			rr.setCode(0);
			rr.setInfo("已经打过分了");
			return rr;
		}

		orderComment.setUser_id(currentUser.getPhone());
		orderComment.setPostTime(new Date());

		try {
			baseService.save(orderComment);
			rr.setCode(1);
			rr.setInfo("success");
			rr.setObject(orderComment);
		} catch (Exception e) {
			e.printStackTrace();
			rr.setCode(0);
			rr.setInfo("系统内部出错");
			rr.setObject(null);
		}
		return rr;
	}

	@RequestMapping(value = "/list")
	public @ResponseBody Object list(@RequestParam(value = "current", required = true) int current, @RequestParam(value = "count", required = true) int count) {
		try {
			String order = "postTime DESC";
			Pagination<OrderComment> pa = (Pagination<OrderComment>) baseService.getPagination(OrderComment.class, null, "ORDER BY o." + order, current, count);
			return new ResponseResult(1, "成功", pa);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseResult(0, "失败", null);
		}
	}

	/**
	 * 根据 order_id 获取评论列表
	 * 
	 * @param product_id
	 * @param current
	 * @param count
	 * @return
	 */
	@RequestMapping(value = "/byUuid")
	public @ResponseBody Object byPid(@RequestParam(value = "uuid", required = true) String uuid) {
		ResponseResult rr = new ResponseResult();
		try {
			List<QueryCondition> list = new ArrayList<QueryCondition>();

			list.add(new QueryCondition("order_id", QueryCondition.EQ, uuid));
			OrderComment orderComment = (OrderComment) baseService.getSingleResult(OrderComment.class, list);
			if (orderComment == null) {
				rr.setCode(2);
				rr.setInfo("待打分");
				return rr;
			}
			rr.setCode(1);
			rr.setInfo("success");
			rr.setObject(orderComment);
			return rr;
		} catch (Exception e) {
			e.printStackTrace();
			rr.setCode(0);
			rr.setInfo("系统有误");
			return rr;
		}
	}
}
