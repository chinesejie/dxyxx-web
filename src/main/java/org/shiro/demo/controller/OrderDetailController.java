package org.shiro.demo.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.shiro.demo.dao.util.Pagination;
import org.shiro.demo.dao.util.QueryCondition;
import org.shiro.demo.entity.OrderDetail;
import org.shiro.demo.entity.ResponseResult;
import org.shiro.demo.service.IOrderDetailService;
import org.shiro.demo.service.IOrderService;

public class OrderDetailController {
	@Resource(name = "orderService")
	private IOrderService orderService;

	@Resource(name = "orderDetailService")
	private IOrderDetailService orderDetailService;

	/**
	 * 根据orderId获取orderDetail的数据.(不对订单进行筛选)
	 * 
	 * @param orderId 订单id
	 * @param currentPage 要获取的页码
	 * @param pageSize 页大小
	 * @return
	 */
	public ResponseResult findByOrder(Long orderId, Integer currentPage, Integer pageSize) {
		List<QueryCondition> queryConditions = new ArrayList<QueryCondition>();
		queryConditions.add(new QueryCondition("order.id", "=", orderId));
		Pagination<OrderDetail> pagination = orderDetailService.getPagination(OrderDetail.class, queryConditions, null, currentPage, pageSize);
		return new ResponseResult(1, "获取orderDetail数据成功", pagination);
	}

}
