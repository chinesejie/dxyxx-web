package org.shiro.demo.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.apache.shiro.subject.Subject;
import org.joda.time.DateTime;
import org.shiro.demo.dao.util.QueryCondition;
import org.shiro.demo.entity.Phone;
import org.shiro.demo.entity.User;
import org.shiro.demo.service.IBaseService;
import org.shiro.demo.service.IUserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/phones")
public class PhonesController {
	@Resource(name = "userService")
	IUserService userService;
	@Resource(name = "baseService")
	private IBaseService baseService;

	private User getCurrentUser() {
		Subject currentAdmin = SecurityUtils.getSubject();
		 
		User user = (User) currentAdmin.getPrincipal();
		return user;
	}

	// 获取 付款码
	@RequestMapping("/SEND_PHONE_FOR_SCAN")
	@ResponseBody
	@RequiresAuthentication
	@RequiresRoles("customer")
	public String SEND_PHONE_FOR_SCAN(HttpServletRequest request) {

		User user = getCurrentUser();
		int user_id = user.getId();

		Random r = new Random();
		String abcdefg = "abcdefghijklmnopqrstuvwxyz";
		String randomLetter = "";
		int num = r.nextInt(abcdefg.length());
		char c = abcdefg.charAt(num);

		String str = String.format("%07d", new Object[] { Integer.valueOf(user_id) });
		str = str.replace("0", String.valueOf(c));
		StringBuilder sb = new StringBuilder();
		sb.append("ebo");
		sb.append(str);

		sb.append((int) (Math.random() * 899.0D + 100.0D));
		Phone phone = baseService.getById(Phone.class, user.getPhone());
		String barcode = sb.toString();
		if (phone != null) {
			// 更新
			phone.setBarcode(barcode);
			phone.setBarcodeTime(new Date());

		} else {
			// 添加
			phone = new Phone();
			phone.setNum(user.getPhone());
			phone.setBarcode(barcode);
			phone.setBarcodeTime(new Date());

		}
		baseService.update(phone);
		return barcode;// 返回字符串
	}

	// 验证 付款码.. 靶枪专用
	@RequestMapping("/Validate_PHONE_FOR_SCAN")
	@ResponseBody
	public String Validate_PHONE_FOR_SCAN(HttpServletRequest request, @RequestParam String id) {
		String phone = "";
		// id 是barcode。 返回手机号 给靶枪
		List<QueryCondition> query = new ArrayList<QueryCondition>();
		query.add(new QueryCondition("barcode", QueryCondition.EQ, id));
		Phone phoneClazz = (Phone) baseService.getSingleResult(Phone.class, query);
		if (phoneClazz != null) {
			Date barcodeTime = phoneClazz.getSmscodeTime();
			DateTime dtInDB = new DateTime(barcodeTime);
			if (dtInDB.plusMinutes(5).compareTo(DateTime.now()) >= 0) {// 验证是否五分钟过期了
				phone = phoneClazz.getNum();
			}
		}
		return phone;
	}

	/**
	 * @deprecated 作废 发验证码
	 * @param request
	 * @param phone
	 * @return
	 */
	@RequestMapping({ "/SEND_SMS_FOR_SCAN" })
	@ResponseBody
	public String SEND_SMS_FOR_SCAN(HttpServletRequest request, @RequestParam String phone) {
		int random = (int) (Math.random() * 10000.0D) + 1000;

		if (true) {
			return "success";
		}
		return "fail:xx";
	}

}