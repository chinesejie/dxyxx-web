package org.shiro.demo.controller;

import javax.annotation.Resource;

import org.shiro.demo.entity.AdminitratorMessage;
import org.shiro.demo.entity.UserMessage;
import org.shiro.demo.service.IBaseService;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
/**
 * 群发消息写入 数据库user_message
 * @author Administrator
 *
 */
@Component
@Scope("prototype")
public class PrintTask implements Runnable {
	@Resource(name = "baseService")
	private IBaseService baseService;
	int userId;
	AdminitratorMessage adminitratorMessage;

	public void setTask(int userId, AdminitratorMessage adminitratorMessage) {
		this.userId = userId;
		this.adminitratorMessage = adminitratorMessage;
	}

	@Override
	public void run() {
		// 写入数据库
		UserMessage message = new UserMessage();
		message.setContent(adminitratorMessage.getContent());
		message.setTitle(adminitratorMessage.getTitle());
		message.setUserId(userId);
		message.setWebsite(adminitratorMessage.getWebsite());
		message.setCreateTime(adminitratorMessage.getCreateTime());
		message.setReadd(false);
		baseService.save(message);
	}
}