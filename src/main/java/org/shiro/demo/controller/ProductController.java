package org.shiro.demo.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.shiro.demo.dao.util.Pagination;
import org.shiro.demo.dao.util.QueryCondition;
import org.shiro.demo.entity.Category;
import org.shiro.demo.entity.Product;
import org.shiro.demo.entity.ResponseResult;
import org.shiro.demo.service.ICategoryService;
import org.shiro.demo.service.IProductService;
import org.shiro.demo.util.ProductOrder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/product")
public class ProductController {
	@Resource(name = "productService")
	private IProductService productService;

	@Resource(name = "categoryService")
	private ICategoryService categoryService;

	/**
	 * 增加产品
	 * 
	 * @param user
	 * @return
	 */
	@RequestMapping(value = "/add")
	@ResponseBody
	@RequiresAuthentication
	@RequiresRoles("product-administrator")
	public Object add(
			@RequestParam(value = "categoryId", required = true) long cateroryId,
			@RequestParam(value = "price", required = true) double price,
			@RequestParam(value = "specification", required = true) String specification,
			@RequestParam(value = "description", required = true) String description,
			@RequestParam(value = "thumbId") String thumbId,
			@RequestParam(value = "imageIds",required=false) String imageIds,
			@RequestParam(value = "amount") int  amount,
			@RequestParam(value = "note") String note) {
		
		ResponseResult rr = new ResponseResult();
		Product product = new Product();

		product.setCategory(categoryService.getById(Category.class, cateroryId));
		product.setDescription(description);
		product.setPrice(price);
		product.setSpecification(specification);
		product.setThumbId(thumbId);
		product.setImageIds(imageIds);
		product.setNote(note);
		product.setModifyDate(new Date());
		product.setAmount(amount);
		product.setStatus(false);
		
		try {
			productService.save(product);
		} catch (Exception e) {
			e.printStackTrace();
			rr.setCode(0);
			return rr;
		}
		
		rr.setObject(product.getId());
		rr.setCode(1);
		rr.setInfo("success");
		// 可能需要优化，因为前台需要productid去拿值
		
		return rr;
	}

	/**
	 * 已使用
	 * 
	 * @param categoryId
	 * @param current
	 * @param count
	 * @param order
	 * @return
	 */
	@RequestMapping(value = "/list")
	public @ResponseBody List<Product> list(
			@RequestParam(value = "categoryId", required = true) long categoryId,
			@RequestParam(value = "current", required = true) int current,
			@RequestParam(value = "count", required = true) int count,
			@RequestParam(value = "order", required = false) String orderName) {

		Category category = categoryService.getById(Category.class, categoryId);
		
		if (null != category) {
			List<QueryCondition> list = new ArrayList<QueryCondition>();
			QueryCondition queryCondition = new QueryCondition("category",QueryCondition.EQ, category);
			QueryCondition queryCondition1 = new QueryCondition("status",QueryCondition.EQ,true );
			list.add(queryCondition);
			list.add(queryCondition1);
			
			ProductOrder productOrder=null;
			//可以用try-catch处理器异常
//			if(orderName!=null)
//				productOrder = ProductOrder.valueOf(orderName);
//			else{
//			productOrder = ProductOrder.newest;
//			}
			try {
				productOrder = ProductOrder.valueOf(orderName);
			} catch (Exception e) {
				productOrder = ProductOrder.newest;
			}
				
			String order;
			switch (productOrder) {
			case hot:
				order = productOrder.getText();
				break;
			case newest:
				order = productOrder.getText();
				break;
			case highprice:
				order=productOrder.getText();
				break;
			case lowerprice:
				order=productOrder.getText();
				break;
			default:
				order= "modifyDate DESC";
				break;
			}
			

			Pagination<Product> pa = (Pagination<Product>) productService
					.getPagination(Product.class, list,
							"ORDER BY o."+order, current, count);

			return pa.getRecordList();
		}
		return null;
	}

	/**
	 * 根据产品类别拿产品
	 * 
	 * @param categoryId
	 * @return
	 */
	@RequestMapping(value = "/listbycid")
	public @ResponseBody List<Product> getProductsByCategoryid(
			@RequestParam(value = "categoryId") long categoryId,
			@RequestParam(value = "order", required = false) String orderName) {
		
		Category category = categoryService.getById(Category.class, categoryId);
		if (null != category) {
			List<QueryCondition> list = new ArrayList<QueryCondition>();
			QueryCondition queryCondition = new QueryCondition("category",QueryCondition.EQ, category);
			QueryCondition queryCondition1 = new QueryCondition("status",QueryCondition.EQ,true );
			list.add(queryCondition);
			list.add(queryCondition1);
			ProductOrder productOrder=null;
			if(orderName!=null)
				productOrder = ProductOrder.valueOf(orderName);
			else
				productOrder = ProductOrder.newest;
			
			String order;
			switch (productOrder) {
			case hot:
				order = productOrder.getText();
				break;
			case newest:
				order = productOrder.getText();
				break;
			case highprice:
				order=productOrder.getText();
				break;
			case lowerprice:
				order=productOrder.getText();
				break;
			default:
				order= "modifyDate DESC";
				break;
			}
			
			List<Product> products = productService.get(Product.class, list,
					"ORDER BY o."+order);

			return products;
		}
		return null;
	}
	/**
	 * 根据产品类别拿产品...不分status
	 * 
	 * @param categoryId
	 * @return
	 */
	@RequestMapping(value = "/listbycid2")
	public @ResponseBody List<Product> getProductsByCategoryid2(
			@RequestParam(value = "categoryId") long categoryId,
			@RequestParam(value = "order", required = false) String orderName) {
		
		Category category = categoryService.getById(Category.class, categoryId);
		if (null != category) {
			List<QueryCondition> list = new ArrayList<QueryCondition>();
			QueryCondition queryCondition = new QueryCondition("category",QueryCondition.EQ, category);
			list.add(queryCondition);
			ProductOrder productOrder=null;
			if(orderName!=null)
				productOrder = ProductOrder.valueOf(orderName);
			else
				productOrder = ProductOrder.newest;
			
			String order;
			switch (productOrder) {
			case hot:
				order = productOrder.getText();
				break;
			case newest:
				order = productOrder.getText();
				break;
			case highprice:
				order=productOrder.getText();
				break;
			case lowerprice:
				order=productOrder.getText();
				break;
			default:
				order= "modifyDate DESC";
				break;
			}
			
			List<Product> products = productService.get(Product.class, list,
					"ORDER BY o."+order);

			return products;
		}
		return null;
	}
	/*
	 * 修改产品
	 */
	@RequestMapping("/modify")
	@ResponseBody
	@RequiresAuthentication
	@RequiresRoles("product-administrator")
	public Object modifyProduct( @RequestParam(value = "description") String description,
			@RequestParam(value = "price") double price,
			@RequestParam(value = "note") String note,
			@RequestParam(value = "thumbId") String thumbId,
			@RequestParam(value = "imageIds") String imageIds,
			@RequestParam(value = "specification") String specification,
			@RequestParam(value = "amount") int amount,
			@RequestParam(value = "status") boolean status,
			@RequestParam(value = "categoryId") long categoryId,
			@RequestParam(value = "id") long id) {

		ResponseResult rr = new ResponseResult();
		Category category = categoryService.getById(Category.class, categoryId);
		Product product=new Product();
		
		product.setDescription(description);
		product.setPrice(price);
		product.setSpecification(specification);
		product.setThumbId(thumbId);
		product.setImageIds(imageIds);
		product.setNote(note);
		product.setModifyDate(new Date());
		product.setAmount(amount);
		product.setStatus(false);
		product.setId(id);
		product.setCategory(category);
		product.setStatus(status);
		// 前台不传修改时间，后台自己改❤
		try {
			productService.update(product);
		} catch (Exception e) {
			e.printStackTrace();
			rr.setCode(0);
			return rr;
		}
		rr.setCode(1);
		rr.setInfo("success");
		rr.setObject(null);
		return rr;

	}

	/*
	 * 根据产品id拿数据
	 */
	@RequestMapping(value = "/get")
	public @ResponseBody Product getByProductId(
			@RequestParam(value = "id", required = true) long id) {
		Product product = productService.getById(Product.class, id);
		return product;

	}

	/*
	 * 
	 */
	@RequestMapping(value = "/delete")
	@ResponseBody
	@RequiresAuthentication
	@RequiresRoles("product-administrator")
	public Object deletById(@RequestParam(value = "id", required = true) long id) {
		ResponseResult rr = new ResponseResult();
		try {
			productService.delete(Product.class, id);
		} catch (Exception e) {
			e.printStackTrace();
			rr.setCode(0);
			return rr;
		}
		rr.setCode(1);
		rr.setInfo("success");
		rr.setObject(null);
		return rr;
	}

}
