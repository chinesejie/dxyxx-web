package org.shiro.demo.controller;

import java.util.Vector;

import javax.annotation.Resource;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.apache.shiro.subject.Subject;
import org.shiro.demo.entity.Administrator;
import org.shiro.demo.entity.ResponseResult;
import org.shiro.demo.entity.Store;
import org.shiro.demo.service.IAdminService;
import org.shiro.demo.service.IStoreService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 
 * @author young 日期： 2015年4月25日
 */

@Controller
@RequestMapping(value = "/store")
public class StoreController {

	@Resource(name = "storeService")
	private IStoreService storeService;
	@Resource(name = "adminService")
	private IAdminService adminService;

	@RequestMapping(value = "/list")
	@ResponseBody
	public Object getAllStores() {
		return storeService.getAll(Store.class);
	}

	@RequestMapping(value = "/getByManger")
	@ResponseBody
	@RequiresAuthentication
	@RequiresRoles("manager")
	public Object getByManager() {
		Administrator admin = getCurrentAdmin();
		return admin.getStores();
	}

	// 给店铺增加店长
	@RequestMapping(value = "/addmanager")
	@ResponseBody
	@RequiresAuthentication
	@RequiresRoles("super-administrator")
	public Object addmanager(@RequestParam(value = "id") int id, @RequestParam(value = "managerId") int managerId) {
		ResponseResult rr = new ResponseResult();
		Store store = storeService.getById(Store.class, id);

		Administrator manager = adminService.getById(Administrator.class, managerId);

		if (store == null) {
			rr.setCode(0);
			rr.setInfo("该店不存在");
			return rr;
		}

		if (manager == null) {
			rr.setCode(0);
			rr.setInfo("该店长不存在");
			return rr;
		}

		// 判定一个店重复店长。
		if (manager.getStores().contains(store)) {
			rr.setCode(0);
			rr.setInfo("该店长已经存在，不能添加");
			return rr;
		}

		manager.getStores().add(store);

		try {
			adminService.update(manager);
		} catch (Exception e) {
			e.printStackTrace();
			rr.setCode(0);
			return rr;
		}

		rr.setCode(1);
		rr.setInfo("success");
		rr.setObject(null);
		return rr;
	}

	/**
	 * 更新店信息
	 * 
	 * @param store
	 * @return
	 */
	@RequestMapping(value = "/update")
	@ResponseBody
	@RequiresAuthentication
	@RequiresRoles("super-administrator")
	public Object updateStore(@RequestBody Store store) {
		ResponseResult rr = new ResponseResult();

		Store oStore = storeService.getById(Store.class, store.getId());

		if (oStore == null) {
			rr.setCode(0);
			rr.setInfo("不存在这家店");
			return rr;
		}

		oStore.setAddress(store.getAddress());
		oStore.setName(store.getName());
		oStore.setImage(store.getImage());

		try {
			storeService.update(oStore);
		} catch (Exception e) {
			e.printStackTrace();
			rr.setCode(0);
			rr.setInfo("系统出错");
			return rr;
		}
		rr.setCode(1);
		rr.setInfo("success");
		rr.setObject("");
		return rr;
	}

	// 得到 店铺的的店长
	@RequestMapping(value = "/geta")
	@ResponseBody
	@RequiresAuthentication
	@RequiresRoles("super-administrator")
	public Object geta(@RequestParam(value = "id") int id) {
		ResponseResult rr = new ResponseResult();

		Store str = storeService.getById(Store.class, id);
		// str.getManagers() 返回org.eclipse.persistence.indirection.IndirectList
		// extends java.util.Vector。。切记
		Vector<Administrator> ads = (Vector<Administrator>) str.getManagers();
		System.out.println(ads.size());
		rr.setCode(1);
		rr.setInfo("success");
		rr.setObject(ads);
		return rr;
	}

	@RequestMapping(value = "/add")
	@ResponseBody
	@RequiresAuthentication
	@RequiresRoles("super-administrator")
	public Object addStore(Store store) {
		ResponseResult rr = new ResponseResult();

		try {
			storeService.save(store);

		} catch (Exception e) {
			e.printStackTrace();
			rr.setCode(0);
			return rr;
		}

		rr.setCode(1);
		rr.setInfo("success");
		rr.setObject(null);
		return rr;
	}

	/**
	 * 删除店长
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/deleteManager")
	@RequiresAuthentication
	@RequiresRoles("super-administrator")
	@ResponseBody
	public Object deleteStore(@RequestParam(value = "mid") int managerId, @RequestParam(value = "id") int id) {
		ResponseResult rr = new ResponseResult();

		Store store = storeService.getById(Store.class, id);
		Administrator manager = adminService.getById(Administrator.class, managerId);

		if (store == null) {
			rr.setCode(0);
			rr.setInfo("该店不存在");
			return rr;
		}

		if (manager == null) {
			rr.setCode(0);
			rr.setInfo("该店不存在这位店长");
			return rr;
		}

		manager.getStores().remove(store);
		try {
			adminService.update(manager);
		} catch (Exception e) {
			e.printStackTrace();
			rr.setCode(0);
			return rr;
		}
		rr.setCode(1);
		rr.setInfo("success");
		rr.setObject(null);
		return rr;
	}

	private Administrator getCurrentAdmin() {
		Subject currentAdmin = SecurityUtils.getSubject();

		Administrator admin = (Administrator) currentAdmin.getPrincipal();
		return admin;
	}

}
