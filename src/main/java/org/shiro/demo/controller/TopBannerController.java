package org.shiro.demo.controller;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.shiro.demo.entity.ResponseResult;
import org.shiro.demo.entity.TopBanner;
import org.shiro.demo.service.ITopBannerService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/topbanner")
public class TopBannerController {
	
	@Resource(name = "topBannerService")
	private ITopBannerService topBannerService;
	
	
	/**
	 *  得到所有的top横幅，无优先级排序
	 * @return 横幅 
	 */
	@RequestMapping(value = "/listNoOrder")
	public @ResponseBody List<TopBanner> getAllTopBanners(){
		List<TopBanner> topBanners=topBannerService.getAll(TopBanner.class);
		return topBanners;
	}
	
	/**
	 *  得到所有的top横幅，有优先级排序
	 * @return 横幅 
	 */
	@RequestMapping(value = "/list")
	public @ResponseBody List<TopBanner> getAllTopBannersOrder(){
		List<TopBanner> topBanners=topBannerService.get(TopBanner.class, null, "ORDER BY o.priority");
		return topBanners;
	}
	
	
	
	/**
	 * 通过ID去修改横幅
	 * @param object 横幅对象
	 * @return
	 */
	@RequestMapping(value = "/modify")
	@ResponseBody
	@RequiresAuthentication
	@RequiresRoles("topBanner-administrator")
	public Object modifyTopBannerById(TopBanner object){
		ResponseResult rr=new ResponseResult();
		object.setModifyTime(new Date());
		try {
			topBannerService.update(object);
		} catch (Exception e) {
			e.printStackTrace();
			rr.setCode(0);
			return rr;
		}
		rr.setCode(1);
		rr.setInfo("success");
		rr.setObject(null);
		return rr;	
		
	}
	
	/***
	 * 
	 * 	 增加横幅 
	 * @return
	 */
	@RequestMapping(value = "/add")
	@ResponseBody
	@RequiresAuthentication
	@RequiresRoles("topBanner-administrator")
	public Object addTopBanner(@RequestParam(value = "description", required = true) String description,
					@RequestParam(value = "priority", required = true) int priority,
					@RequestParam(value = "imageId", required = false) String imageId,
					@RequestParam(value = "note", required = false) String note){
		
		ResponseResult rr=new ResponseResult();
		TopBanner obj=new TopBanner();
		
		obj.setDescription(description);
		obj.setImageId(imageId);
		obj.setModifyTime(new Date());
		obj.setPriority(priority);
		obj.setNote(note);
		
		try {
			topBannerService.save(obj);
		} catch (Exception e) {
			e.printStackTrace();
			rr.setCode(0);
			return rr;
		}
		rr.setCode(1);
		rr.setInfo("success");
		rr.setObject(obj.getId());
		return rr;	
		}
	
	/***
	 * 删除横幅
	 * @param id  横幅id
	 * @return 返回是否成功
	 */
	@RequestMapping(value = "/delete")
	@ResponseBody
	@RequiresAuthentication
	@RequiresRoles("topBanner-administrator")
	public Object deleteTopBanner(@RequestParam(value="id",required=true)long id){

		ResponseResult rr=new ResponseResult();
		
		try {
			topBannerService.delete(TopBanner.class, id);
		} catch (Exception e) {
			e.printStackTrace();
			rr.setCode(0);
			return rr;
		}
		rr.setCode(1);
		rr.setInfo("success");
		rr.setObject(null);
		return rr;	
	}
	
}
