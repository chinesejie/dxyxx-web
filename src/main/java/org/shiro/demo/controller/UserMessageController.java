package org.shiro.demo.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.shiro.demo.dao.util.QueryCondition;
import org.shiro.demo.entity.UserMessage;
import org.shiro.demo.entity.ResponseResult;
import org.shiro.demo.entity.User;
import org.shiro.demo.service.IBaseService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/user_message")
public class UserMessageController {

	@Resource(name = "baseService")
	private IBaseService baseService;

	/**
	 * 管理员群发消息
	 * 
	 * @param title
	 * @param content
	 * @param website
	 * @return
	 */
	@RequestMapping(value = "/get")
	@ResponseBody
	@RequiresAuthentication
	@RequiresRoles("customer")
	public Object get(int current, int size) {
		ResponseResult rr = new ResponseResult();
		User user = (User) SecurityUtils.getSubject().getPrincipal();
		List<QueryCondition> list = new ArrayList<QueryCondition>();
		list.add(new QueryCondition("userId", QueryCondition.EQ, user.getId()));
		 
		rr.setInfo("success");
		rr.setObject(baseService.getPagination(UserMessage.class, list,"ORDER　BY o.createTime desc", current,size ));
		rr.setCode(1);
		return rr;
	}

	// 应该不会用到 此函数，保留
	@RequestMapping(value = "/update")
	@ResponseBody
	@RequiresAuthentication
	@RequiresRoles("customer")
	public Object update(String id) {
		ResponseResult rr = new ResponseResult();
		User user = (User) SecurityUtils.getSubject().getPrincipal();
		try {
			long lid = Long.parseLong(id);
			UserMessage message = baseService.getById(UserMessage.class, lid);
			if (message.getUserId() != user.getId()) {
				rr.setInfo("信息不属于您，不能修改");
				return rr;
			}
			message.setReadd(true);
			baseService.update(message);// 修改成功
			rr.setInfo("success");
			rr.setCode(1);
		} catch (Exception e) {
			e.printStackTrace();

			return rr;
		}
		return rr;
	}

}
