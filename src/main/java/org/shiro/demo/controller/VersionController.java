package org.shiro.demo.controller;

import java.util.List;

import javax.annotation.Resource;

import org.shiro.demo.entity.ResponseResult;
import org.shiro.demo.entity.Version;
import org.shiro.demo.service.IBaseService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/version")
public class VersionController {

	@Resource
	private IBaseService baseService;

	@RequestMapping(value = "/getVersion")
	@ResponseBody
	public Object getVersion() {
		ResponseResult rr = new ResponseResult();
		List<Version> versions = baseService.getAll(Version.class);
		rr.setObject(versions);
		rr.setCode(1);
		return rr;
	}

	@RequestMapping(value = "/getLatestVersion")
	@ResponseBody
	public Object getLatestVersion() {
		String jpql = "SELECT e FROM Version e WHERE e.versions = (SELECT MAX(ee.versions) FROM Version ee)";
		List<Version> version =  baseService.getByJpql(jpql);
		return version;//可能返回两个Version
	}

}
