package org.shiro.demo.disruptor;

import java.util.Date;
import java.util.List;

//DEMO中使用的 消息全假定是一条交易
public class TradeTransaction {
	private String content;// 交易金额
	private Date createTime;
	private String id;// 交易ID
	private List<String> openids;
	private double price;
	private String url;// 点击的网址
	private List<String> userids;

	public TradeTransaction() {
	}

	public TradeTransaction(String content, Date createTime, String id, double price) {
		this.content = content;
		this.createTime = createTime;
		this.id = id;
		this.price = price;
	}

	public String getContent() {
		return content;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public String getId() {
		return id;
	}

	public List<String> getOpenids() {
		return openids;
	}

	public double getPrice() {
		return price;
	}

	public String getUrl() {
		return url;
	}

	public List<String> getUserids() {
		return userids;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setOpenids(List<String> openids) {
		this.openids = openids;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void setUserids(List<String> userids) {
		this.userids = userids;
	}
}