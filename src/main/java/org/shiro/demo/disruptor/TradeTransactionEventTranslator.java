package org.shiro.demo.disruptor;

import com.lmax.disruptor.EventTranslator;

public class TradeTransactionEventTranslator implements EventTranslator<TradeTransaction> {
	private TradeTransaction event;

	public TradeTransactionEventTranslator(TradeTransaction event) {
		this.event = event;
	}

	@Override
	public void translateTo(TradeTransaction event, long sequence) {
		this.generateTradeTransaction(event);
	}

	private TradeTransaction generateTradeTransaction(TradeTransaction trade) {
		trade.setPrice(event.getPrice());
		trade.setId(event.getId());
		trade.setCreateTime(event.getCreateTime());
		trade.setContent(event.getContent());
		trade.setOpenids(event.getOpenids());
		trade.setUserids(event.getUserids());
		trade.setUrl(event.getUrl());
		return trade;

	}
}