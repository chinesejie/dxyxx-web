package org.shiro.demo.disruptor;

import com.lmax.disruptor.dsl.Disruptor;

public class TradeTransactionPublisher implements Runnable {
	Disruptor<TradeTransaction> disruptor;

	TradeTransactionEventTranslator tradeTransloator;

	public TradeTransactionPublisher(TradeTransactionEventTranslator tradeTransloator, Disruptor<TradeTransaction> disruptor) {
		this.disruptor = disruptor;
		this.tradeTransloator = tradeTransloator;
	}

	@Override
	public void run() {

		disruptor.publishEvent(tradeTransloator);

	}

}
