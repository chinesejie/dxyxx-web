package org.shiro.demo.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="n_ad_banner")
public class AdBanner {
	
	/** 横幅广告id */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	/** 描述，选填 */	
	@Column(length=200)
	private String description;
	
	/** 图片id，从zimg获得 */
	@Column(length=200)
	private String imageId;
	
	/**	最近修改时间 */	
	@Temporal(TemporalType.TIMESTAMP)
	private Date modifyTime;
	
	/** 数值越小，优先级越高，放置位置 越前面. 优先级随意设置，但必须是整数，不超过100*/	
	private int priority;
	
	/** 备注 */	
	@Column(length=200)
	private String note;
	
	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImageId() {
		return imageId;
	}

	public void setImageId(String imageId) {
		this.imageId = imageId;
	}

	public Date getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}
	
	
	
}
