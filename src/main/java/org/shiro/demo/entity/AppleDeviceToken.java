package org.shiro.demo.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "n_appledevice_token")
public class AppleDeviceToken {
	/** 写入时间 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_time")
	private Date createTime;
	@Column(name = "token")
	private String token;
	@Column(name = "user_id")
	private int userId;
	// iphone的uuid，保证 一只手机只有一个 token，保证不多余..uuid 是40位字符串
	@Id
	@Column(name = "uuid")
	private String uuid;

	public Date getCreateTime() {
		return createTime;
	}

	public String getToken() {
		return token;
	}

	public int getUserId() {
		return userId;
	}

	public String getUuid() {
		return uuid;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

}
