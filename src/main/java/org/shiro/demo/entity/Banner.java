package org.shiro.demo.entity;

import java.util.List;

public class Banner {
	
	private List<HomePageBanner> homePageBanners;
	private List<TopBanner> topBanners;
	public List<HomePageBanner> getHomePageBanners() {
		return homePageBanners;
	}
	public void setHomePageBanners(List<HomePageBanner> homePageBanners) {
		this.homePageBanners = homePageBanners;
	}
	public List<TopBanner> getTopBanners() {
		return topBanners;
	}
	public void setTopBanners(List<TopBanner> topBanners) {
		this.topBanners = topBanners;
	}
	
}
