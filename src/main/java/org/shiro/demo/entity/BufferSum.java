package org.shiro.demo.entity;

import java.util.Date;

/**
 * 店长看月账单 暂时不做
 */
public class BufferSum {
	private Date bufferDay;
	private int g;
	private double gmoney;
	private String managerPhone;// 店长的手机号
	private int o;
	private double omoney;
	private int store;// 门店id

	private int t;

	private double tmoney;

	public Date getBufferDay() {
		return bufferDay;
	}

	public int getG() {
		return g;
	}

	public double getGmoney() {
		return gmoney;
	}

	public String getManagerPhone() {
		return managerPhone;
	}

	public int getO() {
		return o;
	}

	public double getOmoney() {
		return omoney;
	}

	public int getStore() {
		return store;
	}

	public int getT() {
		return t;
	}

	public double getTmoney() {
		return tmoney;
	}

	public void setBufferDay(Date bufferDay) {
		this.bufferDay = bufferDay;
	}

	public void setG(int g) {
		this.g = g;
	}

	public void setGmoney(double gmoney) {
		this.gmoney = gmoney;
	}

	public void setManagerPhone(String managerPhone) {
		this.managerPhone = managerPhone;
	}

	public void setO(int o) {
		this.o = o;
	}

	public void setOmoney(double omoney) {
		this.omoney = omoney;
	}

	public void setStore(int store) {
		this.store = store;
	}

	public void setT(int t) {
		this.t = t;
	}

	public void setTmoney(double tmoney) {
		this.tmoney = tmoney;
	}

}
