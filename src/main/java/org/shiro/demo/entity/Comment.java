package org.shiro.demo.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "n_comment")
public class Comment {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	/** 主键 */
	private long id;
	private String images;
	@Temporal(TemporalType.TIMESTAMP)
	private Date postTime;// 不做外键
	private String product_id;// 不做外键
	@Column(name = "REPLYIDS", columnDefinition = "longtext")
	private String replyIds;
	@Transient
	private List<CommentReply> replys;

	@Transient
	private User user;

	// 其实就是手机
	private String user_id;

	/**
	 * 这张image 是快照。就是用户修改了 图片不会影响这条评论
	 */
	@Column(name = "user_image")
	private String user_image;

	@Column(name = "userName")
	private String userName;

	private String word;

	public long getId() {
		return id;
	}

	public String getImages() {
		return images;
	}

	public Date getPostTime() {
		return postTime;
	}

	public String getProduct_id() {
		return product_id;
	}

	public String getReplyIds() {
		return replyIds;
	}

	public List<CommentReply> getReplys() {
		return replys;
	}

	public User getUser() {
		return user;
	}

	/**
	 * // 其实就是手机
	 * 
	 * @return
	 */
	public String getUser_id() {
		return user_id;
	}

	public String getUser_image() {
		return user_image;
	}

	public String getUserName() {
		return userName;
	}

	public String getWord() {
		return word;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setImages(String images) {
		this.images = images;
	}

	public void setPostTime(Date postTime) {
		this.postTime = postTime;
	}

	public void setProduct_id(String product_id) {
		this.product_id = product_id;
	}

	public void setReplyIds(String replyIds) {
		this.replyIds = replyIds;
	}

	public void setReplys(List<CommentReply> replys) {
		this.replys = replys;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public void setUser_image(String user_image) {
		this.user_image = user_image;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setWord(String word) {
		this.word = word;
	}

}
