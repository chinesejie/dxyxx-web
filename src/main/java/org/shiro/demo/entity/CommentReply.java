package org.shiro.demo.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "n_comment_reply")
public class CommentReply implements Serializable {
	private long commentId;// 不做外键

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	/** 主键 */
	private long id;

	@Temporal(TemporalType.TIMESTAMP)
	private Date postTime;// 不做外键

	@Transient
	private User user;

	// 其实就是手机
	private String user_id;
	/**
	 * 这张image 是快照。就是用户修改了 图片不会影响这条评论
	 */
	@Column(name = "user_image")
	private String user_image;

	private String userName;

	private String word;// 限制200字

	public long getCommentId() {
		return commentId;
	}

	public long getId() {
		return id;
	}

	public Date getPostTime() {
		return postTime;
	}

	public User getUser() {
		return user;
	}

	public String getUser_id() {
		return user_id;
	}

	public String getUser_image() {
		return user_image;
	}

	public String getUserName() {
		return userName;
	}

	public String getWord() {
		return word;
	}

	public void setCommentId(long commentId) {
		this.commentId = commentId;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setPostTime(Date postTime) {
		this.postTime = postTime;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public void setUser_image(String user_image) {
		this.user_image = user_image;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setWord(String word) {
		this.word = word;
	}

}
