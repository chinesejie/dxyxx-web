package org.shiro.demo.entity;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DoubanInfo {
	// 豆瓣根据用户token得到信息 返回：{"loc_id":"108296","name":"巨胖子","created":"2014-07-30 21:19:26","is_banned":false,"is_suicide":false,"loc_name":"上海","avatar":"http:\/\/img3.douban.com\/icon\/user_normal.jpg","signature":"","uid":"94707561","alt":"http:\/\/www.douban.com\/people\/94707561\/","desc":"","type":"user","id":"94707561","large_avatar":"http:\/\/img3.douban.com\/icon\/user_large.jpg"}
	private String access_token;

	private String uid;

	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAccess_token() {
		return access_token;
	}

	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}
}
