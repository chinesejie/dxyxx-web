package org.shiro.demo.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 记录免费 代金券的提取情况
 * 
 * @author Chinesejie
 *
 */
@Entity
@Table(name = "n_free_collect")
public class FreeCollect {

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_time")
	private Date createTime;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	private boolean isPay;
	private String phone;
	private String uuid;
	// 对应 n_free_info 的版本号。
	private int version;

	public Date getCreateTime() {
		return createTime;
	}

	public int getId() {
		return id;
	}

	public String getPhone() {
		return phone;
	}

	public String getUuid() {
		return uuid;
	}

	public int getVersion() {
		return version;
	}

	public boolean isPay() {
		return isPay;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setPay(boolean isPay) {
		this.isPay = isPay;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public void setVersion(int version) {
		this.version = version;
	}

}
