package org.shiro.demo.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 记录免费 代金券的 情况。
 * 10块钱=1000分
 * 所以1分钱 对应的discount就是0.001
 * 
 * 需要缓存。。。
 * 
 * 金额，折扣？ 版本号。 是否可用。。要发出几份？
 * 10,0.001,1,true,1000
 * @author Chinesejie
 *
 */
@Entity
@Table(name = "n_free_info")
public class FreeInfo implements Serializable {
	private double discount;
	private int howmany;
	private double money;
	private boolean useful;
	@Id
	private int version;

	public double getDiscount() {
		return discount;
	}

	public int getHowmany() {
		return howmany;
	}

	public double getMoney() {
		return money;
	}

	public int getVersion() {
		return version;
	}

	public boolean isUseful() {
		return useful;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	public void setHowmany(int howmany) {
		this.howmany = howmany;
	}

	public void setMoney(double money) {
		this.money = money;
	}

	public void setUseful(boolean useful) {
		this.useful = useful;
	}

	public void setVersion(int version) {
		this.version = version;
	}

}
