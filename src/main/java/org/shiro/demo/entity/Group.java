package org.shiro.demo.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 团类型。
 * 
 * A产品一人团、 直接调用order/add 买、 A产品二人团 B产品二人团 B产品二人团
 * 
 * @author Administrator
 *
 */
@Entity
@Table(name = "n_group")
public class Group {

	/**
	 * 余货。每下一单，少一个
	 */
	private int balance; 
	
	private String bigImage;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "deadline")
	private Date deadline;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modify_time")
	private Date modifyTime;
	
	@Column(name = "description")
	private String description;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	/** 主键 */
	private long id;

	private String image;
	@Column(name = "person_amount")
	/** 人数 */
	private int personAmount;

	@Column(name = "person_price")
	/** 每人价格 单价 */
	private Double personPrice;

	/** 产品 */
	@ManyToOne
	@JoinColumn(name = "product_id",nullable = false)
	private Product product;
	@Column(name = "productId",nullable = false)
	private long productId;

	private boolean status;

	// 版本号
	@Column(name = "version")
	private int version;

	public int getBalance() {
		return balance;
	}
	public String getBigImage() {
		return bigImage;
	}

	public Date getDeadline() {
		return deadline;
	}

	public String getDescription() {
		return description;
	}

	public long getId() {
		return id;
	}

	public String getImage() {
		return image;
	}

	public int getPersonAmount() {
		return personAmount;
	}

	public Double getPersonPrice() {
		return personPrice;
	}

	public Product getProduct() {
		return product;
	}

	public long getProductId() {
		return productId;
	}

	public boolean getStatus() {
		return status;
	}

	public int getVersion() {
		return version;
	}

	public void setBalance(int balance) {
		this.balance = balance;
	}

	public void setBigImage(String bigImage) {
		this.bigImage = bigImage;
	}

	public void setDeadline(Date deadline) {
		this.deadline = deadline;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public void setPersonAmount(int personAmount) {
		this.personAmount = personAmount;
	}

	public void setPersonPrice(Double personPrice) {
		this.personPrice = personPrice;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public void setVersion(int version) {
		this.version = version;
	}

}
