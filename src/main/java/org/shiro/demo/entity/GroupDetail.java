package org.shiro.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 团类型 包括产品+人数
 * */
//@Entity
//@Table(name = "n_group_detail")
public class GroupDetail {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	/** 主鍵 */
	private long id;

	@ManyToOne
	@JoinColumn(name = "group_id")
	/** 团大类(只包括产品) */
	private Group group;

	@Column(name = "person_amount")
	/** 人数 */
	private int personAmount;

	@Column(name = "person_price")
	/** 每人价格 */
	private Double personPrice;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Group getGroup() {
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

	public int getPersonAmount() {
		return personAmount;
	}

	public void setPersonAmount(int personAmount) {
		this.personAmount = personAmount;
	}

	public Double getPersonPrice() {
		return personPrice;
	}

	public void setPersonPrice(Double personPrice) {
		this.personPrice = personPrice;
	}

}
