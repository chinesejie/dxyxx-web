package org.shiro.demo.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;

@Entity
@Table(name = "n_group_instance")
public class GroupInstance {

	@ManyToOne
	@JoinColumn(name = "captain_id")
	/** 创建者 */
	private User captain;

	@ManyToOne
	@JoinColumn(name = "group_id")
	/** 团的具体型(包括什么产品,多少人数) */
	private Group group;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	/** 主键 */
	private long id;

	// @ManyToMany(mappedBy = "myGroupInstance")
	// /** 团成员 */
	// private Set<User> users;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "groupInstance")
	private Set<GroupOrder> orders = new HashSet<GroupOrder>();

	public User getCaptain() {
		return captain;
	}
	public Group getGroup() {
		return group;
	}

	public long getId() {
		return id;
	}
	@JsonIgnore
	public Set<GroupOrder> getOrders() {
		return orders;
	}

	public void setCaptain(User captain) {
		this.captain = captain;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setOrders(Set<GroupOrder> orders) {
		this.orders = orders;
	}

	// public Set<User> getUsers() {
	// return users;
	// }
	//
	// public void setUsers(Set<User> users) {
	// this.users = users;
	// }

}
