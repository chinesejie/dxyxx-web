package org.shiro.demo.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.codehaus.jackson.annotate.JsonIgnore;

@Entity
@Table(name = "n_group_order")
public class GroupOrder {
	@Column(name = "buffer_interval")
	private int bufferInterval;
	/** 提货时间 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "buffer_time")
	private Date bufferTime;

	/** 付款时间 */
	@Temporal(TemporalType.TIMESTAMP)
	private Date dealTime;

	/**
	 * 自提还是快递
	 */
	private String deliveryType;

	/** 下单时的备注 */
	@Column(name = "description")
	private String description;

	/** 折扣 */
	@Column(name = "discount")
	private Double discount;

	@ManyToOne
	@JoinColumn(name = "group_id")
	/** 对应的 团类型*/
	private Group group;
	/**
	 * 对应的团实例
	 */
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "group_instance_id")
	private GroupInstance groupInstance;

	@Column(name = "history_version")
	private int historyVersion;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	/** 主键 */
	private long id;
	/** 是否自提 */
	@Column(name = "is_buffet")
	private boolean isBuffet;

	/** 是否已经取消 */
	@Column(name = "is_cancel")
	private boolean isCancel;

	/** 是否付款 */
	@Column(name = "is_pay")
	private boolean isPay;
	/**
	 * 总价
	 */
	private double money;
	private String operator;

	@Column(name = "operator_store")
	private int operator_store;// 处理订单的门店。。提货门店

	@ManyToOne
	@JoinColumn(name = "prop_id")
	private OrderProp orderProp;

	/**
	 * 微信支付，支付宝支付
	 */
	private String payType;

	/** 下单时间 */
	@Temporal(TemporalType.TIMESTAMP)
	private Date postTime;

	/** 应提货时间 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "pre_buffer_time")
	private Date preBufferTime;
	/** 提货的店面号 */
	@Column(name = "store_id")
	private int storeId;
	/**
	 * 对应的交易id号码
	 */
	private String trade_no;

	@ManyToOne
	@JoinColumn(name = "user_id")
	/** 下单的用户 */
	private User user;

	private String uuid;

	public int getBufferInterval() {
		return bufferInterval;
	}

	public Date getBufferTime() {
		return bufferTime;
	}

	public Date getDealTime() {
		return dealTime;
	}

	public String getDeliveryType() {
		return deliveryType;
	}

	public String getDescription() {
		return description;
	}
	public Double getDiscount() {
		return discount;
	}
	public Group getGroup() {
		return group;
	}

	public GroupInstance getGroupInstance() {
		return groupInstance;
	}

	public int getHistoryVersion() {
		return historyVersion;
	}

	public long getId() {
		return id;
	}

	public boolean getIsBuffet() {
		return isBuffet;
	}

	public boolean getIsCancel() {
		return isCancel;
	}

	public boolean getIsPay() {
		return isPay;
	}

	public double getMoney() {
		return money;
	}

	public String getOperator() {
		return operator;
	}

	public int getOperator_store() {
		return operator_store;
	}

	public OrderProp getOrderProp() {
		return orderProp;
	}

	public String getPayType() {
		return payType;
	}

	public Date getPostTime() {
		return postTime;
	}

	public Date getPreBufferTime() {
		return preBufferTime;
	}

	public int getStoreId() {
		return storeId;
	}

	public String getTrade_no() {
		return trade_no;
	}

	public User getUser() {
		return user;
	}

	public String getUuid() {
		return uuid;
	}

	public void setBufferInterval(int bufferInterval) {
		this.bufferInterval = bufferInterval;
	}

	public void setBufferTime(Date bufferTime) {
		this.bufferTime = bufferTime;
	}

	public void setDealTime(Date dealTime) {
		this.dealTime = dealTime;
	}

	public void setDeliveryType(String deliveryType) {
		this.deliveryType = deliveryType;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

	public void setGroupInstance(GroupInstance groupInstance) {
		this.groupInstance = groupInstance;
	}

	public void setHistoryVersion(int historyVersion) {
		this.historyVersion = historyVersion;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setIsBuffet(Boolean isBuffet) {
		this.isBuffet = isBuffet;
	}

	public void setIsCancel(Boolean isCancel) {
		this.isCancel = isCancel;
	}

	public void setIsPay(Boolean isPay) {
		this.isPay = isPay;
	}

	public void setMoney(double money) {
		this.money = money;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public void setOperator_store(int operator_store) {
		this.operator_store = operator_store;
	}

	public void setOrderProp(OrderProp orderProp) {
		this.orderProp = orderProp;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public void setPostTime(Date postTime) {
		this.postTime = postTime;
	}

	public void setPreBufferTime(Date preBufferTime) {
		this.preBufferTime = preBufferTime;
	}

	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}

	public void setTrade_no(String trade_no) {
		this.trade_no = trade_no;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

}
