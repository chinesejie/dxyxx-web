package org.shiro.demo.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "t_user_integral")
public class Integral {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_integral_id")
	/** 主键 */
	private int id;
	@Column(name = "user_id")
	private int user_id;
	@Column(name = "user_integral_surplus")
	private int user_integral_surplus;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "user_integral_time")
	private Date user_integral_time;

	public int getId() {
		return id;
	}

	public int getUser_id() {
		return user_id;
	}

	public int getUser_integral_surplus() {
		return user_integral_surplus;
	}

	public Date getUser_integral_time() {
		return user_integral_time;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public void setUser_integral_surplus(int user_integral_surplus) {
		this.user_integral_surplus = user_integral_surplus;
	}

	public void setUser_integral_time(Date user_integral_time) {
		this.user_integral_time = user_integral_time;
	}

}
