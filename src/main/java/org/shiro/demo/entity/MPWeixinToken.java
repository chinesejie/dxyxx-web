package org.shiro.demo.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@Entity
@Table(name = "n_mpweixin_token")
@JsonIgnoreProperties(ignoreUnknown = true)
public class MPWeixinToken implements Serializable {
	@Column(name = "access_token", columnDefinition = "longtext")
	private String access_token;
	@Column(name = "expires_in")
	private int expires_in;// 7200秒=60s*60m*2h

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "expired_time")
	private Date expires_time;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	public String getAccess_token() {
		return access_token;
	}

	public int getExpires_in() {
		return expires_in;
	}

	public Date getExpires_time() {
		return expires_time;
	}

	public int getId() {
		return id;
	}

	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}

	public void setExpires_in(int expires_in) {
		this.expires_in = expires_in;
	}

	public void setExpires_time(Date expires_time) {
		this.expires_time = expires_time;
	}

	public void setId(int id) {
		this.id = id;
	}

}
