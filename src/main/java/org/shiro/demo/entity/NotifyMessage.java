package org.shiro.demo.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * 订单支付成功之后 会给店长 发微信跟发消息。
 * 
 * 后期要求顾客绑定微信，也会给顾客发送微信通知。
 * 
 * @author Administrator
 *
 */
@Entity
@Table(name = "n_notify_message")
@JsonIgnoreProperties(ignoreUnknown = true)
public class NotifyMessage {
	@Column(name = "admin_id")
	private String admin_id;
	@Column(columnDefinition = "longtext")
	private String content;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_time")
	private Date createTime;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	/** 主键 */
	private long id;
	@Column(name = "hreads")
	private boolean hreads;

	public String getAdmin_id() {
		return admin_id;
	}

	public void setAdmin_id(String admin_id) {
		this.admin_id = admin_id;
	}

	public String getContent() {
		return content;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public long getId() {
		return id;
	}

	public boolean isHreads() {
		return hreads;
	}

	public void setHreads(boolean hreads) {
		this.hreads = hreads;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public void setId(long id) {
		this.id = id;
	}

}
