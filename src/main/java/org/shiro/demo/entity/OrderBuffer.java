package org.shiro.demo.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 记录提货详情。
 * 
 * 
 * @author Administrator
 *
 */
@Entity
@Table(name = "n_order_buffer")
public class OrderBuffer {
	/**
	 * 提货的 日期
	 */
	@Temporal(TemporalType.DATE)
	@Column(name = "buffer_date")
	private Date bufferDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "buffer_time")
	private Date bufferTime;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	/** 主键 */
	private long id;

	/**
	 * 提货的金额
	 */
	@Column(name = "money")
	private double money;

	/**
	 * 操作人。。。店长
	 */
	@Column(name = "operator")
	private String operator;
	/**
	 * 订单类型
	 * 
	 * 正常订单1 开团订单2 代金券3
	 */
	@Column(name = "order_type")
	private int orderType;

	/**
	 * 门店的id
	 */
	@Column(name = "store_id")
	private int storeId;

	/**
	 * 订单的uuid
	 */
	@Column(name = "uuid")
	private String uuid;

	public Date getBufferDate() {
		return bufferDate;
	}

	public Date getBufferTime() {
		return bufferTime;
	}

	public long getId() {
		return id;
	}

	public double getMoney() {
		return money;
	}

	public String getOperator() {
		return operator;
	}
	/**
	 * 订单类型
	 * 
	 * 正常订单1 开团订单2 代金券3
	 */
	public int getOrderType() {
		return orderType;
	}

	public int getStoreId() {
		return storeId;
	}

	public String getUuid() {
		return uuid;
	}

	public void setBufferDate(Date bufferDate) {
		this.bufferDate = bufferDate;
	}

	public void setBufferTime(Date bufferTime) {
		this.bufferTime = bufferTime;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setMoney(double money) {
		this.money = money;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public void setOrderType(int orderType) {
		this.orderType = orderType;
	}

	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

}
