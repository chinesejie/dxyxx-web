package org.shiro.demo.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 订单评价
 * 
 * @author Administrator
 *
 */
@Entity
@Table(name = "n_order_comment")
public class OrderComment {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	/** 主键 */
	private long id;

	private String images;

	private String order_id;// 不做外键..就是uuid

	@Temporal(TemporalType.TIMESTAMP)
	private Date postTime;// 不做外键
	private int score;
	private String user_id;

	private String word;

	public long getId() {
		return id;
	}

	public String getImages() {
		return images;
	}

	public String getOrder_id() {
		return order_id;
	}

	public Date getPostTime() {
		return postTime;
	}

	public int getScore() {
		return score;
	}

	public String getUser_id() {
		return user_id;
	}

	public String getWord() {
		return word;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setImages(String images) {
		this.images = images;
	}

	public void setOrder_id(String order_id) {
		this.order_id = order_id;
	}

	public void setPostTime(Date postTime) {
		this.postTime = postTime;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public void setWord(String word) {
		this.word = word;
	}

}
