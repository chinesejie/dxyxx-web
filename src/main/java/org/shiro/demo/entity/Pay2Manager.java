package org.shiro.demo.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "n_pay_to_manager")
public class Pay2Manager {
	/**
	 * 钱 是否已经被提
	 */
	@Column(name = "atmed")
	private boolean atmed;
	/**
	 * 提取时间
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "atm_time")
	private Date atmTime;
	/**
	 * 对应的 日期 2005-02-24
	 */
	@Temporal(TemporalType.DATE)
	@Column(name = "corres_date")
	private Date correspondingDate;
	/**
	 * 创建时间
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_time")
	private Date createTime;

	/**
	 * 支付的详情。 o_12_23.33;t_12_23.34;g_23_33.33
	 */
	private String detail;

	// 不让自增长了。。uuid
	@Id
	@Column(name = "id")
	/** 主键 */
	private String id;

	/**
	 * 付款金额
	 */

	@Column(name = "money")
	private double money;

	/**
	 * 收款人
	 */
	@Column(name = "payee")
	private String payee; // 收款人，手机号

	/**
	 * 门店id
	 */
	@Column(name = "storeId")
	private int storeId;

	/**
	 * 提取时间
	 */
	public Date getAtmTime() {
		return atmTime;
	}

	public Date getCorrespondingDate() {
		return correspondingDate;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public String getDetail() {
		return detail;
	}

	public String getId() {
		return id;
	}

	public double getMoney() {
		return money;
	}

	/**
	 * 就是手机号
	 * 
	 * @return
	 */
	public String getPayee() {
		return payee;
	}

	public int getStoreId() {
		return storeId;
	}

	/**
	 * 钱 是否已经被提
	 */
	public boolean isAtmed() {
		return atmed;
	}

	public void setAtmed(boolean atmed) {
		this.atmed = atmed;
	}

	public void setAtmTime(Date atmTime) {
		this.atmTime = atmTime;
	}

	public void setCorrespondingDate(Date correspondingDate) {
		this.correspondingDate = correspondingDate;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setMoney(double money) {
		this.money = money;
	}

	public void setPayee(String payee) {
		this.payee = payee;
	}

	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}

}
