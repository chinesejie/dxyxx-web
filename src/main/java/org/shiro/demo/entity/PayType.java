package org.shiro.demo.entity;

public class PayType {
	public static final String ALIPAY = "alipay";
	public static final String WEIXIN = "weixin";
	public static final String ACCOUNT = "account";
}
