package org.shiro.demo.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "phones")
public class Phone {

	@Column(name = "barcode")
	private String barcode;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "barcodeTime")
	private Date barcodeTime;
	@Id
	@Column(name = "num")
	/** 主键 */
	private String num;

	@Column(name = "smscode")
	private String smscode;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "smscodeTime")
	private Date smscodeTime;

	public String getBarcode() {
		return barcode;
	}

	public Date getBarcodeTime() {
		return barcodeTime;
	}

	public String getNum() {
		return num;
	}

	public String getSmscode() {
		return smscode;
	}

	public Date getSmscodeTime() {
		return smscodeTime;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public void setBarcodeTime(Date barcodeTime) {
		this.barcodeTime = barcodeTime;
	}

	public void setNum(String num) {
		this.num = num;
	}

	public void setSmscode(String smscode) {
		this.smscode = smscode;
	}

	public void setSmscodeTime(Date smscodeTime) {
		this.smscodeTime = smscodeTime;
	}
}
