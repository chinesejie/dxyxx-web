package org.shiro.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "n_product_detail")
public class ProductDetail {
	@Column(columnDefinition = "longtext")
	private String details;

	@Id
	@Column(name = "id")
	private long id;

	private String imageId;

	@OneToOne
	@JoinColumn(name = "product_id")
	private Product product;

	public String getDetails() {
		return details;
	}

	public long getId() {
		return id;
	}

	public String getImageId() {
		return imageId;
	}

	public Product getProduct() {
		return product;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setImageId(String imageId) {
		this.imageId = imageId;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

}
