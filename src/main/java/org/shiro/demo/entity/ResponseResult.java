package org.shiro.demo.entity;

public class ResponseResult {
	public ResponseResult() {
		code = 0;
		info = "initial";
	}

	public ResponseResult(int code, String info, Object object) {
		this.code = code;
		this.info = info;
		this.object = object;
	}

	private int code; // 1 or 0 。。0表示失败

	private String info;

	private Object object;

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

}
