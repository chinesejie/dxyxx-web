package org.shiro.demo.entity;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;

@Entity
@Table(name = "n_role")
public class Role implements Serializable {

	private static final long serialVersionUID = 6177417450707400228L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "roleId")
	private long id;

	@Column(length = 50)
	private String name;

	@Column(length = 50)
	private String description;

	@ManyToMany(mappedBy = "roles")
	@Basic(fetch = FetchType.LAZY)
	private Collection<User> users;

	/**
	 * 因为updatable = false，所以Role的改变不影响Permission. 所以Permission 这张表中不会有Roles字段
	 */
	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE }, fetch = FetchType.LAZY)
	@JoinTable(name = "n_role_pms", joinColumns = { @JoinColumn(name = "roleId", updatable = false) }, inverseJoinColumns = { @JoinColumn(name = "pmsId", updatable = false) })
	private Collection<Permission> pmss;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@JsonIgnore
	public Collection<User> getUsers() {
		return users;
	}

	public void setUsers(Collection<User> users) {
		this.users = users;
	}

	@JsonIgnore
	public Collection<Permission> getPmss() {
		return pmss;
	}

	public void setPmss(Collection<Permission> pmss) {
		this.pmss = pmss;
	}
}
