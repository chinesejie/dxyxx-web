package org.shiro.demo.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 
 * 
 * @author Chinesejie
 *
 */
@Entity
@Table(name = "t_sms_token")
public class SmsToken {
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "expired_time")
	private Date expiredTime;

	@Id
	private int id;

	private String token;

	public Date getExpiredTime() {
		return expiredTime;
	}

	public int getId() {
		return id;
	}

	public String getToken() {
		return token;
	}

	public void setExpiredTime(Date expiredTime) {
		this.expiredTime = expiredTime;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setToken(String token) {
		this.token = token;
	}

}
