package org.shiro.demo.entity;

import java.util.Collection;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;

/**
 * 实体店
 * 
 * @author Chinesejie
 *
 */
@Entity
@Table(name = "n_store")
public class Store {
	private String address;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "storeId")
	private int id;

	private String image;
	
	@ManyToMany(cascade = { CascadeType.REFRESH }, mappedBy = "stores")
	private Collection<Administrator> managers;

	private String name;

	public String getAddress() {
		return address;
	}

	public int getId() {
		return id;
	}

	public String getImage() {
		return image;
	}

	@JsonIgnore
	public Collection<Administrator> getManagers() {
		return managers;
	}

	public String getName() {
		return name;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public void setManagers(Collection<Administrator> managers) {
		this.managers = managers;
	}

	public void setName(String name) {
		this.name = name;
	}

}
