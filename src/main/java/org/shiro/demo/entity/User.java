package org.shiro.demo.entity;

import java.util.Collection;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "t_user")
public class User  {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_id")
	/** 主键 */
	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	@Column(name = "user_name")
	/** 真实名字 */
	private String name;

	@Column(name = "user_sex")
	/** 性别 */
	private String sex;

	@Column(name = "user_birthday")
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	/** 出生日期 */
	private Date birthday;

	@Column(name = "user_job")
	/** 工作 */
	private String job;

	@Column(name = "user_interests")
	/** 兴趣爱好 */
	private String image;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "user_create_time")
	/** 创建时间 */
	private Date createTime;

	@Column(name = "user_phone")
	/** 电话号码 */
	private String phone;

	@Column(name = "user_username")
	/** 用户名 */
	private String username;

	@Column(name = "user_password")
	/** 密码 */
	private String password;

	@Column(name = "user_moneypassword")
	/** 支付密码 */
	private String moneyPassword;

	@Column(name = "user_alipay")
	/**  */
	private String alipay;

	@Column(name = "user_income")
	/** 收入 */
	private String income;

	@Column(name = "user_address")
	/** 地址 */
	private String address;

	// @ManyToMany
	// @JoinTable(name = "n_groupInstance_user", joinColumns = {
	// @JoinColumn(name = "user_id", updatable = false) }, inverseJoinColumns =
	// { @JoinColumn(name = "group_instance_id", updatable = false) })
	// /** 参加的团 */
	// private Set<GroupInstance> myGroupInstance;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@JsonIgnore
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMoneyPassword() {
		return moneyPassword;
	}

	public void setMoneyPassword(String moneyPassword) {
		this.moneyPassword = moneyPassword;
	}

	public String getAlipay() {
		return alipay;
	}

	public void setAlipay(String alipay) {
		this.alipay = alipay;
	}

	public String getIncome() {
		return income;
	}

	public void setIncome(String income) {
		this.income = income;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	// public Set<GroupInstance> getMyGroupInstance() {
	// return myGroupInstance;
	// }
	//
	// public void setMyGroupInstance(Set<GroupInstance> myGroupInstance) {
	// this.myGroupInstance = myGroupInstance;
	// }

	/**
	 * 
	 * CascadeType.PERSIST的意义：
	 * 
	 * joinColumns是维护端; inverseJoinColumns是被维护端
	 * 关系维护端删除时，如果中间表存在些纪录的关联信息，则会删除该关联信息;
	 * 
	 * 关系被维护端删除时，如果中间表存在些纪录的关联信息，则会删除失败.
	 * 
	 * 
	 * 
	 */
	@ManyToMany(cascade = { CascadeType.PERSIST })
	@JoinTable(name = "n_user_role", joinColumns = { @JoinColumn(name = "userId", referencedColumnName = "user_id") }, inverseJoinColumns = { @JoinColumn(name = "roleId", referencedColumnName = "roleId") })
	private Collection<Role> roles;

	@JsonIgnore
	public Collection<Role> getRoles() {
		return roles;
	}

	public void setRoles(Collection<Role> roles) {
		this.roles = roles;
	}
}
