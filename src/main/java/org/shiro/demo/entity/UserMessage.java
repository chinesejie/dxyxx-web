package org.shiro.demo.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 收件箱...用户的message
 * 
 * @author chinesejie
 *
 */
@Entity
@Table(name = "n_user_message")
public class UserMessage {
	@Column(name = "content")
	private String content; // 纯文字。

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_time")
	private Date createTime;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;

	private boolean readd;//read 关键字

	@Column(name = "title")
	private String title; // 纯文字。控制在240个字符下
	@Column(name = "user_id")
	private int userId;

	@Column(name = "website")
	private String website; // 纯文字。

	public String getContent() {
		return content;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public long getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public int getUserId() {
		return userId;
	}

	public String getWebsite() {
		return website;
	}

	public boolean isReadd() {
		return readd;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setReadd(boolean readd) {
		this.readd = readd;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

}
