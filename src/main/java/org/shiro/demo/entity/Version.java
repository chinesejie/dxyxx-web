package org.shiro.demo.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "n_version")
public class Version {

	@Temporal(TemporalType.TIMESTAMP)
	private Date createTime;

	@Column(name = "description")
	private String description;
	private String downloadLink;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	private double versions;

	public Date getCreateTime() {
		return createTime;
	}

	public String getDescription() {
		return description;
	}

	public String getDownloadLink() {
		return downloadLink;
	}

	public long getId() {
		return id;
	}

	public double getVersions() {
		return versions;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setDownloadLink(String downloadLink) {
		this.downloadLink = downloadLink;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setVersions(double versions) {
		this.versions = versions;
	}

}
