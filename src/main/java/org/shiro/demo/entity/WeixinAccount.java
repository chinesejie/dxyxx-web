package org.shiro.demo.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 店长微信
 * 
 * @author Administrator
 *
 */
@Entity
@Table(name = "n_weixin_account")
public class WeixinAccount {

	/**
	 * 十分钟失效
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_time")
	private Date createTime;

	@Column(name = "openid")
	private String openid;

	@Id
	@Column(name = "phone")
	private String phone;

	public Date getCreateTime() {
		return createTime;
	}

	public String getOpenid() {
		return openid;
	}

	public String getPhone() {
		return phone;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

}
