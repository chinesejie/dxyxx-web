package org.shiro.demo.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 基于公众号的 openid
 * 
 * @author Administrator
 *
 */
@Entity
@Table(name = "n_weixin_open")
public class WeixinOpen {
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "bind_time")
	private Date bindTime;
	@Column(name = "openid")
	private String openid;
	@Id
	@Column(name = "phone")
	private String phone;

	@Column(name = "unionid")
	private String unionid;

	@Column(name = "user_id")
private int userId;

	public Date getBindTime() {
		return bindTime;
	}

	public String getOpenid() {
		return openid;
	}

	public String getPhone() {
		return phone;
	}

	public String getUnionid() {
		return unionid;
	}

	public int getUserId() {
		return userId;
	}

	public void setBindTime(Date bindTime) {
		this.bindTime = bindTime;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setUnionid(String unionid) {
		this.unionid = unionid;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

}
