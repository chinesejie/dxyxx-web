package org.shiro.demo.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "n_weixin_prepay")
public class WeixinPrepay {

	@Column(name = "json", columnDefinition = "longtext")
	private String json;
	@Column(name = "order_phone_type")
	private String orderPhoneType;
	@Temporal(TemporalType.TIMESTAMP)
	private Date prepayTime;
	@Id
	@Column(name = "uuid")
	/** 主键 */
	private String uuid;

	public String getJson() {
		return json;
	}

	public String getOrderPhoneType() {
		return orderPhoneType;
	}

	public Date getPrepayTime() {
		return prepayTime;
	}

	public String getUuid() {
		return uuid;
	}

	public void setJson(String json) {
		this.json = json;
	}

	public void setOrderPhoneType(String orderPhoneType) {
		this.orderPhoneType = orderPhoneType;
	}

	public void setPrepayTime(Date prepayTime) {
		this.prepayTime = prepayTime;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
}
