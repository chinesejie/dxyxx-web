package org.shiro.demo.service;

import java.util.List;

import org.shiro.demo.entity.Administrator;

public interface IAdminService extends IBaseService {

	public Administrator getByAccount(String account);
	public Administrator getByName(String name);
	public boolean register(Administrator Administrator);

	public Administrator getByPhone(String phone);

	public Administrator getBySina(String account);

	public Administrator getByDouban(String doubanId);

	/**
	 * 查找所有 friends
	 */
	public List<Administrator> getAllFriends(Long AdministratorId);

	/**
	 * 取消关注好友
	 */
	public boolean cancelCare(Long AdministratorId, long frinndId);

	/**
	 * 关注好友
	 */
	public Administrator care(Long AdministratorId, long frinndId);

}
