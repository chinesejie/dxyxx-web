package org.shiro.demo.service;

import java.util.List;

import org.shiro.demo.entity.Comment;
import org.shiro.demo.entity.CommentReply;

public interface ICommentReplyService extends IBaseService {
	public List<CommentReply> get(Comment comment);

	public boolean updateAndEvit(Comment comment);
}
