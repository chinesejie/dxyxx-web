package org.shiro.demo.service;

import org.shiro.demo.entity.FreeInfo;

public interface IFreeInfoService extends IBaseService {
	// 缓存起来，基本 不修改
	public FreeInfo get(int version);

	public boolean delate(int version);
	// public FreeInfo update(int version);
}
