package org.shiro.demo.service;

import org.shiro.demo.entity.MPWeixinToken;

public interface IMPWeixinTokenService extends IBaseService {

	// public List<NotifyMessage> getAllByAdmin(String adminId);

	// 得到条数 。 注意缓存
	public MPWeixinToken get(int id);

	// 更新消息，顺便更新缓存
	public MPWeixinToken update(int id);

}
