package org.shiro.demo.service;

import org.shiro.demo.entity.NotifyMessage;

public interface INotifyMessageService extends IBaseService {

	// public List<NotifyMessage> getAllByAdmin(String adminId);

	//  get message record amount 
	public int getNumByAdmin(String adminId);

	//  update message status
	public int updateByAdmin(String adminId, NotifyMessage nm);

	//  add message ...only use in project "pay"
	public int save(String adminId, NotifyMessage nm);

}
