package org.shiro.demo.service;

import java.util.List;

import org.shiro.demo.entity.OrderBuffer;

public interface IOrderBufferService extends IBaseService {
	public List<OrderBuffer> getAndCache(String phone, String date);

	public boolean saveAndCache(String phone, String date, OrderBuffer orderBuffer);
}
