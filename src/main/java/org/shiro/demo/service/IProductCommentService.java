package org.shiro.demo.service;

import org.shiro.demo.entity.ProductComment;

public interface IProductCommentService extends IBaseService {
	//public List<ProductComment> getCommentsByProductId(int productId, int page, int count);

	public ProductComment addProductComment(ProductComment productComment);
}
