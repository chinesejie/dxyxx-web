package org.shiro.demo.service;

import org.shiro.demo.entity.Role;

public interface IRoleService extends IBaseService {

	public Role getByName(String name);

}
