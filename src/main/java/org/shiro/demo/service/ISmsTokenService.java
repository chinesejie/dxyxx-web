package org.shiro.demo.service;

import org.shiro.demo.entity.SmsToken;

public interface ISmsTokenService extends IBaseService {

	// id=1
	public SmsToken get(int id);

	// 更新token，顺便更新缓存
	public SmsToken update(int id);

}
