package org.shiro.demo.service;

import java.util.List;

import org.shiro.demo.entity.User;

public interface IUserService extends IBaseService {

	public User getByAccount(String account);

	public boolean register(User user);

	public User getByPhone(String phone);

	public User getBySina(String account);

	public User getByDouban(String doubanId);

	/**
	 * 查找所有 friends
	 */
	public List<User> getAllFriends(Long userId);

	/**
	 * 取消关注好友
	 */
	public boolean cancelCare(Long userId, long frinndId);

	/**
	 * 关注好友
	 */
	public User care(Long userId, long frinndId);

}
