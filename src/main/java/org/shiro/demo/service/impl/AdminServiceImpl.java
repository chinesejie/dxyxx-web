package org.shiro.demo.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.shiro.demo.dao.util.QueryCondition;
import org.shiro.demo.entity.Administrator;
import org.shiro.demo.service.IAdminService;
import org.shiro.demo.service.IBaseService;
import org.springframework.stereotype.Service;

@Service("adminService")
public class AdminServiceImpl extends DefultBaseService implements IAdminService {

	@Resource(name = "baseService")
	private IBaseService baseService;

	public Administrator getByAccount(String account) {
		Administrator Administrator = null;
		try {
			// 1、使用hpql进行查询
			// Administrator = (Administrator)
			// baseService.getUniqueResultByJpql("from Administrator as o where o.account=?",
			// account);

			// 2、借助QueryCondition 进行查询
			QueryCondition queryCondition = new QueryCondition("account", QueryCondition.EQ, account);

			// 3、借助QueryCondition，使用自定义hpql进行查询
			// String hpql = "o.account='" + account + "'";
			// QueryCondition queryCondition = new QueryCondition(hpql);

			List<QueryCondition> queryConditions = new ArrayList<QueryCondition>();
			queryConditions.add(queryCondition);

			Administrator = (Administrator) baseService.getSingleResult(Administrator.class, queryConditions);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return Administrator;
	}

	public boolean register(Administrator Administrator) {
		boolean flag = false;
		try {
			baseService.save(Administrator);
			flag = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}

	public Administrator getByPhone(String phone) {
		Administrator Administrator = null;
		try {

			// 2、借助QueryCondition 进行查询
			QueryCondition queryCondition = new QueryCondition("phone", QueryCondition.EQ, phone);

			List<QueryCondition> queryConditions = new ArrayList<QueryCondition>();
			queryConditions.add(queryCondition);

			Administrator = (Administrator) baseService.getSingleResult(Administrator.class, queryConditions);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return Administrator;
	}

	@Override
	public Administrator getBySina(String sinaId) {
		Administrator Administrator = null;
		try {

			// 2、借助QueryCondition 进行查询
			QueryCondition queryCondition = new QueryCondition("sinaId", QueryCondition.EQ, sinaId);

			List<QueryCondition> queryConditions = new ArrayList<QueryCondition>();
			queryConditions.add(queryCondition);

			Administrator = (Administrator) baseService.getSingleResult(Administrator.class, queryConditions);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return Administrator;
	}

	@Override
	public Administrator getByDouban(String doubanId) {
		Administrator Administrator = null;
		try {

			// 2、借助QueryCondition 进行查询
			QueryCondition queryCondition = new QueryCondition("doubanId", QueryCondition.EQ, doubanId);

			List<QueryCondition> queryConditions = new ArrayList<QueryCondition>();
			queryConditions.add(queryCondition);

			Administrator = (Administrator) baseService.getSingleResult(Administrator.class, queryConditions);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return Administrator;
	}

	/**
	 * 得到关注的frineds 列表
	 */
	// @Override
	// public List<Administrator> getAllFriends(Long AdministratorId) {
	// Administrator Administrator = null;
	// try {
	//
	// // 2、借助QueryCondition 进行查询
	// QueryCondition queryCondition = new QueryCondition("id",
	// QueryCondition.EQ, AdministratorId);
	//
	// List<QueryCondition> queryConditions = new ArrayList<QueryCondition>();
	// queryConditions.add(queryCondition);
	//
	// Administrator = (Administrator)
	// baseService.getSingleResult(Administrator.class, queryConditions);
	//
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// // 懒加载
	// return (List<Administrator>) Administrator.getFriends();
	// }

	@Override
	public boolean cancelCare(Long AdministratorId, long frinndId) {
		Administrator Administrator = null;
		Administrator friend = null;
		try {

			// 2、借助QueryCondition 进行查询
			QueryCondition queryCondition = new QueryCondition("id", QueryCondition.EQ, AdministratorId);

			List<QueryCondition> queryConditions = new ArrayList<QueryCondition>();
			queryConditions.add(queryCondition);

			Administrator = (Administrator) baseService.getSingleResult(Administrator.class, queryConditions);

		} catch (Exception e) {
			e.printStackTrace();
		}
		try {

			QueryCondition queryCondition = new QueryCondition("id", QueryCondition.EQ, frinndId);

			List<QueryCondition> queryConditions = new ArrayList<QueryCondition>();
			queryConditions.add(queryCondition);

			friend = (Administrator) baseService.getSingleResult(Administrator.class, queryConditions);

		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			// Administrator.getFriends().remove(friend);
			baseService.update(Administrator);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;

	}

	@Override
	public Administrator care(Long AdministratorId, long frinndId) {
		Administrator Administrator = null;
		Administrator friend = null;

		try {

			// 2、借助QueryCondition 进行查询
			QueryCondition queryCondition = new QueryCondition("id", QueryCondition.EQ, AdministratorId);

			List<QueryCondition> queryConditions = new ArrayList<QueryCondition>();
			queryConditions.add(queryCondition);

			Administrator = (Administrator) baseService.getSingleResult(Administrator.class, queryConditions);

		} catch (Exception e) {
			e.printStackTrace();
		}
		try {

			QueryCondition queryCondition = new QueryCondition("id", QueryCondition.EQ, frinndId);

			List<QueryCondition> queryConditions = new ArrayList<QueryCondition>();
			queryConditions.add(queryCondition);

			friend = (Administrator) baseService.getSingleResult(Administrator.class, queryConditions);

		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			// Administrator.getFriends().add(friend);
			baseService.update(Administrator);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return friend;

	}

	@Override
	public List<Administrator> getAllFriends(Long AdministratorId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Administrator getByName(String name) {
		Administrator Administrator = null;
		try {
			// 1、使用hpql进行查询
			// Administrator = (Administrator)
			// baseService.getUniqueResultByJpql("from Administrator as o where o.account=?",
			// account);

			// 2、借助QueryCondition 进行查询
			QueryCondition queryCondition = new QueryCondition("name", QueryCondition.EQ, name);

			// 3、借助QueryCondition，使用自定义hpql进行查询
			// String hpql = "o.account='" + account + "'";
			// QueryCondition queryCondition = new QueryCondition(hpql);

			List<QueryCondition> queryConditions = new ArrayList<QueryCondition>();
			queryConditions.add(queryCondition);

			Administrator = (Administrator) baseService.getSingleResult(Administrator.class, queryConditions);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return Administrator;
	}

}
