package org.shiro.demo.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.shiro.demo.dao.util.QueryCondition;
import org.shiro.demo.entity.Comment;
import org.shiro.demo.entity.CommentReply;
import org.shiro.demo.entity.OrderBuffer;
import org.shiro.demo.service.IBaseService;
import org.shiro.demo.service.ICommentReplyService;
import org.shiro.demo.service.IOrderBufferService;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service("commentReplyService")
public class CommentReplyServiceImpl extends DefultBaseService implements ICommentReplyService {
	@Resource(name = "baseService")
	private IBaseService baseService;
	DateTimeFormatter dayFormat = DateTimeFormat.forPattern("yyyy-MM-dd");

	/**
	 * 根据 date跟 phone 找到 所有的 orderreply
	 * 
	 * @param phone
	 * @param date
	 * @return
	 */
	@Cacheable(value = "reply", key = "#comment.getId()")
	public List<CommentReply> get(Comment comment) {
		String[] arrs = comment.getReplyIds().split(";");
		Long[] list = new Long[arrs.length];

		for (int i = 0; i < arrs.length; i++) {
			list[i] = Long.parseLong(arrs[i]);
		}
		List<CommentReply> replys = baseService.getByIds(CommentReply.class, list);
		return replys;
	}

	/**
	 * 更新之后， 删除reply 缓存
	 * 
	 * @param phone
	 * @param date
	 * @param orderBuffer
	 * @return
	 */
	@CacheEvict(value = "reply", key = "#comment.getId()")
	public boolean updateAndEvit(Comment comment) {
		try {
			baseService.update(comment);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

}
