package org.shiro.demo.service.impl;

import javax.annotation.Resource;

import org.shiro.demo.entity.FreeInfo;
import org.shiro.demo.service.IBaseService;
import org.shiro.demo.service.IFreeInfoService;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service("freeInfoService")
public class FreeInfoServiceImpl extends DefultBaseService implements IFreeInfoService {
	@Resource(name = "baseService")
	private IBaseService baseService;

	// 使用了一个缓存名叫 freeinfo
	@Cacheable("freeinfo")
	@Override
	public FreeInfo get(int version) {
		return baseService.getById(FreeInfo.class, version);
	}

	// 清空
	@CacheEvict(value = "freeinfo", key = "#version")
	@Override
	public boolean delate(int version) {
		try {
			baseService.delete(FreeInfo.class, version);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

}
