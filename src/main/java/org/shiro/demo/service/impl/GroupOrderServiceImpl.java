package org.shiro.demo.service.impl;

import javax.annotation.Resource;

import org.shiro.demo.dao.IBaseDao;
import org.shiro.demo.entity.Group;
import org.shiro.demo.entity.GroupOrder;
import org.shiro.demo.service.IGroupOrderService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service("groupOrderService")
public class GroupOrderServiceImpl extends DefultBaseService implements IGroupOrderService {
	@Resource(name = "baseDao")
	private IBaseDao baseDao;

	/**
	 * 事务添加订单，更新存货
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public boolean addGrouOrderWithMinusAmount(GroupOrder groupOrder) {

		try {
			Group group = groupOrder.getGroup();
			group.setBalance(group.getBalance() - 1);
			baseDao.update(group);
			baseDao.save(groupOrder);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}

}
