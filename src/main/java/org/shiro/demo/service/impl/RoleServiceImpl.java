package org.shiro.demo.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.shiro.demo.dao.util.QueryCondition;
import org.shiro.demo.entity.Role;
import org.shiro.demo.service.IBaseService;
import org.shiro.demo.service.IRoleService;
import org.springframework.stereotype.Service;

@Service("roleService")
public class RoleServiceImpl extends DefultBaseService implements IRoleService {

	@Resource(name = "baseService")
	private IBaseService baseService;

	public Role getByName(String name) {
		Role role = null;
		try {
			// 1、使用hpql进行查询
			// user = (User)
			// baseService.getUniqueResultByJpql("from User as o where o.account=?",
			// account);

			// 2、借助QueryCondition 进行查询
			QueryCondition queryCondition = new QueryCondition("name", QueryCondition.EQ, name);

			// 3、借助QueryCondition，使用自定义hpql进行查询
			// String hpql = "o.account='" + account + "'";
			// QueryCondition queryCondition = new QueryCondition(hpql);

			List<QueryCondition> queryConditions = new ArrayList<QueryCondition>();
			queryConditions.add(queryCondition);

			role = (Role) baseService.getSingleResult(Role.class, queryConditions);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return role;
	}
}
