package org.shiro.demo.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.shiro.demo.dao.util.QueryCondition;
import org.shiro.demo.entity.User;
import org.shiro.demo.service.IBaseService;
import org.shiro.demo.service.IUserService;
import org.springframework.stereotype.Service;

@Service("userService")
public class UserServiceImpl extends DefultBaseService implements IUserService {

	@Resource(name = "baseService")
	private IBaseService baseService;

	public User getByAccount(String account) {
		User user = null;
		try {
			// 1、使用hpql进行查询
			// user = (User)
			// baseService.getUniqueResultByJpql("from User as o where o.account=?",
			// account);

			// 2、借助QueryCondition 进行查询
			QueryCondition queryCondition = new QueryCondition("account", QueryCondition.EQ, account);

			// 3、借助QueryCondition，使用自定义hpql进行查询
			// String hpql = "o.account='" + account + "'";
			// QueryCondition queryCondition = new QueryCondition(hpql);

			List<QueryCondition> queryConditions = new ArrayList<QueryCondition>();
			queryConditions.add(queryCondition);

			user = (User) baseService.getSingleResult(User.class, queryConditions);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return user;
	}
	
 
	 

	public boolean register(User user) {
		boolean flag = false;
		try {
			baseService.save(user);
			flag = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}

	public User getByPhone(String phone) {
		User user = null;
		try {

			// 2、借助QueryCondition 进行查询
			QueryCondition queryCondition = new QueryCondition("phone", QueryCondition.EQ, phone);

			List<QueryCondition> queryConditions = new ArrayList<QueryCondition>();
			queryConditions.add(queryCondition);

			user = (User) baseService.getSingleResult(User.class, queryConditions);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return user;
	}

	@Override
	public User getBySina(String sinaId) {
		User user = null;
		try {

			// 2、借助QueryCondition 进行查询
			QueryCondition queryCondition = new QueryCondition("sinaId", QueryCondition.EQ, sinaId);

			List<QueryCondition> queryConditions = new ArrayList<QueryCondition>();
			queryConditions.add(queryCondition);

			user = (User) baseService.getSingleResult(User.class, queryConditions);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return user;
	}

	@Override
	public User getByDouban(String doubanId) {
		User user = null;
		try {

			// 2、借助QueryCondition 进行查询
			QueryCondition queryCondition = new QueryCondition("doubanId", QueryCondition.EQ, doubanId);

			List<QueryCondition> queryConditions = new ArrayList<QueryCondition>();
			queryConditions.add(queryCondition);

			user = (User) baseService.getSingleResult(User.class, queryConditions);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return user;
	}

	/**
	 * 得到关注的frineds 列表
	 */
//	@Override
//	public List<User> getAllFriends(Long userId) {
//		User user = null;
//		try {
//
//			// 2、借助QueryCondition 进行查询
//			QueryCondition queryCondition = new QueryCondition("id", QueryCondition.EQ, userId);
//
//			List<QueryCondition> queryConditions = new ArrayList<QueryCondition>();
//			queryConditions.add(queryCondition);
//
//			user = (User) baseService.getSingleResult(User.class, queryConditions);
//
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		// 懒加载
//		return (List<User>) user.getFriends();
//	}

	@Override
	public boolean cancelCare(Long userId, long frinndId) {
		User user = null;
		User friend = null;
		try {

			// 2、借助QueryCondition 进行查询
			QueryCondition queryCondition = new QueryCondition("id", QueryCondition.EQ, userId);

			List<QueryCondition> queryConditions = new ArrayList<QueryCondition>();
			queryConditions.add(queryCondition);

			user = (User) baseService.getSingleResult(User.class, queryConditions);

		} catch (Exception e) {
			e.printStackTrace();
		}
		try {

			QueryCondition queryCondition = new QueryCondition("id", QueryCondition.EQ, frinndId);

			List<QueryCondition> queryConditions = new ArrayList<QueryCondition>();
			queryConditions.add(queryCondition);

			friend = (User) baseService.getSingleResult(User.class, queryConditions);

		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			//user.getFriends().remove(friend);
			baseService.update(user);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;

	}

	@Override
	public User care(Long userId, long frinndId) {
		User user = null;
		User friend = null;

		try {

			// 2、借助QueryCondition 进行查询
			QueryCondition queryCondition = new QueryCondition("id", QueryCondition.EQ, userId);

			List<QueryCondition> queryConditions = new ArrayList<QueryCondition>();
			queryConditions.add(queryCondition);

			user = (User) baseService.getSingleResult(User.class, queryConditions);

		} catch (Exception e) {
			e.printStackTrace();
		}
		try {

			QueryCondition queryCondition = new QueryCondition("id", QueryCondition.EQ, frinndId);

			List<QueryCondition> queryConditions = new ArrayList<QueryCondition>();
			queryConditions.add(queryCondition);

			friend = (User) baseService.getSingleResult(User.class, queryConditions);

		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			//user.getFriends().add(friend);
			baseService.update(user);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return friend;

	}

	@Override
	public List<User> getAllFriends(Long userId) {
		// TODO Auto-generated method stub
		return null;
	}

}
