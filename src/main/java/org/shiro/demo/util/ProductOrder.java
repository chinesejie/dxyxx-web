package org.shiro.demo.util;

public enum ProductOrder {
	newest("modifyDate DESC"),
	hot("amount DESC"),
	highprice("price DESC"),
	lowerprice("price ASC");
	
	private String text;
	
	private ProductOrder(String text) {
		this.text=text;
	}


	public String getText() {
		return text;
	}
	
}
