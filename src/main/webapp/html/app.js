// create the module and name it scotchApp
// 模块加入时要注意

var zimgeUrl='';
var uploadUrl='';
var getImageInfo='';

var scotchApp = angular.module('scotchApp', 
		['PaginatorService','PaginatorRRService','dateTimePicker','frapontillo.bootstrap-switch',
		 'scotchApp.directives',
                 'ngRoute','angularFileUpload','ConfigService']);

// configure our routes
scotchApp.config(function($routeProvider,$configProvider  ) {
	zimgeUrl = $configProvider.zimgeUrl;
	uploadUrl = $configProvider.uploadUrl;
	getImageInfo = $configProvider.getImageInfo;
	$routeProvider
	
	.when('/user',{
		templateUrl : 'user/user.html',
		controller : UserController
	})
	.when('/wuser',{
		templateUrl : 'wuser/wuser.html',
		controller : WuserController
	})
	.when('/adminInfo',{
		templateUrl : 'adminInfo/adminInfo.html',
		controller : AdminInfoController
	})

	.when('/admin',{
		templateUrl : 'admin/admin.html',
		controller : AdminController
	})
	
	// 路径是小写的  templateUrl不需要/
	.when('/productcategory', {
		templateUrl : 'productcategory/productcategory.html',
		controller :   ProductCategoryController
	})
	
	.when('/product',{
		templateUrl : 'product/product.html',
		controller : ProductController
	})

	// route for the home page
	.when('/', {
		// url：路径是以当前的页面为基准的相对路径
		templateUrl : 'index.html',
		controller : IndexController
	})

	// route for the about page
	.when('/help', {
		templateUrl : 'help/layout.html',
		controller : HelpController
	})

	// route for the contact page
	.when('/contact', {
		templateUrl : 'contact.html',
		controller : 'contactController'
	}).when('/homebanner', {
		templateUrl : 'homebanner/homebanner.html',
		controller : HomeBannerController
	})

	.when('/adbanner', {
		templateUrl : 'Adbanner/adbanner.html',
		controller : AdBannerController
	})

	.when('/topbanner', {
		templateUrl : 'topbanner/topbanner.html',
		controller : TopBannerController
	
	}).when('/dailyspecial', {
		templateUrl : 'dailyspecial/dailySpecial.html',
		controller : DailySpecialController
	})
	
	.when('/error', {
		templateUrl : '404.html'
	})
	/** 店长 */
	.when('/manager', {
		templateUrl : 'manager/manager.html',
		controller : ManagerController
	})

	.when('/manageHandledOrder', {
		templateUrl : 'manager/managerHandledOrder.html',
		controller : ManageHandledOrderController

	})

	// manager
	.when('/tickets', {
		templateUrl : 'tickets/tickets.html',
		controller : TicketsController
	})

	.when('/groups', {
		templateUrl : 'manager/deliveryGroup.html',
		controller : DeliveryGroupController
	})

	.when('/discount', {
		templateUrl : 'admin_discount/AdminDiscount.html',
		controller : AdminDiscountController
	})

	/** 订单 */
	.when('/AdminOrderOrder', {
		templateUrl : 'order/Order.html',
		controller : AdminOrderController
	})

	.when('/AdminTicketOrder', {
		templateUrl : 'order/Ticket.html',
		controller : AdminTicketOrderController
	})

	.when('/AdminGroupOrder', {
		templateUrl : 'order/Group.html',
		controller : AdminGroupOrderController
	})
	
	.when("/dailyStatistics",{
		templateUrl : 'orderstatistics/dailyStatistics.html',
		controller : dailyStatisticsController
	})
	
	.when("/monthStatistics",{
		templateUrl : 'orderstatistics/MonthStatistics.html',
		controller : MonthStatisticsController
	})

	// 店长查看自己的店
	.when('/store', {
		templateUrl : 'store/store.html',
		controller : storeController
	})

	.when('/version', {
		templateUrl : 'version/version.html',
		controller : VersionController
	})
	
	.when('/push', {
		templateUrl : 'push/push.html',
		controller : PushController
	})
	
	.when('/message',{
		templateUrl : 'message/myMessage.html',
		controller : MessageController
	})

	.when('/groupControl', {
		templateUrl : 'admin_group/adminGroup.html',
		controller : AdminGroupController
	})

	.when('/adminStore', {
		templateUrl : 'admin_store/adminStore.html',
		controller : adminStoreController
	});

});

// create the controller and inject Angular's $scope
scotchApp.controller('mainController', function($scope, $http,$timeout) {
	$scope.showBar = false;

	$http.get("../user/look").success(function(response) {
		if (response.code == 1) {
			$scope.showBar = true;
			$scope.phone = response.object.phone;
		} else {
			$scope.showBar = false;
			window.location.href = "admin-login.html";
		}
	});
	

	$scope.updateMessageCount = function() {
		$http.get("../message/getNum").success(function(response){
			if(response.object!=0 && response.code==1){
				$scope.messageCount = response.object;
			}else if(response.object ==0 && response.code==1){
				$scope.messageCount="";
			}
		}).error();
		$timeout($scope.updateMessageCount, 120000);
	}
	
	$scope.updateMessageCount();

	$scope.logout = function() {
		$http.get("../user/logout").success(function(response) {

			if (response.code == 0) {
				sweetAlert(response.info);
				return;
			} else {

				window.location.href = "admin-login.html";
			}
		});
	}

});

scotchApp.controller('aboutController', function($scope) {
	$scope.message = 'Look! I am an about page.';
});

scotchApp.controller('contactController', function($scope) {
	$scope.message = 'Contact us! JK. This is just a demo.';
});