/**
 * 
 */
var cameraApp = angular.module('cameraApp', [ 'ngRoute' ]);

// configure our routes
cameraApp.config(function($routeProvider) {
	$routeProvider
	
	.when('/barcodeOrders',{
		templateUrl :'barcode2Order.html',
		controller : barcode2OrdersController
	})

	.when('/barcodeScanner', {
		templateUrl : 'barcodeScanner.html',
		controller : barcodeScannerController
	});

});
