var AdminController = function($scope, $http,$upload,$log) {

	$scope.admin = null;
	$scope.id = null;
	$scope.roles = null;
	$scope.rolesChecked = null;
	$scope.roleIds = null;
	
	$scope.baseUrl = zimgeUrl;
	$scope.formatImage = "?p=0";

	$("#roles_ui").attr('name', 'name');
	$("#edit_ui").attr('name', 'name');

	$scope.nextPage = function(page) {
		$scope.findAdmins(page.pageSize, page.nextPage, 'id', true);
	}

	$scope.prePage = function(page) {
		$scope.findAdmins(page.pageSize, page.prePage, 'id', true);
	}

	// 分页查询
	$scope.findAdmins = function(pageSize, pageNumber, orderBy, asc) {
		// alert(pageSize +' '+pageNumber +' '+orderBy+' ' +asc);
		var $url = '../admin/findAdmins?pageSize=' + pageSize + '&pageNumber=' + pageNumber + '&orderBy=' + orderBy + '&asc=' + asc;

		$http.get($url).success(function(response) {

			$scope.page = null;
			$scope.page = response;
		});
	}

	// 删除
	$scope.deleteById = function(admin) {
		// console.log(admin);
		var $url = '../admin/deleteById?id=' + admin.id;
		$http.get($url).success(function(response) {
			if (response.code == 0) {
				sweetAlert(response.info);
			} else {
				sweetAlert("删除成功");
				$scope.refreshPage();
			}
		});

	}

	// 获取角色列表
	$scope.getRoles = function() {
		// console.log('getRoles is invoked...');
		$url = '../admin/getRoles';
		$http.get($url).success(function(response) {
			$scope.roles = response['object'];
		});
	}

	// 显示更新的ui并且准备数据
	$scope.showEdit = function(admin) {
		$scope.admin = null;
		$scope.admin = admin;
		$("#edit_ui").offCanvas('open');
	}

	// 更新
	$scope.updateInfo = function(admin) {
		// admin.birthdayStr = $("#birthday_input").val();
		// admin.birthday = admin.birthdayStr;
		$http.post('../admin/updateInfo', admin).success(function() {
			console.log("update success");
			$scope.closeEditUI();
		});
	}

	// 显示设置权限的UI并且准备据角色数据以及角色的回显.
	$scope.showSetRoles = function(admin) {
		// console.log('the admin to update is :' + admin);

		$scope.id = admin.id;

		$scope.roleIds = new Array();

		$url = '../admin/getRoles';

		$http.get('../admin/getRolesById?id=' + $scope.id).success(function(data) {
			for (var i = 0; i < data.length; i++) {
				$scope.roleIds.push(data[i].id);
			}
		}).then(function() {
			$http.get($url).success(function(response) {
				
				
				$scope.roles = response['object'];
				$log.info($scope.roles);
				
				// 删除顾客权限
				$scope.roles.splice( 7,1 );

				if ($scope.roles != null) {
					$scope.rolesChecked = new Array();
					for (var i = 0; i < $scope.roles.length; i++) {
						if ($scope.roles[i].id == 8) {
							// 删除顾客权限
							$scope.roles.splice(i, 1);
						}

						if ($scope.roleIds.indexOf($scope.roles[i].id) > -1) {
							$scope.rolesChecked.push(true);
						} else {
							$scope.rolesChecked.push(false);
						}
					}
				}

			});
		});

		$("#roles_ui").offCanvas('open');
	}

	// 设置权限
	$scope.setRoles = function() {
		var params = $("#roles_ui").find("form").serialize();
		$url = "../admin/setRoles?id=" + $scope.id + '&' + params;
		$http.get($url).success(function(response) {
			// console.log('success');
			$scope.closeRolesUI();
		});
		// console.log('setRoles: the roleIds is : ' + $scope.roleIds);
	}

	// 显示新建管理员的UI
	$scope.newAdminUI = function() {
		$scope.admin ={'image':""};
		$("#new_ui").offCanvas('open');
	}

	// 新建立管理员
	$scope.newAdmin = function(admin) {

		if (admin == null) {
			alert('null');
			return;
		}
		// admin.birthdayStr = $("#birthday_input").val();
		// admin.birthday = admin.birthdayStr;
		$http.post('../admin/save', admin).success(function(response) {
			
			if(response.code==0){
				sweetAlert("新增失败");
				$("#edit_ui").attr('name', '');
				$("#edit_ui").offCanvas('close');
				return;
			}else{
				sweetAlert("新增成功");
				$scope.closeNewUI();
			}
			
		});

	}
	$scope.closeNewUI = function() {
		$("#new_ui").attr('name', '');
		$("#new_ui").offCanvas('close');
		$scope.refreshPage();
	}

	// 关闭设置权限的侧边栏并且刷新页面
	$scope.closeRolesUI = function() {
		$("#roles_ui").attr('name', '');
		$("#roles_ui").offCanvas('close');
		$scope.refreshPage();
	}

	// 关闭编辑侧边栏并且刷新页面
	$scope.closeEditUI = function() {
		$("#edit_ui").attr('name', '');
		$("#edit_ui").offCanvas('close');
		$scope.refreshPage();
	}

	// 刷新
	$scope.refreshPage = function() {
		$scope.findAdmins($scope.page.pageSize, $scope.page.currentPage, "id", true);
		$("#roles_ui").attr('name', 'name');
		$("#edit_ui").attr('name', 'name');
	}
	
	$scope.newAdminImage = function($files) {
		// $files: an array of files selected, each file has name, size,
		// and type.
		for (var i = 0; i < $files.length; i++) {
			var file = $files[i];

			$scope.upload = $upload.http({
				url : uploadUrl,
				headers : {
					'Content-Type' : file.type.split("/")[1]
				},
				data : file,
			}).progress(function(evt) {
				// console.log('percent: ' + parseInt(100.0 *
				// evt.loaded / evt.total));
			}).success(function(data) {
				
				$scope.admin.image = data['info']['md5'];
				
			}).error(function(data) {
				// error
				sweetAlert("上传图片失败");
			});

		}
	}


	$scope.findAdmins(10, 1, "id", true);

}
