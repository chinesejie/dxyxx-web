/**
 * 
 */
var AdminGroupController = function($http, $scope, $log) {
	$scope.baseUrl = zimgeUrl;
	$scope.formatImage = "?w=200&h=200";
	$scope.open = "open";
	$scope.images = [];

	var data = new Array();
	var category = new Array();
	var map = {};

	$scope.getProductByid = function(categoryId) {
		$http.get('../group/listbycid?categoryId=' + categoryId).success(function(response) {
			if (response == []) {
				return;
			}

//			$log.info(map[categoryId], response);

			$scope.products[map[categoryId]] = [];

			for (var i = 0; i < response.length; i++) {
				var tempGroups;
				var tempValue = [];
				var groups = [];

				if (response[i].groups == null) {
					$scope.products[map[categoryId]].push({
						"product" : response[i].product,
						"groups" : null
					});
					continue;
				}

				tempGroups = response[i].groups.split(";");

				for (var j = 0; j < tempGroups.length; j++) {
					tempValue = tempGroups[j].split("_");
					var tempGroup = {};
//					$log.info(tempValue);
					tempGroup["id"] = tempValue[0];
					tempGroup["personAmount"] = tempValue[1];
					if (tempValue[2] == "true") {
						tempGroup['status'] = true;
					} else {
						tempGroup['status'] = false;
					}
					tempGroup["version"] = tempValue[3];
					groups.push(new Object(tempGroup));
				}

				// $log.info(i, j, "groups", groups);

				$scope.products[map[categoryId]].push({
					"product" : response[i].product,
					"groups" : groups
				});

			}

		});
	}

	$scope.getAllProductCategory = function() {
		$http.get('../category/list').success(function(response) {
			$scope.productCategorys = response;
			// 这个是有问题的，问题出在如果后面的array太大会有异常
			data.push.apply(data, response);
			$scope.products = new Array([ data.length ]);
			for (var i = 0; i < data.length; i++) {
				// 这个地方i的变化是递增的，也就是有序的，而data[i].id因为异步的原因可能顺序会乱
				map[data[i].id] = i;
				$scope.getProductByid(data[i].id);
			}
		});
	}

	/*
	 * 按照产品的id去拿到产品信息
	 */
	$scope.getProduct = function(id) {

		$http.get('../product/get?id=' + id).success(function(response) {

			$scope.nproduct = response;
			if ($scope.nproduct.imageIds)
				$scope.images = $scope.nproduct.imageIds.split(";");

		});

	}

	/**
	 * 由于类别会多出一个length属性，加个filter
	 */
	$scope.filterProduct = function(item) {

		if (angular.isDefined(item.product)) {
			return item;
		}
	}

	/**
	 * bootStrap Switch
	 */
	$scope.changeStatus = function(status, id) {
		var urlPath;
		var postData = {
			"groupId" : id
		};

		$log.info(status);

		if (status) {
			urlPath = "../group/activate";
		} else {
			urlPath = "../group/deactivate";
		}
		$http({
			method : "post",
			url : urlPath,
			data : $.param(postData),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(response) {
			$log.info(response);
			if (response.code == 1) {
				sweetAlert("修改成功");
			} else {
				sweetAlert(response.info);
			}
		});
	}

	/**
	 * for single picture
	 */
	$scope.pass = function(thumbId) {

		$scope.$parent.singlepicture = $scope.baseUrl + thumbId + "?p=0";
	}

	// new group
	$scope.add = function(item) {
		$scope.ngroup = "";
		$scope.nproduct = item.product;
		$("#birthday_input1").val("请点击选择");
	}

	$scope.submitdata = function(ngroup) {
		// $log.info(ngroup);
		var urlPath;
		var postData;

		urlPath = "../group/add";
		postData = {
			"productId" : $scope.nproduct.id,
			"balance" : ngroup.balance,
			"personPrice" : ngroup.personPrice,
			"deadline" : $("#birthday_input1").val(),
			"image" : $scope.nproduct.thumbId,
			"personAmount" : ngroup.personAmount,
			"description" : ngroup.description
		}

		$http({
			method : "post",
			url : urlPath,
			data : $.param(postData),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}

		}).success(function(response) {
			if (response.code == 1) {
				sweetAlert("修改成功");
				$scope.getProductByid($scope.nproduct.category.id);
			} else {
				sweetAlert(response.info);
			}
		}).error();
	}

	$scope.goUpdate = function(id) {
		$http.get("../group/detailsByGid?groupId=" + id).success(function(response) {
			$log.info(response);
			if (response.code == 1) {
				$scope.updateGroup = response.object;
				$scope.minVersion = $scope.updateGroup.version;
			} else {
				sweetAlert(response.info);
				return;
			}
		}).error();
	}

	$scope.update = function(updateGroup, pristine) {
		$log.info(pristine);
		var postData;
		if (pristine && $("#birthday_input").val() == "") {
			sweetAlert("未做任何修改");
			return;
		}
		
		if($scope.minVersion > updateGroup.version){
			sweetAlert("版本号过低");
			return;
		}

		if ($("#birthday_input").val() != "") {
			updateGroup.deadline = $("#birthday_input").val();
		} else {
			updateGroup.deadline = $("#birthday_input").attr('placeholder');
		}

		postData = {
			"groupId" : updateGroup.id,
			"deadline" : updateGroup.deadline,
			"balance" : updateGroup.balance,
			"personPrice" : updateGroup.personPrice,
			"version" : updateGroup.version
		};

		$http({
			method : "post",
			url : "../group/modify",
			data : $.param(postData),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(response) {
			if (response.code == 1) {
				sweetAlert("修改成功");
				// 无必要刷新
				$scope.getProductByid(updateGroup.product.category.id);
			} else {
				sweetAlert(response.info);
				return;
			}
		}).error();
	}

	$scope.getAllProductCategory();

}