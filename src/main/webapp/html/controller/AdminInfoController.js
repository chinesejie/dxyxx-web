var AdminInfoController = function($scope, $http,$upload,$log) {
	$scope.admin = null;
	$scope.birthday = '';
	
	$scope.baseUrl = zimgeUrl;
	$scope.formatImage = "?p=0";

	// getInfo
	$scope.getAdminInfo = function() {
		$http.get('../adminInfo/getInfo').success(function(response) {
			if (response.code == 0) {
				window.location.href = "admin-login.html";
				return;
			}
			
			$log.info(response);
			$scope.admin = response;
			$scope.birthday = response.birthday;
			// console.log(response.birthday);
		});
	}

	// update
	$scope.updateInfo = function(admin) {

		if ($("#birthday_input").val() != "") {
			admin.birthday = $("#birthday_input").val();
		} else {
			admin.birthday = $("#birthday_input").attr('placeholder');
		}

		var data = {
			"id" : admin.id,
			"name" : admin.name,
			"sex" : admin.sex,
			"birthday" : admin.birthday,
			"job" : admin.job,
			"image" : admin.image,
			"phone" : admin.phone,
			"address" : admin.address,
			"income" : admin.income
		}
		$http({
			method : 'POST',
			url : '../adminInfo/updateInfo',
			// params : $scope.formdata, 用data传数据可以解决乱码问题
			data : $.param(data), // pass in data as strings
			// 'accept-charset':'utf-8' 这个不需要
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		// set the headers so angular passing info as form data (not
		// request payload)
		}).success(function(response) {
			if (response['code'] == 1) {
				sweetAlert("修改成功");
				$scope.getAdminInfo();
			} else
				sweetAlert("修改失败");
		});
	}
	
	
	/**
	 * 上传文件
	 */
	$scope.onFileSelect = function($files) {
		// $files: an array of files selected, each file has name, size,
		// and type.
		for (var i = 0; i < $files.length; i++) {
			var file = $files[i];
			var image = new Image();

			$scope.upload = $upload.http({
				url : uploadUrl,
				headers : {
					'Content-Type' : file.type.split("/")[1]
				},
				data : file,
			}).progress(function(evt) {
				// console.log('percent: ' + parseInt(100.0 *
				// evt.loaded / evt.total));
			}).success(function(data) {

				$scope.admin.image = data['info']['md5'];
			}).error(function(data) {
				// error
				sweetAlert("上传图片失败");
			});

		}
	}
	
	/**
	 * for single picture
	 */
	$scope.pass = function(thumbId) {

		$scope.$parent.singlepicture = $scope.baseUrl + thumbId + "?p=0";
	}

	$scope.getAdminInfo();
}
