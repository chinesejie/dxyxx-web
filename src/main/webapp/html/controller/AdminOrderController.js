/**
 * 超级仪表盘
 */

var AdminOrderController = function($http, $scope, Paginator) {
	// console.log(Paginator);
	$scope.pageSize = 10;
	$scope.recordCount = [];
	$scope.search = [];
	$scope.showOperatorDetail = false;
	$scope.operator = "";

	var fetchFunction = function(currentPage, limit, propId, callback) {
		$http.get('../order/adminOrders', {
			params : {
				'pageSize' : limit,
				'pageNumber' : currentPage,
				'propId' : propId
			}
		}).success(callback);
	}
	$scope.search[1] = Paginator(fetchFunction, $scope.pageSize, 1);

	$scope.getTab = function(propId) {
		$scope.search[propId] = Paginator(fetchFunction, $scope.pageSize, propId);
	}

	/**
	 * 查看页面
	 */
	$scope.edit = function(obj) {

		if (obj.orderProp.id == 3) {
			obj.showOperator = true;

		} else {
			obj.showOperator = false;
		}

		$scope.norder = obj;
		$scope.showOperatorDetail = false;
		$scope.operator = "";
		$("#new_ui").offCanvas('open');
	}

	/**
	 * 关闭侧边栏
	 */
	$scope.closeNew = function() {

		$scope.showOperator = false;
		$("#new_ui").offCanvas('close');
		$("#edit_ui").offCanvas('close');
	}

	$scope.showOperatorDetailStatus = function(operatorPhone) {
		$scope.showOperatorDetail = !$scope.showOperatorDetail;
		if ($scope.operator == "") {
			$http.get("../admin/getOperator?phone=" + operatorPhone).success(function(response) {
				$scope.operator = response.object;
			});
		}
	}
}