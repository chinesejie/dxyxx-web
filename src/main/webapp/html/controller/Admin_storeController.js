/**
 * updateStore:新增店长；updatedStore:修改店
 */
var adminStoreController = function($http, $scope,$upload, $log) {

	$scope.updateStore = "";
	var map = {};
	$scope.open="open";
	
	$scope.nstore={'image':null};
	
	$scope.baseUrl = zimgeUrl;
	$scope.formatImage = "?p=0";
	
	$scope.start = function() {
		$http.get('../store/list').success(function(response) {
			var data = [];
//			$log.info(response);
			
			if(map!={}){
				map={};
			}
			
			$scope.stores = response;
			data.push.apply(data, response);

			$scope.managers = new Array([ data.length ]);
			
			for (var i = 0; i < data.length; i++) {
				map[data[i].id] = i;
				$scope.getManager(data[i].id);
			}
//			$log.warn("managers",$scope.managers);
		}).error();
	}

	$scope.getManager = function(StoreId) {

		$http.get('../store/geta?id=' + StoreId).success(function(response) {
//			$log.info("manager",response);
			$scope.managers[map[StoreId]] = response;
		}).error();

	}

	$scope.DeleteManager = function(mid,id) {
//		$log.info("待删除的店长的店：",id);
		
		$http.get( "../store/deleteManager?mid="+mid+"&id="+id).success(function(response) {
//			$log.info("删除结果：",response);
			$scope.getManager(id);
		}).error();
	}
	
	//进入新增页面
	$scope.add = function(store) {

		$scope.updateStore = store;
		$scope.managerSelected="";
//		$log.info("进入新增", store);
		// 后面要改成table
		$http.get('../admin/findAdmins?pageNumber=1&pageSize=150&orderBy=id&asc=true').success(function(data) {
			$scope.managerForSelect = data.recordList;
			
		}).error();
	}
	
	//关闭侧边栏
	$scope.close = function() {

		$("#doc-oc-demo3").offCanvas('close');
	}
	
	$scope.ToAddStore=function(){
		$scope.nstore={'image':''};
		$log.info($scope.nstore);
	}
	
	$scope.closeEditUI=function(){
		
		$("#product-detail")[0].attributes.name.value = "";
		$("#product-detail").offCanvas('close');
		$("#product-detail")[0].attributes.name.value = "open";
	}
	
	
	$scope.addManager = function() {
		if(angular.isUndefined($scope.managerSelected) || $scope.managerSelected==null || $scope.managerSelected==""){
			sweetAlert("请选择店长");
			return;
		}
//		$log.info("$scope.updateStore",$scope.updateStore.id);
		var postData = {
			"id" : $scope.updateStore.id,
			"managerId" : $scope.managerSelected.id,
		};
		$http({
			method : "post",
			url : "../store/addmanager",
			data : $.param(postData),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(response) {
			
			if(response.code==0){
				sweetAlert(response.info);
				
				return;
			}
//			$scope.updateStore = "";
			sweetAlert("添加成功");
//			$scope.start();
			$scope.getManager($scope.updateStore.id);
		}).error();
	}

	$scope.addStore = function(nstore) {
		
		if(angular.isUndefined(nstore.address) || nstore.address==null || nstore.address==""){
			sweetAlert("请添加地址");
			return;
		}
		
		if(angular.isUndefined(nstore.name) ||nstore.name==null || nstore.name==""){
			sweetAlert("请添加店名");
			return;
		}
		
		var postData = {
				"address" : nstore.address,
				"name" : nstore.name,
				"image" : nstore.image
			};
		
		$http({
			method : "post",
			url : "../store/add",
			data : $.param(postData),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(response) {
			if(response.code==0){
				sweetAlert("添加失败 ",response.info);
				return;
			}
			sweetAlert("添加成功");
			$scope.start();
			$scope.closeEditUI();
		}).error();
	}
	
	$scope.showUpdateStore=function(store){
		$scope.updatedStore=store;
		$log.info($scope.updatedStore);
	}
	
	$scope.closeEditUI2=function(){
		$scope.updatedStore="";
		
		$("#edit-store")[0].attributes.name.value = "";
		$("#edit-store").offCanvas('close');
		$("#edit-store")[0].attributes.name.value = "open";

	}
	
	$scope.update=function(updatedStore){
		
		$http.post("../store/update",updatedStore).success(function(response){
			if(response.code==0){
				sweetAlert(response.info);
				$scope.closeEditUI2();
				return;
			}else{
				sweetAlert("更新成功");
				$scope.closeEditUI2();
			}
		}).error();
	}
	
	
	/**
	 * 上传文件
	 */
	$scope.onFileSelect = function($files) {
		// $files: an array of files selected, each file has name, size,
		// and type.
		for (var i = 0; i < $files.length; i++) {
			var file = $files[i];
			var image = new Image();

			$scope.upload = $upload.http({
				url : uploadUrl,
				headers : {
					'Content-Type' : file.type.split("/")[1]
				},
				data : file,
			}).progress(function(evt) {
				// console.log('percent: ' + parseInt(100.0 *
				// evt.loaded / evt.total));
			}).success(function(data) {
				
				$scope.nstore.image = data['info']['md5'];
			}).error(function(data) {
				// error
				sweetAlert("上传图片失败");
			});

		}
	}
	
	$scope.onFileSelectUpdateStore = function($files) {
		for (var i = 0; i < $files.length; i++) {
			var file = $files[i];
			var image = new Image();

			$scope.upload = $upload.http({
				url : uploadUrl,
				headers : {
					'Content-Type' : file.type.split("/")[1]
				},
				data : file,
			}).progress(function(evt) {
				// console.log('percent: ' + parseInt(100.0 *
				// evt.loaded / evt.total));
			}).success(function(data) {
				
				$scope.updatedStore.image = data['info']['md5'];
				$log.info($scope.updatedStore);
			}).error(function(data) {
				// error
				sweetAlert("上传图片失败");
			});

		}
	}
	
	$scope.start();
}
