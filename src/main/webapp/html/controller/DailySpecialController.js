/**
 * 
 */

var DailySpecialController=function($scope,$http,$upload){
	
	$scope.baseUrl = zimgeUrl;
	$scope.formatImage = "?w=200&h=200";
	$scope.open="open";
	$scope.controlC=false;
	
	
	
	/**
	 * 是否需要post数据
	 * 分页拿今日特价
	 */
	$scope.getDailySpecials=function(){
		
		//控制语句，没有orderby和asc的时候
		var urlpath="../dailyspecial/list";
		
		$http.get(urlpath).success(function(data){
			$scope.page=data;
			console.log(data);
		}).error(function(){
			
		});
		$scope.getCategorys();
	}
	
	/**
	 * 拿到所有的种类
	 */
	$scope.getCategorys=function(){
		$http.get('../category/list').success(function(response) {
			$scope.productCategorys = response;
		});
	}
	
	/**
	 *  拿到对应的产品
	 */
	$scope.getProducts=function(cselected){
		console.log(cselected);
		$http.get('../product/listbycid?categoryId='+cselected).success(function(response) {
			$scope.products=response;
		});
	}
	
	/**
	 * 跳转到修改页面
	 */
	$scope.modify=function(dailySpecial){
		$scope.controlC=true;
		$scope.cselected=dailySpecial.product.category.id;
		$scope.getProducts($scope.cselected);
		$scope.pselected=dailySpecial.product.id;
		$scope.ndailySpecial = dailySpecial;
		
	}
	
	/**
	 * 新增
	 */
	$scope.add=function(){
		$scope.cselected="";
		$scope.pselected="";
		$scope.controlC=false;
		$scope.ndailySpecial={};
	}
	
	
	/**
	 * 图片上传功能
	 */
	$scope.onFileSelect = function($files) { 
		// $files: an array of files selected, each file has name, size, and type.
		for (var i = 0; i < $files.length; i++) {
			var file = $files[i];

			$scope.upload = $upload.http({
				url :uploadUrl,
				headers : {
					'Content-Type' : file.type.split("/")[1]
				},
				data : file,
			}).progress(function(evt) {
//				console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
			}).success(function(data) {
				
				$scope.ndailySpecial.imageId=data['info']['md5'];
				
			}).error(function(data) {
				// error
				sweetAlert("上传图片失败");
			});
		} 
	}
	
	/**
	 * 完成数据更新
	 */
	$scope.submitdata=function(obj){
		
		if(!obj.imageId){
			sweetAlert("请添加图片");
			return;
		}
		
		var urlpath="";
		console.log(obj.imageIds);
		var data={
				'description' : obj.description,
				'price' :   obj.price,
				'imageId':  obj.imageId,
				'productId' : $scope.pselected
				};
		if(obj.id){
			urlpath="../dailyspecial/modify";
			data["id"]=obj.id;
			data['status']=obj.status;
		}
		else{
			urlpath="../dailyspecial/add";
			data['status']=true;
		}
				
		$http({
			method  : "post",
			url     : urlpath,
		        data    : $.param(data),  // pass in data as strings
		        headers : { 'Content-Type': 'application/x-www-form-urlencoded'}  
			
		}).success(function(data) {
			
			if(data['code']==-1){
				sweetAlert("已存在激活的特价产品");
				return;
			}else if(data['code']==0){
				 swal({	
					   title: "警告",
					   text:"您无权限进行操作",
					   timer: 2000
					   });
				   //重定向到product那里了
				   $location.path("error");
				   return;
			}
			sweetAlert("数据已更新");
	        	    //可能需要刷新纪录
			$scope.getDailySpecials();
	        	    
	        }).error(function(){
	        	sweetAlert("数据更新失败，请重新更新");
	        });
	}
	
	/*
	 *  激活状态
	 *  
	 */
	$scope.Activation=function(obj){
		var index=$scope.page.indexOf(obj);
//		console.log(index);
		
		var data={
				'id':$scope.page[index].id,
				'description' : $scope.page[index].description,
				'price' :$scope.page[index].price,
				'imageId':$scope.page[index].imageId,
				'productId' : $scope.page[index].product.id,
				'status' : true,
		}
		$http({
			method  :"post",
			url     : "../dailyspecial/activation",
			data    : $.param(data),
		        headers : { 'Content-Type': 'application/x-www-form-urlencoded'} 
		}).success(function(response){
			
			if(response['code']==-1){
				sweetAlert("已存在激活的特价产品");
				return;
			}else if(response['code']==0){
				 swal({	
					   title: "警告",
					   text:"您无权限进行操作",
					   timer: 2000
					   });
				   //重定向到product那里了
				   $location.path("error");
				   return;
			}
			$scope.page[index].status=true;
			sweetAlert("激活成功");
		}).error(function(){
			
		});
	}
	
	/**
	 * 取消激活状态
	 */
	$scope.cancelActivation=function(obj){
		var index=$scope.page.indexOf(obj);
		$scope.page[index].status=false;
		var data={
				'id':$scope.page[index].id,
				'description' : $scope.page[index].description,
				'price' :$scope.page[index].price,
				'imageId':$scope.page[index].imageId,
				'productId' : $scope.page[index].product.id,
				'status' : $scope.page[index].status,
		}
		$http({
			method  :"post",
			url     : "../dailyspecial/modify",
			data    : $.param(data),
		        headers : { 'Content-Type': 'application/x-www-form-urlencoded'} 
		}).success(function(data){
			
			 if(response['code']==0){
				 swal({	
					   title: "警告",
					   text:"您无权限进行操作",
					   timer: 2000
					   });
				   //重定向到product那里了
				   $location.path("error");
				   return;
			}
			sweetAlert("已经取消激活");
			
		}).error(function(){
			
		});
	}
	
	
	$scope.getDailySpecials();
	
}