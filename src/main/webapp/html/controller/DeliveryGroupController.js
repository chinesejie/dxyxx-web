/**
 * 
 */
var DeliveryGroupController = function($http,$scope) {
	$scope.code = "";
	$scope.shows = false;
	$scope.check = function(code) {
		if (code == "") {
			$scope.shows = false;
			sweetAlert("请填写订单唯一标识号");
			return;
		}
		$http.get('../gorder/getGroupOrder?uuid=' + code).success(function(response) {

			if (response.code == 1) {
				$scope.order = response.object;
				$scope.shows = true;
			} else if (response.code == 0) {
				sweetAlert(response.info);
				return;
			} else if (response.code == -1) {
				$scope.shows = false;
				sweetAlert(response.info);
			}
//			$("#product").removeClass("am-in");
//			$("#groupOwner").removeClass("am-in");
			$scope.code = "";

		}).error(function() {

		}).then(function(){
			$http.get("../groupinstance/get?groupinstanceId="+$scope.order.groupInstance.id).success(function(response){
				$scope.groupOwners = response.object;
			});
		});
	}

	/** 提货 */
	$scope.delivery = function(order) {
		if (order == "") {
			sweetAlert("错误操作");
			return;
		}
		$http.get('../gorder/sureGroupOrder?uuid=' + order.uuid).success(function(response) {
			if (response.code == 0) {
				sweetAlert(response.info);
				return;
			} else if (response.code == 1) {
				sweetAlert("提货成功");
				$scope.order = "";
				$scope.shows = false;
			}
		}).error(function() {

		});
	}
}