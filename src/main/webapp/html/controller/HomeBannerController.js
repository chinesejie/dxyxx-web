/**
 * tip : 按照xx排序 issue：
 * 
 * @author young 2015年3月18日14:56:00
 */

var HomeBannerController = function($scope, $http, $location, $upload, $log) {

	$scope.baseUrl = zimgeUrl;
	$scope.formatImage = "";
	$scope.open = "open";
	$scope.controlC = false;

	/*
	 * 查所有的homebanner
	 */
	$scope.getAll = function() {
		$http.get('../homepagebanner/list').success(function(response) {

			$scope.homebanners = response;
		});
		// 这样快点
		$scope.getCategorys();
	}

	/*
	 * 激活状态
	 * 
	 */
	$scope.Activation = function(homebanner) {
		var index = $scope.homebanners.indexOf(homebanner);
		// console.log(index);

		var data = {
			'id' : $scope.homebanners[index].id,
			'description' : $scope.homebanners[index].description,
			'price' : $scope.homebanners[index].price,
			'imageId' : $scope.homebanners[index].imageId,
			'productId' : $scope.homebanners[index].product.id,
			'status' : true,
			'priority' : $scope.homebanners[index].priority
		}
		$http({
			method : "post",
			url : "../homepagebanner/activation",
			data : $.param(data),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(response) {

			if (response['code'] == 0) {
				swal({
					title : "警告",
					text : response.info,
					timer : 2000
				});
				return;
			} else if (response['code'] == -1) {
				swal({
					title : "警告",
					text : "无权操作",
					timer : 2000
				});
				// 重定向到product那里了
				$location.path("error");
				return;
			}

			$scope.homebanners[index].status = true;
			sweetAlert("激活成功");

		}).error(function() {

		});
	}

	/**
	 * 取消激活状态
	 */
	$scope.cancelActivation = function(homebanner) {

		var index = $scope.homebanners.indexOf(homebanner);
		
		var data = {
			'id' : $scope.homebanners[index].id,
			'productId' : $scope.homebanners[index].product.id,
		}
		$http({
			method : "post",
			url : "../homepagebanner/cancelActivation",
			data : $.param(data),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(response) {

			if (response['code'] == 0) {
				swal({
					title : "警告",
					text : response.info,
					timer : 2000
				});

				return;

			}
			
			$scope.homebanners[index].status = false;
			sweetAlert("已经取消激活");

		}).error(function() {

		});
	}

	/**
	 * 拿到所有的种类
	 */
	$scope.getCategorys = function() {
		$http.get('../category/list').success(function(response) {
			$scope.productCategorys = response;
		});
	}
	/**
	 * 拿到对应的产品
	 */
	$scope.getProducts = function(cselected) {
		console.log(cselected);
		$http.get('../product/listbycid?categoryId=' + cselected).success(function(response) {
			$scope.products = response;
		});
	}
	/**
	 * 跳转到修改页面
	 */
	$scope.modify = function(homebanner) {

		$scope.controlC = true;
		$log.info(homebanner);
		$scope.nhomebanner = homebanner;
		$scope.cselected = homebanner.product.category.id;
		$scope.getProducts($scope.cselected);
		$scope.pselected = homebanner.product.id;

	}

	/**
	 * 图片上传功能
	 */
	$scope.onFileSelect = function($files) {
		// $files: an array of files selected, each file has name, size,
		// and type.
		for (var i = 0; i < $files.length; i++) {
			var file = $files[i];

			$scope.upload = $upload.http({
				url : uploadUrl,
				headers : {
					'Content-Type' : file.type.split("/")[1]
				},
				data : file,
			}).progress(function(evt) {
				console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
			}).success(function(data) {

				$scope.nhomebanner.imageId = data['info']['md5'];

			}).error(function(data) {
				// error
				sweetAlert("上传图片失败");
			});
		}
	}

	/**
	 * 新增
	 */
	$scope.add = function() {
		$scope.cselected = "";
		$scope.pselected = "";
		$scope.controlC = false;
		$scope.nhomebanner = {};

	}

	/**
	 * 完成数据更新
	 */
	$scope.submitdata = function(homebanner) {

		var urlpath = "";

		if (angular.isUndefined(homebanner.imageId)) {
			sweetAlert("请上传图片");
			return;
		}

		var data = {
			'description' : homebanner.description,
			'price' : homebanner.price,
			'imageId' : homebanner.imageId,
			'productId' : $scope.pselected,
			'priority' : homebanner.priority
		};
		if (homebanner.id) {
			urlpath = "../homepagebanner/modify";
			data["id"] = homebanner.id;
			data['status'] = homebanner.status;
		} else
			urlpath = "../homepagebanner/add";

		$http({
			method : "post",
			url : urlpath,
			data : $.param(data), // pass in data as strings
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}

		}).success(function(data) {

			if (data['code'] == 0) {
				swal({
					title : "警告",
					text : data.info,
					timer : 2000
				});
//				$location.path("error");
				
				return;
			} else if (data['code'] == -1)
				sweetAlert("已存在一个激活的homePageBanner");
			else {
				sweetAlert("数据更新成功");
				$scope.getAll();
			}

		}).error(function() {

			sweetAlert("数据更新失败，请重新更新");

		});
	}

	/**
	 * for single picture
	 */
	$scope.pass = function(thumbId) {

		$scope.$parent.singlepicture = $scope.baseUrl + thumbId + "?p=0";
	}

	$scope.getAll();

}