/**
 * 显示了用户的电话号码
 */
var ManagerController=function($http,$scope,$location){

		$scope.code="";
		$scope.shows=false;
		$scope.check=function(code){
			if(code==""){
				$scope.shows=false;
				sweetAlert("请填写订单唯一标识号");
				return;
			}
			$http.get('../order/getDiliverOrder?uuid='+code).success(function(response){
				
				if(response.code==1){
					$scope.order=response.object;
					$scope.shows=true;
				}else if(response.code==0){
					 swal({	
						   title: "警告",
						   text:response.info,
						   timer: 2000
						   });
					   //重定向到product那里了
					   $location.path("error");
					   return;
				}else if(response.code==-1){
					$scope.shows=false;
					sweetAlert(response.info);
				}
				$scope.code="";
				
			}).error(function(){

			});
		}
		
	/**	提货*/
	$scope.delivery=function(order){
		console.log(order);
		if(order==""){
			sweetAlert("错误操作");
			return;
		}
		$http.get('../order/dilivery?uuid='+order.uuid).success(function(response){
			 if(response.code==0){
				 swal({	
					   title: "警告",
					   text:"您无权限进行操作",
					   timer: 2000
					   });
				   //重定向到product那里了
				   $location.path("error");
				   return;
			}else if(response.code==1){
				sweetAlert("提货成功");
				$scope.order="";
				$scope.shows=false;
			}if(response.code==-1){
				sweetAlert("已经被提货了，或者该订单不存在");
			}
		}).error(function(){
			
		});
	}
		
}
