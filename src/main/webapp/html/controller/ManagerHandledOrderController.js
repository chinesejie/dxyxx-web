/**
 * 
 */
var ManageHandledOrderController=function(Paginator,$scope,$http,$log){
	$scope.pageSize=10;
	$scope.data=[];
	
	var fetchFunction=function(currentPage,limit,urlPath,callback){
		$http.get(urlPath,{
			params:{
				'pageSize':limit,
				'pageNumber':currentPage,
			}}).success(callback);
	}
	
	$scope.data['order']=Paginator(fetchFunction,$scope.pageSize,"../order/getHandledOrder");
	$log.info($scope.data['order']);
	
	$scope.getTab=function(argu){
		var urlPath;
		var dataName;
		switch(argu){
			case '1':
				urlPath="../order/getHandledOrder";
				dataName="order";
				break;
			case '2':
				urlPath="../order/getHandledTicketOrder";
				dataName="ticket";
				break;
			case '3':
				urlPath="../gorder/getHandledGroupOrder";
				dataName="groupOrder";
				break;
			default:
				sweetAlert("系统出错");
				return;
		}
		$scope.data[dataName]=Paginator(fetchFunction,$scope.pageSize,urlPath);
	}
	
	
	/**
	 * 查看页面
	 */
	$scope.editOrder = function(obj) {

		$scope.norder = obj;
		$("#order_ui").offCanvas('open');
	}
	
	$scope.editTicket = function(obj) {

		$scope.nticket = obj;
		$("#ticket_ui").offCanvas('open');
	}
	
	$scope.editGroup = function(obj) {

		$scope.ngroup = obj;
		$log.info(obj);
		$("#group_ui").offCanvas('open');
	}
	

	/**
	 * 关闭侧边栏
	 */
	$scope.closeNew = function() {

		$scope.showOperator = false;
		$("#order_ui").offCanvas('close');
	}
	$scope.closeNew2 = function() {

		$scope.showOperator = false;
		$("#ticket_ui").offCanvas('close');
	}
	$scope.closeNew3 = function() {

		$scope.showOperator = false;
		$("#group_ui").offCanvas('close');
	}
	
	
	
	
	
}