/**
 * 
 */
var MessageController = function($http, $scope, PaginatorRR, $log) {

	$scope.pageSize = 10;
	$scope.messages = [];
	$scope.NoModify = true;

	var fetchFunction = function(currentPage, limit, propId, callback) {

		$http.get('../message/get', {
			params : {
				'pageSize' : limit,
				'cPage' : currentPage
			}
		}).success(callback);
	}
	$scope.messages[0] = PaginatorRR(fetchFunction, $scope.pageSize, 0);

	$scope.readMessage = function(message) {
		if (message.hreads == false) {
			$http.get("../message/update?mid=" + message.id).success(function(response) {
				// TODO
				$scope.messages[0] = PaginatorRR(fetchFunction, $scope.pageSize, 0);
			}).error().then(function() {
				$http.get("../message/getNum").success(function(response) {
					$log.info(response);
					if (response.object != 0 && response.code == 1) {
						$scope.$parent.messageCount = response.object;
					} else if (response.object == 0 && response.code == 1) {
						$scope.$parent.messageCount = "";
					}
				});
			});
		}

		$scope.nmessage = message;
		$("#new_ui").offCanvas('open');
	}

	$scope.closeNew = function() {

		$("#new_ui").offCanvas('close');
	}
}