/**
 * 
 */
var MonthStatisticsController = function($http, $scope, PaginatorRR, $log) {
	$scope.pageSize = 10;
	$scope.dataStatistics = [];
	$scope.checkDate = new Date();

	var fetchFunction = function(currentPage, limit, propId, callback) {

		$http.get('../orderbuffer/getByMonth', {
			params : {
				'pageSize' : limit,
				'cpage' : currentPage,
				'date' : propId
			}
		}).success(callback);
	}

	$scope.check = function() {
		var checkedDate = "";
		if ($("#birthday_input").val() != "") {
			checkedDate = $("#birthday_input").val();
		} else {
			checkedDate = $("#birthday_input").attr('placeholder');
		}

		$scope.dataStatistics[0] = PaginatorRR(fetchFunction, $scope.pageSize, checkedDate);
	}

	$scope.edit = function(order) {
		var urlPath = "../orderbuffer/get?uuid=" + order.uuid;
		var offCanvas = "";
		switch (order.orderType) {
		case 2:
			offCanvas = "#group_ui";
			break;
		case 3:
			offCanvas = "#ticket_ui";
			break;
		case 1:
			offCanvas = "#new_ui";
			break;
		default:
			return;
		}

		$http.get(urlPath).success(function(response) {
			if (response.code == 1) {
				$scope.norder = response.object;
				if (offCanvas == "#group_ui") {
					$http.get("../groupinstance/get?groupinstanceId=" + $scope.norder.groupInstance.id).success(function(response) {
						$log.info(response);
						$scope.groupOwners = response.object;
					});
				}
				$(offCanvas).offCanvas('open');
			} else if (response.code == 0) {
				sweetAlert(response.info);
			}
		}).error();

	}

	$scope.closeNew = function() {
		$("#new_ui").offCanvas('close');
	}
	$scope.closeNew2 = function() {
		$("#ticket_ui").offCanvas('close');
	}
	$scope.closeNew3 = function() {
		$("#group_ui").offCanvas('close');
	}
}