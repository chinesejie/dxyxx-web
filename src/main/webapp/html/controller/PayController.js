/**
 * 
 */
var payApp = angular.module('payApp', []);


payApp.config([ '$locationProvider', function AppConfig($locationProvider) {

	$locationProvider.html5Mode(true);

} ]);

payApp.controller('PayController', function($http,$scope,$location) {
	
	$scope.arguments=$location.search();
	$scope.callPay=function(){
		WeixinJSBridge.invoke('getBrandWCPayRequest', {
			"appId" : $scope.arguments.appid,
			"timeStamp" : $scope.arguments.timeStamp,
			"nonceStr" : $scope.arguments.nonceStr,
			"package" : $scope.arguments.package1,
			"signType" : "MD5",
			"paySign" : $scope.arguments.sign
		}, function(res) {
			WeixinJSBridge.log(res.err_msg);
			alert(res.err_msg);
			// 				alert(res.err_code + res.err_desc + res.err_msg);
			if (res.err_msg == "get_brand_wcpay_request:ok") {
				alert("微信支付成功!");
				window.location.href = "./wx/index.html";//回到首页
			} else if (res.err_msg == "get_brand_wcpay_request:cancel") {
				alert("用户取消支付!");
			} else {
				alert("支付失败!");
			}
		})
	}
	
	$scope.start=function(){
		$http.get('../order/findByUuid?uuid=' + $scope.arguments.uuid).success(function(response) {
			if(response.code==0){
				alert(response.info);
				return;
			}
			$scope.order = response.object;
			if ($scope.order.payType == "tenpay") {
				$scope.order.payType = "微信支付";
			} else {
				$scope.order.payType = "支付宝";
			}

		}).error(function() {

		});
	}
	
	$scope.start();

});