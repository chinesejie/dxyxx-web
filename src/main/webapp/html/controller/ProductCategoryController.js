/**
 * Created by tmichels on 8/1/14. 修改和增加 不用发送再拿数据请求了。
 */

var ProductCategoryController = function($scope, $location, $http, $upload) {

	$scope.editMode = false;
	$scope.position = '';
	$scope.baseUrl = zimgeUrl;
	$scope.formatImage = "?p=0";
	$scope.status = true;
	$scope.icon = "edit";

	$scope.getAllProductCategory = function() {
		$http.get('../category/list').success(function(response) {
			$scope.productCategorys = response;
		});
	}

	/**
	 * add
	 */
	$scope.addProductCategory = function() {

		if (angular.isUndefined($scope.imageId)) {
			sweetAlert("请添加图片");
			return;
		}

		var data = {
			"description" : $scope.formdata.description,
			"imageid" : $scope.imageId.info.md5
		};

		if ($scope.formdata.description != null && $scope.imageId != null) {
			$http({
				method : 'POST',
				url : '../category/add',
				// params : $scope.formdata, 用data传数据可以解决乱码问题
				data : $.param(data), 
				// 'accept-charset':'utf-8' 这个不需要
				headers : {
					'Content-Type' : 'application/x-www-form-urlencoded'
				}
			// set the headers so angular passing info as form data
			// (not request payload)
			}).success(function(data) {

				if (data['code'] == 0) {
					swal({
						title : "警告",
						text : "您无权限进行操作",
						timer : 2000
					});
					// 重定向到product那里了
					$location.path("error");
					return;
				}
				
				$scope.imageId = null;
				sweetAlert("添加成功");
				$scope.formdata.description = "";
				$scope.filename = "";

				$scope.getAllProductCategory();

			}).error(function() {
				sweetAlert("添加失败，请重新添加");
			});
		} else {
			sweetAlert("请添加描述和图片,并且描述不能有空格");
		}

		/*
		 * $http.post('../category/add?description=' + description +
		 * '&imageid=' + imageID).success(function(response) {
		 * $scope.description=""; $scope.filename="";
		 * $scope.getAllProductCategory(); }).errors(function() {
		 * alert("上传失败，请重新上传"); }); } else { alert("请添加产品种类图片或者是描述信息"); }
		 */
	}

	/**
	 * 
	 */
	$scope.onFileSelectadd = function($files, productCategory) {
		// $files: an array of files selected, each file has name, size,
		// and type.
		for (var i = 0; i < $files.length; i++) {
			var file = $files[i];

			$scope.upload = $upload.http({
				url : uploadUrl,
				headers : {
					'Content-Type' : file.type.split("/")[1]
				},
				data : file,
			}).progress(function(evt) {
				// console.log('percent: ' + parseInt(100.0 *
				// evt.loaded / evt.total));
			}).success(function(data) {

				productCategory['imageId'] = data['info']['md5'];

			}).error(function(data) {
				// error
				sweetAlert("上传图片失败");
			});
		}
	}
	/**
	 * 上传文件
	 */
	$scope.onFileSelect = function($files) {
		// $files: an array of files selected, each file has name, size,
		// and type.
		for (var i = 0; i < $files.length; i++) {
			var file = $files[i];
			$scope.filename = file.name;

			$scope.upload = $upload.http({
				url : uploadUrl,
				headers : {
					'Content-Type' : file.type.split("/")[1]
				},
				data : file,
			}).progress(function(ev) {
				// progress
			}).success(function(data) {
				$scope.imageId = data;
			}).error(function(data) {
				// error
			});
		}
	}

	/*
	 * edit icon exchange with check icon
	 */
	$scope.changeStatus = function() {
		$scope.status = !$scope.status;
		if ($scope.status)
			$scope.icon = "edit";
		else
			$scope.icon = "check";

	}
	
	/**
	 * statusForm:表单验证的 class状态
	 */
	$scope.modifyById = function(productCategory, statusForm) {

		// console.log(productCategory);
		if (productCategory.icon == "edit") {
			productCategory.status = false;
			productCategory.icon = "check";
			return;
		}
		productCategory.status = true;
		productCategory.icon = "edit";

		$scope.modifydata = {
			"id" : productCategory.id,
			"description" : productCategory.description,
			"imageId" : productCategory.imageId
		};
		if (productCategory.id != null) {
			$http({
				method : 'POST',
				url : '../category/modify',
				// params : $scope.formdata, 用data传数据可以解决乱码问题
				data : $.param($scope.modifydata), // pass
									// in
									// data
									// as
									// strings
				headers : {
					'Content-Type' : 'application/x-www-form-urlencoded'
				}
			
			}).success(function(data) {

				if (data['code'] == 0) {
					swal({
						title : "警告",
						text : "您无权限进行操作",
						timer : 2000
					});
					$location.path("error");
					return;
				}
				sweetAlert("修改成功");
				$scope.getAllProductCategory();

			}).error(function() {
				sweetAlert("添加失败，请重新添加");
			});
		} else {
			sweetAlert("请添加描述和图片,并且描述不能有空格");
		}
	}

	/**
	 * for single picture
	 */
	$scope.pass = function(thumbId) {
		$scope.$parent.singlepicture = $scope.baseUrl + thumbId + "?p=0";
	}

	$scope.getAllProductCategory();

}