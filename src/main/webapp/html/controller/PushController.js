/**
 * 推送管理
 */
var PushController = function($http, $scope, $log) {
	$scope.open = "open";
	$scope.start = function() {
		$http.get("../admin_message/get").success(function(response) {
			if (response.object.length == 0) {
				sweetAlert("无数据");
				return;
			}
			console.log(response.object);
			$scope.messages = response.object;
		});
	}

	$scope.closeEditUI = function() {

		$("#push-detail")[0].attributes.name.value = "";
		$("#push-detail").offCanvas('close');
		$("#push-detail")[0].attributes.name.value = "open";
	}
	$scope.ToAddPush = function() {
		$scope.npush = {};
		$log.info($scope.nstore);
	}

	$scope.addPush = function(npush) {

		if (angular.isUndefined(npush.title) || npush.title == null
				|| npush.title == "") {
			sweetAlert("请添加标题");
			return;
		}

		if (angular.isUndefined(npush.content) || npush.content == null
				|| npush.content == "") {
			sweetAlert("请添加内容");
			return;
		}

		var postData = {
			"content" : npush.content,
			"title" : npush.title,
			"website" : npush.website
		};

		$http({
			method : "post",
			url : "../admin_message/add",
			data : $.param(postData),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(response) {
			if (response.code == 0) {
				sweetAlert("添加失败 ", response.info);
				return;
			}
			sweetAlert("添加成功");
			$scope.start();
			$scope.closeEditUI();
		}).error();
	}
	$scope.start();
}