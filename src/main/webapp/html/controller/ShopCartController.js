/**
 * 
 */
var ShopCartController = function($scope, $http,$window, localStorageService,$location,$log) {

	$scope.products = [];
	$scope.totalPrice;
	$scope.baseUrl = zimgeUrl;

	
	$scope.start = function() {
		$log.info(localStorageService.keys());
		var keys = localStorageService.keys();
		if (keys == null || keys == "") {
			sweetAlert("请添加商品");
			return;
		}
		for ( var item in keys) {

			$scope.products.push(localStorageService.get(keys[item]));
		}
		$log.info($scope.products);
	}

	// add 1
	$scope.addProduct = function(item) {
		item.amount = item.amount + 1;
		localStorageService.set(item.product.id, {
			"product" : item.product,
			"amount" : item.amount
		});
	}
	// minus 1
	$scope.minusProduct = function(item) {
		if (item.amount == 1) {
			sweetAlert("亲，不能再减了哦");
			return;
		}
		item.amount = item.amount - 1;
		localStorageService.set(item.product.id, {
			"product" : item.product,
			"amount" : item.amount
		});
	}

	$scope.deleteItem = function(item, key) {
		$scope.products.splice($scope.products.indexOf(item), 1);
		localStorageService.remove(item.product.id);
	}

	$scope.$watch("products", function(value) {
		var products=$scope.products;
		$scope.totalPrice=0;
		//重新计算
		for(var i in products){
			
			$scope.totalPrice=$scope.totalPrice+products[i].product.price*products[i].amount;
		}
	},true);
	
	//订单页面
	$scope.pay=function(){
		$location.path("verifyOrder");
	}
	//之前的商品页面
	/**	返回*/
	$scope.goBack=function(){
		$window.history.go(-1);
//		$location.path("wcPay").search();
//		window.location.href='verifyOrder.html?id='+$scope.order.orderDetails[0].product.id+"&amount="+$scope.order.orderDetails[0].amount;
	}
	
	$scope.homePage=function(){
		$location.path("").search({"type":$location.search().type});
	}

	$scope.start();
}
