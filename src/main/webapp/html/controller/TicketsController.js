/**
 * 
 */
var TicketsController = function($scope,$http) {
	$scope.code = "";
	$scope.shows = false;
	$scope.check = function(code) {
		if (code == "") {
			$scope.shows = false;
			sweetAlert("请填写订单唯一标识号");
			return;
		}
		$http.get('../order/getTicketOrder?uuid=' + code).success(function(response) {
			console.log(response);

			if (response.code == 1) {
				$scope.order = response.object;
				$scope.shows = true;
			} else if (response.code == 0) {
				swal({
					title : "警告",
					text : "您无权限进行操作",
					timer : 2000
				});
				// 重定向到product那里了
				$location.path("error");
				return;
			} else if (response.code == -1) {
				$scope.shows = false;
				sweetAlert("请确认这张订单未被提货");
			}
			$scope.code = "";

		}).error(function() {

		});
	}

	/** 提货 */
	$scope.delivery = function(order) {
		console.log(order);
		if (order == "") {
			sweetAlert("错误操作");
			return;
		}
		$http.get('../order/diliveryTicket?uuid=' + order.uuid).success(function(response) {
			if (response.code == 0) {
				swal({
					title : "警告",
					text : "您无权限进行操作",
					timer : 2000
				});
				// 重定向到product那里了
				$location.path("error");
				return;
			} else if (response.code == 1) {
				sweetAlert("提取成功");
				$scope.order = "";
				$scope.shows = false;
			}
			if (response.code == -1) {
				sweetAlert("已经被提货了");
			}
		}).error(function() {

		});
	}
}