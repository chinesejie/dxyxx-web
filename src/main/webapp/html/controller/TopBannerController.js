/**
 *   topbanner 有按照修改时间来排序
 *   issue: input的readONLY属性的变化
 */

var TopBannerController = function($scope, $http,$upload,$location) {
	
	$scope.baseUrl = zimgeUrl;
	$scope.formatImage = "";
	
	$scope.getAll=function(){
		
		$http.get('../topbanner/list').success(function(response) {
			
			$scope.topbanners = response;
//			console.log($scope.topbanners);
		});
	}
	
	
	
	/*
	 * 删除
	 */
	$scope.deletetopbanner=function(obj){

		if(obj.id==null){
			 $scope.topbanners.splice($scope.topbanners.indexOf(obj), 1);
			 sweetAlert("数据更新成功");
			 return;
		}
		
		$http({
			method:"post",
			url     : "../topbanner/delete",
			params  : {'id':obj.id},
		        headers : { 'Content-Type': 'application/x-www-form-urlencoded'}  
			
		}).success(function(data) {
			
			 if(data['code']==0){
				   swal({	
					   title: "警告",
					   text:"您无权限进行操作",
					   timer: 2000
					   });
				   //重定向到product那里了
				   $location.path("error");
				   return;
			   }else{
				   sweetAlert("数据更新成功");
				   $scope.topbanners.splice($scope.topbanners.indexOf(obj), 1);
				   //没有重新去拿
				   
			   }
	        	   
	        }).error(function(){
	        	sweetAlert("删除失败");
	        });
		
	}
	
	/* 待完善
	 * 新增
	 * 表达式是可以判定true和false的
	 */
	$scope.add=function(){
		var object={};
		object['imageId']="";
		$scope.topbanners.push(object);
	}
	
	/**
	 *  修改和增加
	 *  需要再改改 有个小bug，点击编辑以后只点击一下 上传，保存，再点击上传。
	 */
	$scope.sumbitdata=function(topbanner,dirty){
		var urlpath="";
		
		console.log(dirty);
		if(topbanner.save=="编辑"){
			topbanner.status=false;
			topbanner.save="保存";
			return;
		}
		topbanner.status=true;
		
		if(!dirty){
			topbanner.save="编辑";
			return;
		}
		
		var data={
				'description':topbanner.description,
				'priority'   :topbanner.priority,
				'imageId'    :topbanner.imageId,
				'note'       :topbanner.note
				};
		//data['id']那种定义方式不能放前面，需放后面
		if(topbanner.id==null || topbanner.id=='' ){
			urlpath="../topbanner/add";
			topbanner['modifyTime']=new Date();
		}
		else{
			urlpath="../topbanner/modify";
			data['id']=topbanner.id;
		}
				
		$http({
			method  : "post",
			url     : urlpath,
		        data    : $.param(data),  // pass in data as strings
		        headers : { 'Content-Type': 'application/x-www-form-urlencoded'}  
			
		}).success(function(data) {
			   
			if(data['code']==0){
				   swal({	
					   title: "警告",
					   text:"您无权限进行操作",
					   timer: 2000
					   });
				   //重定向到product那里了
				   $location.path("error");
				   return;
			   }else{
				  
				   sweetAlert("数据更新成功");
				   topbanner.id=data.object;
		        	  //应该还需要做优化
				   topbanner.save="编辑";
			   }
	        	    
	        	    
	        }).error(function(){
	        	
	        	 sweetAlert("更新失败，请重新更新数据");
	        	 topbanner.save="编辑";
	        });
		
	}
	
	/*
	 * 新增时上传图片出现未定义错误
	 * 上传图片
	 */
	$scope.onFileSelect = function($files,topbanner) { 
		// $files: an array of files selected, each file has name, size, and type.
		for (var i = 0; i < $files.length; i++) {
			var file = $files[i];
			
			console.log(angular.isUndefined(topbanner.imageId));
			$scope.upload = $upload.http({
				url : uploadUrl,
				headers : {
					'Content-Type' : file.type.split("/")[1]
				},
				data : file,
			}).progress(function(evt) {
//				console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
			}).success(function(data) {
				
				topbanner['imageId']=data['info']['md5'];
				
			}).error(function(data) {
				// error
				sweetAlert("上传图片失败");
			});
		} 
	}
	
	
	/**
	 * for single picture
	 */
	$scope.pass=function(thumbId){
		
		$scope.$parent.singlepicture=$scope.baseUrl+thumbId+"?p=0";
	}
	
	
	$scope.getAll();
	
	
}