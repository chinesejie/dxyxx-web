var UserController = function($scope, $http) {
	
	$scope.nextPage = function(page){
		$scope.findUsers(page.pageSize,page.nextPage,'id',true);
 	}
	
	$scope.prePage = function(page){
		$scope.findUsers(page.pageSize,page.prePage,'id',true);
 	}
	
	$scope.findUsers = function(pageSize,pageNumber,orderBy,asc){
//		alert(pageSize +' '+pageNumber +' '+orderBy+' ' +asc);
		var $url = '../user/findUsers?pageSize=' + pageSize + '&pageNumber=' + pageNumber + '&orderBy=' + orderBy + '&asc=' + asc;

		$http.get($url).success(function(response){
			//TODO 待进一步测试
			if(response.code==0){
				sweetAlert(response.info);
				return;
			}
			$scope.page = response;
		});
 	}

	$scope.findUsers(10,1,"id",true);

}
