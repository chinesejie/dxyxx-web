/**
 * 
 */
var VersionController=function($http,$scope){
	$http.get("../version/getVersion").success(function(response){
		if(response.object.length==0){
			sweetAlert("无数据");
			return;
		}
		console.log(response.object);
		$scope.versions=response.object;
	});
}