/**
 * 
 */
var testController = function($scope, $location, $http, localStorageService) {

	var temp;
	var argu = $location.absUrl().split("?")[1];
	var getUrl = "../../product/get?";

	$scope.baseUrl = zimgeUrl;
	$scope.formatImage = "?p=0";
	$scope.amount = 1;
	$scope.pageSize = 10;
	$scope.current = 1;
	$scope.totalPage;
	$scope.comments = "";

	// add 1
	$scope.addProduct = function() {
		$scope.amount = $scope.amount + 1;
	}
	// minus 1
	$scope.minusProduct = function() {
		if ($scope.amount == 1) {
			sweetAlert("亲，不能再减了哦");
			return;
		}
		$scope.amount = $scope.amount - 1;
	}

	/** 启动 */
	$scope.start = function() {
		$http.get(getUrl + "id=" + $location.search().id).success(function(response) {

			// $log.info(response);
			$scope.product = response;

			temp = response.imageIds;
			$scope.pictures = temp.split(";");
			var slider = $('#am-slider').data('flexslider');
			angular.forEach($scope.pictures, function(data, index) {
				slider.addSlide("<li><img src=\"" + $scope.baseUrl + data + $scope.formatImage + "\"></li>", index);
			});
			slider.removeSlide($("#first"));

		}).error(function() {
			alert("error");
			return;
		}).then(function() {

			$http.get("../../productDetail/get?productId=" + $location.search().id).success(function(response) {

				// $log.info(response);
				if (response != null && response != "") {
					$scope.productDetails = angular.fromJson(response);
					// $scope.productDetails = eval('(' +
					// response + ')');
					$scope.Details = eval('(' + $scope.productDetails.details + ')');
				}
			}).error();

			$http.get("../../product/list?categoryId=" + $scope.product.category.id + "&current=1&count=5&order=hot").success(function(response) {
				var secondSlider = $('#am-slider2').data('flexslider');
				var temp = response;
				angular.forEach(temp, function(data, index) {
					// $log.info(data);
					secondSlider.addSlide("<li><a href=\"index.html#/wcPay?id=" + data.id + "\"><img src=\"" + $scope.baseUrl + data.thumbId + $scope.formatImage + "\"></a></li>", index);
				});
				secondSlider.removeSlide($("#second"));
			}).error();
		});

//		if ($location.search().type == "ios") {
//			$scope.downloadLink = IOSdownLoadLink;
//		} else if ($location.search().type == "android") {
//			$scope.downloadLink = AndroidLoadLink;
//		}
		$scope.downloadLink=AndroidLoadLink;// 统一了
	}

	/** 跳转 =》 下单 */
	$scope.pay = function() {
		// 存
		localStorageService.set($scope.product.id, {
			"product" : $scope.product,
			"amount" : $scope.amount
		});
		$location.path("verifyOrder");

	}

	$scope.showComments = function() {
		// 已经存在不再次读取
		if ($scope.comments == null || $scope.comments == "") {

			$http.get('../../comment/listByPid?pid=' + $location.search().id + '&current=' + 1 + '&count=' + $scope.pageSize).success(function(response) {
				// $log.info(response);
				$scope.totalPage = Math.ceil(response.object.recordCount / $scope.pageSize);
				$scope.comments = response.object.recordList;

			}).error();
		}
	}

	/** login cancel */
	$scope.cancel = function() {
		$('#my-prompt').modal("close");
	}
	$scope.login = function(phone, pwd) {
		var password;

		$http.get("../../encryption/transfer?plain=" + pwd).success(function(response) {
			password = response.object;
		}).then(function() {

			var user = {
				'phone' : phone,
				'password' : password
			};

			$http({
				method : "post",
				url : '../../login',
				data : $.param(user), // pass in data
				headers : {
					'Content-Type' : 'application/x-www-form-urlencoded'
				}

			}).success(function(response) {
				// $log.info(response);
				if (response.code == 1) {
					$('#my-prompt').modal("close");
					sweetAlert("登录成功");
					$location.path("verifyOrder").search({
						"id" : argu.split("=")[1]
					});
				} else if (response.code == 0) {
					sweetAlert("登录失败");
				}

			}).error();

		});

	}

	$scope.nextPage = function() {
		$scope.current += 1;
		if ($scope.current > $scope.totalPage) {
			sweetAlert("后面没有评论了哦");
			return;
		}
		$http.get('../../comment/listByPid?pid=' + $location.search().id + '&current=' + $scope.current + '&count=' + $scope.pageSize).success(function(response) {

			for ( var item in response.object.recordList) {
				// TODO 测试
				$scope.commets.push(item);
			}
		}).error();

	}

	$scope.showLogin = function() {
		$('#my-prompt').modal();
	}

	$scope.addShoppingCart = function() {

		localStorageService.set($scope.product.id, {
			"product" : $scope.product,
			"amount" : $scope.amount
		});
		$location.path("shopCart");
	}

	// 进入购物车
	$scope.shoppingCart = function() {

		$location.path("shopCart");
	}

	$scope.homePage = function() {
		$location.path("").search({
			"type" : $location.search().type
		});
	}

	$scope.start();
}