/**
 * 
 */

var zimgeUrl='http://123.56.91.1:4869/';
var uploadUrl="http://123.56.91.1/upload";
var getImageInfo="http://123.56.91.1:4869/info";

var scotch = angular.module('scotch', [ 'ngRoute' ]);

scotch.config([ '$routeProvider', '$locationProvider', function AppConfig($routeProvider, $locationProvider) {

	// enable html5Mode for pushstate ('#'-less URLs)
	$locationProvider.html5Mode(true);

} ]);

scotch.controller('WcPayController', function($scope, $location, $http) {

	var temp;
	var argu = $location.absUrl().split("?")[1];
	var getUrl = "../../product/get?";
	$scope.baseUrl = zimgeUrl;
	$scope.formatImage = "?p=0";
	$scope.amount = 1;

	// add 1
	$scope.addProduct = function() {
		$scope.amount = $scope.amount + 1;
	}
	// minus 1
	$scope.minusProduct = function() {
		if ($scope.amount == 1) {
			sweetAlert("亲，不能再减了哦");
			return;
		}
		$scope.amount = $scope.amount - 1;
	}

	/** 启动 */
	$scope.start = function() {
		$http.get(getUrl + argu).success(function(response) {
			console.log(response);
			temp = response.imageIds;
			$scope.pictures = temp.split(";");
			var slider = $('.am-slider').data('flexslider');
			angular.forEach($scope.pictures,function(data,index){
				slider.addSlide("<li><img src=\""+$scope.baseUrl+data+"\"></li>",0);
			});
			
		}).error(function() {
			alert("error");
		});
	}
	/** 跳转 */
	$scope.pay = function() {
		console.log($location);
		$http.get("../../user/look").success(function(response) {

			console.log(response);
			if (response.code!=0) {
//				$location.path('chinesejie/html/wcpay/verifyOrder.html').search('amount', $scope.amount);
				// 刷新页面
				window.location.href='verifyOrder.html?'+argu+"&amount="+$scope.amount;
			}else{
				$('#my-prompt').modal();
			}
		}).error(function() {
			
		});

	}
	/**login cancel*/
	$scope.cancel=function(){
		$('#my-prompt').modal("close");
	}
	$scope.login=function(phone,pwd){
		var user={'phone':phone,'password':pwd};
		$http({
			method  : "post",
			url     : '../../login',
		        data    : $.param(user),  // pass in data as strings
		        headers : { 'Content-Type': 'application/x-www-form-urlencoded'}  
			
		}).success(function(response){
			
			if(response.code==1){
				window.location.href='verifyOrder.html?'+argu+"&amount="+$scope.amount;
			}else if(response.code==0){
				sweetAlert("登录失败");
			}
			
		}).error();
	}
	

	$scope.start();
});