/**
 * 
 */
var adminDailyStatisticsController = function($http, $scope, PaginatorRR, $log) {
	$scope.pageSize = 10;
	$scope.dataStatistics = [];
	// $scope.checkDate = new Date();

	var fetchFunction = function(currentPage, limit, propId, callback) {

		$http.get('../orderbuffer/getSuperDay', {
			params : {
				'pageSize' : limit,
				'cpage' : currentPage,
				'date' : propId
			}
		}).success(callback);
	}

	$scope.check = function() {
		var checkedDate = "";
		checkedDate = $("#birthday_input").val();

		$http.get("../orderbuffer/superSum?date=" + checkedDate).success(function(response) {
			$scope.statisticsInfo = response.object;
		}).error();
	}
	// 查询店
	$scope.check2 = function() {
		var checkedDate2 = "";
		checkedDate2 = $("#birthday_input2").val();

		$http.get("../orderbuffer/superStoreSum?date=" + checkedDate2 + "&storeId=" + $scope.selectedStore).success(function(response) {
			$log.info(response);
			if (response.code == 1) {
				if (response.object != null) {
					var detailData = response.object.detail.split(";");
					$scope.storeStatisticsO = detailData[0].split('_');
					$scope.storeStatisticsG = detailData[1].split('_');
					$scope.storeStatisticsT = detailData[2].split('_');
					$scope.storeStatisticsMoney=parseFloat($scope.storeStatisticsO[2])+parseFloat($scope.storeStatisticsG[2]) + parseFloat($scope.storeStatisticsT[2]);
				}
				$scope.storeStatisticsO = ["o",0,0];
				$scope.storeStatisticsG = ["G",0,0];
				$scope.storeStatisticsT = ["T",0,0];
				$scope.storeStatisticsMoney=0;
				$scope.storeStatistics = response.object;
			} else {
				sweetAlert(response.info);
			}
		}).error();
	}

	$scope.check3 = function() {
		var checkedDate3 = "";
		checkedDate3 = $("#birthday_input3").val();

		$scope.dataStatistics[0] = PaginatorRR(fetchFunction, $scope.pageSize, checkedDate3);
	}

	$scope.edit = function(order) {
		var urlPath = "../orderbuffer/superGet?uuid=" + order.uuid;
		var offCanvas = "";
		switch (order.orderType) {
		case 2:
			offCanvas = "#group_ui";
			break;
		case 3:
			offCanvas = "#ticket_ui";
			break;
		case 1:
			offCanvas = "#new_ui";
			break;
		default:
			return;
		}

		$http.get(urlPath).success(function(response) {
			if (response.code == 1) {
				$scope.norder = response.object;
				if (offCanvas == "#group_ui") {
					$http.get("../groupinstance/get?groupinstanceId=" + $scope.norder.groupInstance.id).success(function(response) {
						$scope.groupOwners = response.object;
					});
				}
				$(offCanvas).offCanvas('open');
			} else if (response.code == 0) {
				sweetAlert(response.info);
			}
		}).error();

	}

	$scope.closeNew = function() {
		$("#new_ui").offCanvas('close');
	}
	$scope.closeNew2 = function() {
		$("#ticket_ui").offCanvas('close');
	}
	$scope.closeNew3 = function() {
		$("#group_ui").offCanvas('close');
	}

	$scope.showPay = function() {
		var checkedDate2 = $("#birthday_input2").val();

		$('#my-confirm').modal({
			relatedTarget : this,
			onConfirm : function(options) {
				if($scope.storeStatisticsMoney<=0){
					sweetAlert("支付金额不能为空！");
					return;
				}
				$http.get('../orderbuffer/superPay?date=' + checkedDate2 + "&storeId=" + $scope.selectedStore).success(function(data) {
					if (data.code == 1) {
						sweetAlert("付款成功成功");
					} else {
						sweetAlert(data.info);
					}

				}).error(function() {
					sweetAlert("付款失败");
				});
			},
			onCancel : function() {
			}
		});
	}

	$scope.start = function(response) {
		$scope.check();

		$http.get("../store/list").success(function(response) {
			$scope.stores = response;
			$scope.selectedStore = $scope.stores[0].id;
			$scope.check2();
			$scope.check3();
		}).error();
	}

	$scope.start();

}