/**
 * 
 */

var barcodeScannerController = function($http,$scope, $location,$log) {

	$scope.App = "";
	$scope.flag = true;
	$scope.pictures = [];
	$scope.status = true;
	$scope.result = "";

	$scope.start = function() {
		$scope.App = {
			init : function() {
				Quagga.init({
					inputStream : {
						name : "Live",
						type : "LiveStream"
					},
					decoder : {
						readers : [ "code_128_reader" ]
					}
				}, function() {
					$scope.App.attachListeners();
					Quagga.start();
				});
			},
			attachListeners : function() {

				$("#controls").on("click", function(e) {
					e.preventDefault();
					Quagga.stop();
					$("#controls").off("click");
				});
			},
			detachListeners : function() {
				// $(".controls .reader-group").off("change",
				// "input");
				$("#controls").off("click");
			},
			lastResult : null
		};

		Quagga.onProcessed(function(result) {
			var drawingCtx = Quagga.canvas.ctx.overlay, drawingCanvas = Quagga.canvas.dom.overlay;

			if (result) {
				if (result.boxes) {
					drawingCtx.clearRect(0, 0, parseInt(drawingCanvas.getAttribute("width")), parseInt(drawingCanvas.getAttribute("height")));
					result.boxes.filter(function(box) {
						return box !== result.box;
					}).forEach(function(box) {
						Quagga.ImageDebug.drawPath(box, {
							x : 0,
							y : 1
						}, drawingCtx, {
							color : "green",
							lineWidth : 2
						});
					});
				}

				if (result.box) {
					Quagga.ImageDebug.drawPath(result.box, {
						x : 0,
						y : 1
					}, drawingCtx, {
						color : "#00F",
						lineWidth : 2
					});
				}

				if (result.codeResult && result.codeResult.code) {
					Quagga.ImageDebug.drawPath(result.line, {
						x : 'x',
						y : 'y'
					}, drawingCtx, {
						color : 'red',
						lineWidth : 3
					});
				}
			}
		});
		Quagga.onDetected(function(result) {
			var code = result.codeResult.code;
			if ($scope.App.lastResult !== code) {
				$scope.App.lastResult = code;
				var $node = null, canvas = Quagga.canvas.dom.image;
				Quagga.stop();
				$scope.$apply(function() {
					$scope.pictures.splice(0, 0, {
						'code' : code,
						'src' : canvas.toDataURL()
					});
					$scope.result = code;
					$scope.status = true;
				});
			}
		});
	}

	$scope.chooseResult = function(picture) {
		$scope.result = picture.code;
	}

	$scope.barScanner = function() {
		if ($scope.flag) {
			$scope.App.init();
		} else {
			$scope.App.start();
		}
		$scope.status = false;
	}

	$scope.submit = function() {
		if ($scope.result == "") {
			return;
		}
		// window.location.reload();
		// alert($scope.result);
		$location.path("barcodeOrders").search('uuid', $scope.result);

	}
	
	//管理员登录
//	$scope.login = function(phone, pwd) {
//		var user = {
//			'phone' : phone,
//			'password' : pwd
//		};
//
//		$http({
//			method : "post",
//			url : '../../',
//			data : $.param(user), // pass in data
//			headers : {
//				'Content-Type' : 'application/x-www-form-urlencoded'
//			}
//
//		}).success(function(response) {
//			$log.info(response);
//			if (response.code == 1) {
//				alert("登录成功");
//			} else if (response.code == 0) {
//				alert("登录失败");
//			}
//
//		}).error();
//	}

	$scope.backHome=function(){
		window.location.href="../admin-index.html#/";
	}
	
	$scope.start();
}