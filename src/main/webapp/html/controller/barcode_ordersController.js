/**
 * 
 */
// TODO 团订单的测试 没做
var barcode2OrdersController = function($location, $http, $scope, $log) {

	$scope.deliveryPath = "";
	$scope.loginStatus = false;

	$scope.start = function() {
		var temp = $location.search().uuid.substr(0, 1);
		var urlPath;
		switch (temp) {

		case 'q':
			urlPath = "../../gorder/getGroupOrder?uuid=";
			$scope.orderProductStatus = true;
			$scope.deliveryPath = "../../gorder/sureGroupOrder?uuid=";
			break;
		case 't':
			urlPath = "../../order/getTicketOrder?uuid=";
			$scope.orderProductStatus = false;
			$scope.deliveryPath = "../../order/diliveryTicket?uuid=";
			break;
		case 'o':
			urlPath = "../../order/getDiliverOrder?uuid=";
			$scope.orderProductStatus = true;
			$scope.deliveryPath = "../../order/dilivery?uuid=";
			break;
		default:
			alert("无此订单");
			return;
		}

		$http.get(urlPath + $location.search().uuid).success(function(response) {
			$log.info(response);

			if (response.code == 1) {
				$scope.order = response.object;

				if (temp == 'q') {
					$scope.order.orderDetails = [ {
						'product' : {
							'price' : $scope.order.group.personPrice,
							'description' : $scope.order.group.product.description
						},
						'amount' : 1
					} ];
				}

			} else if (response.code == 0) {

				alert(response.info);
				// window.location.href="../admin-login.html";
				$location.path("barcodeScanner");
				return;
			} else {
				alert(response.info);
				$scope.loginStatus = false;
			}
		}).error(function() {

		});
	}

	/** 提货 */
	$scope.delivery = function(order) {

		if (order == "") {
			sweetAlert("错误操作");
			return;
		}
		$http.get($scope.deliveryPath + $location.search().uuid).success(function(response) {
			if (response.code == 1) {
				alert("提货成功");
				// window.location.href
				// ="camera-index.html#/barcodeScanner";
				$location.path("barcodeScanner");
			} else {
				alert(response.info);
				return;
			}
		}).error(function() {

		});
	}

	$scope.backHome = function() {
		window.location.href = "../admin-index.html#/";
	}

	// 管理员登录
	$scope.login = function(phone, pwd) {
		var user = {
			'phone' : phone,
			'password' : pwd
		};

		$http({
			method : "post",
			url : '../../adminlogin',
			data : $.param(user), // pass in data
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}

		}).success(function(response) {
			$log.info(response);
			if (response.code == 1) {
				alert("登录成功");
				$scope.loginStatus = true;
			} else if (response.code == 0) {
				alert("登录失败");
			}

		}).error();
	}

	$scope.start();
}