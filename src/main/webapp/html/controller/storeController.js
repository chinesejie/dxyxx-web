/**
 * 
 */
var storeController=function($scope,$http,$log){
	
	$scope.baseUrl = zimgeUrl;
	$scope.formatImage = "?p=0";
	
	$scope.start=function(){
		$http.get('../store/getByManger').success(function(data){
			$log.info(data);
			$scope.stores=data;
		}).error();
	}
	
	/**
	 * for single picture
	 */
	$scope.pass = function(thumbId) {

		$scope.$parent.singlepicture = $scope.baseUrl + thumbId + "?p=0";
	}
	
	$scope.start();
}