/**
 * 
 */

/**
 * 
 */
var verifyOrderController = function($scope, $location, $http, $window, localStorageService, $log) {

	// var temp = $location.absUrl().split("?")[1];
	// var argus = temp.split("&");
	// var arguId = $scope.arguId = argus[0];
	// var getUrl = "../../product/get?";
	// var amount = argus[1];
	// $scope.amount = amount.split("=")[1];
	$scope.baseUrl = zimgeUrl;
	$scope.products = [];
	$scope.totalPrice = 0;
	$scope.bufferIntervalName = "当天";
	$scope.bufferInterval = 0;

	$scope.showLoading = false;

	$scope.start = function() {

		var keys = localStorageService.keys();
		if (keys == null || keys == "") {
			sweetAlert("请添加商品");
			return;
		}
		for ( var item in keys) {
			// 拿到总价格和所有要买的东西
			$scope.totalPrice = $scope.totalPrice + localStorageService.get(keys[item]).product.price * localStorageService.get(keys[item]).amount;
			$scope.products.push(localStorageService.get(keys[item]));
		}

		$http.get('../../store/list').success(function(response) {
			$scope.stores = response;
		});
	}

	$scope.addOrder = function() {

		var description = "描述";
		var payType = "";
		if (angular.isUndefined($scope.payType)) {
			sweetAlert("请选择付款方式");
			return;
		}

		if (angular.isUndefined($scope.storeSelected)) {
			sweetAlert("请选择店面");
			return;
		}

		if ($scope.payType == "支付宝") {
			payType = "alipay";
		} else {
			payType = "tenpay"
		}

		// GetID
		var orderDetailsData = [];
		var products = $scope.products;
		for ( var item in products) {
			orderDetailsData.push({
				"amount" : products[item].amount,
				"price" : products[item].product.price,
				"productId" : products[item].product.id
			});
		}

		var orderDetails = JSON.stringify(orderDetailsData);

		var data = {
			"description" : description,
			"orderDetails" : orderDetails,
			"payType" : payType,
			"deliveryType" : $scope.deliveryType,
			"storeId" : $scope.storeSelected.id,
			bufferInterval : $scope.bufferInterval
		};

		$http({
			method : "post",
			url : '../../order/add',
			data : $.param(data), // pass in data as strings
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}

		}).success(function(response) {

			if (response.code == 1) {

				$scope.showLoading = true;
				window.location.href = "../../mainServlet?uuid=" + response.object.uuid;
			} else {
				sweetAlert(response.info);
				$('#my-prompt').modal();
				return;
			}
		}).error(function() {

		});

	}

	$scope.login = function(phone, pwd) {
		var password;
		$http.get("../../encryption/transfer?plain=" + pwd ).success(function(response) {
			password=response.object;
		}).then(function() {
			
			var user = {
				'phone' : phone,
				'password' : password
			};
			
			$http({
				method : "post",
				url : '../../login',
				data : $.param(user), // pass in data
				headers : {
					'Content-Type' : 'application/x-www-form-urlencoded'
				}

			}).success(function(response) {
				$log.info(response);
				if (response.code == 1) {
					sweetAlert("登录成功");
					$('#my-prompt').modal("close");
				} else if (response.code == 0) {
					sweetAlert("登录失败");
				}

			}).error();
		});

	}

	$scope.showLogin = function() {
		$('#my-prompt').modal();
	}
	/** login cancel */
	$scope.cancel = function() {
		$('#my-prompt').modal("close");
	}

	/** login cancel */
	$scope.cancelPayType = function() {
		$('#doc-modal-3').modal("close");
	}

	/** login cancel */
	$scope.cancelDeliveryType = function() {
		$('#doc-modal-1').modal("close");
	}

	$scope.cancelbufferIntervalSelect = function(bufferInterval) {
		$log.info(bufferInterval);
		switch (bufferInterval) {
		case '0':
			$scope.bufferIntervalName = "当天";
			break;
		case '1':
			$scope.bufferIntervalName = "第二天";
			break;
		case '2':
			$scope.bufferIntervalName = "第三天";
			break;

		default:
			$scope.bufferIntervalName = "当天";
			break;
		}
		$('#doc-modal-2').modal("close");
	}

	$scope.cancelStoreSelect = function(storeSelected) {

		if (storeSelected == "" || angular.isUndefined(storeSelected)) {
			sweetAlert("请选择");
		} else {
			$('#doc-modal-4').modal("close");
		}
	}

	/** 返回 */
	$scope.goBack = function() {
		$window.history.go(-1);
		// $location.path("wcPay").search();
		// window.location.href='wcPay.html?'+arguId;
	}

	$scope.start();
};