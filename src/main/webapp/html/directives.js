
'use strict'
/* Directives */

var AppDirectives = angular.module('scotchApp.directives', []);

AppDirectives.directive('focusinput', function($timeout) {
	return {
		link: function(scope, element, attrs) {
			element.bind('click', function() {
				$timeout(function() {
					//去查找真正要被点击的标签
					element.parent().find('input')[0].click();
				});
			});
		}
	}

	 /*return {
		 link: function(scope, element,attrs) {
			
 			 	element.bind('click', function() {
 			 		$bind_click(function() {
 			 			$scope.inputh.click();
 			 		 console.log('来了')}
 	  		});
		 }}*/});



AppDirectives.directive('focuse',
	    function() {
	  return {
	    link: function(scope, element, attrs) {
	      element[0].focus();
	    }
	  };
	});

/*************************************************************/
/**
 * datatimePicker 指令
 */

angular.module('dateTimePicker',[])
	.directive('datepicker',function(){
		return {
//			restrict: 'A',
			require: '?ngModel',
			scope:{
				select:'&'
			},
			link: function(scope,element,attrs,ngModel){
				if(!ngModel)
					return;
				var optionsObj={};
				
				optionsObj.dataFormat='yy/mm/dd';
				var updataModel=function(dataText){
					$scope.$apply(function(){
						ngModel.$setViewValue(dataText);
					});
				};
				
				optionsObj.onSelect=function(dateText,picker){
					updataModel(dateText);
					if(scope.select){
						scope.$apply(function(){
							scope.select({date:dateText})
						});
					}
				};
				
				ngModel.$render=function(){
					element.datepicker('setDate',ngModel.$viewValue || '' );
				};
				element.datepicker(optionsObj);
			}
		}
	});

/**	tabs的指令*/
AppDirectives.directive('tabdir',function(){
	return{
		restrict: 'EA',
		link: function(scope,element,attrs){
			element.find('a').on('open.tabs.amui',function(event){
				scope.getTab(this.attributes.value.value);
			});
		}
	}
});





