/**
 * 
 */
var AttendGroupController = function($scope, $http, $location, $log) {

	$scope.baseUrl = zimgeUrl;
	$scope.formatImage = "?p=0";
	$scope.arguments = $location.search();

	$scope.start = function() {

		$http.get("../../groupinstance/check?groupinstanceId=" + $scope.arguments.groupinstanceId).success(function(response) {
			$log.info(response);
			if (response.code == 0) {
				sweetAlert(response.info);
				return;
			} else {
				$scope.group = response.object;
			}
		}).error().then(function() {
			
			$http.get("../../groupinstance/get?groupinstanceId=" + $scope.arguments.groupinstanceId ).success(function(response){
				$scope.orders=response.object;
			});
			
			$http.get("../../productDetail/get?productId=" + $scope.group.group.product.id).success(function(response) {

				if (response != null && response != "") {
					$scope.productDetails = angular.fromJson(response);
					$scope.Details = eval('(' + $scope.productDetails.details + ')');
				}
			});
		});

	}

	$scope.directShop = function() {
		window.location.href = "native://group?action=attend&groupinstanceId=" + $scope.arguments.groupinstanceId;
	}

	$scope.start();
}