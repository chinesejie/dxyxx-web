/**
 * 
 */
var GroupController = function($http, $scope, $location,$timeout, $log) {
	$scope.baseUrl = zimgeUrl;
	$scope.formatImage = "?p=0";
	$scope.items = [];
	$scope.products;
	
	Array.prototype.distinct = function() {
		for (var i = 0; i < this.length; i++) {
			var n = this[i];
			this.splice(i, 1, null);
			if (this.indexOf(n) < 0) {
				this.splice(i, 1, n);// 不存在重复
			} else {
				this.splice(i, 1);// 存在重复
			}
		}
		return this;
	};

	$scope.start = function() {
		$http.get("../../group/all").success(function(response) {

			var temp = [];
			
			for ( var i=0;i<response.length-1;i++){
				$log.info(response[i]);
				temp.push(response[i].productId);
			}
			
			products = temp.distinct();

			for ( var i in response) {
				var temp = products.indexOf(response[i].productId);
				if (angular.isUndefined($scope.items[temp])) {
					$scope.items[temp] = response[i];
					$scope.items[temp].personAmount = [ response[i].personAmount ];
				} else {
					$scope.items[temp].personAmount.push(response[i].personAmount);
				}
			}
//			$log.info($scope.items);
		});
	}

	$scope.check = function(groupId4Serach) {
		if(groupId4Serach==null || groupId4Serach==""){
			sweetAlert("请输入团号！");
			return;
		}
		$http.get("../../groupinstance/check?groupinstanceId=" + groupId4Serach).success(function(response) {
			if (response.code == 1) {
				$('#my-prompt').modal("close");
//				$timeout(function(){
				$location.path("attend").search("groupinstanceId", groupId4Serach);
//				},200);
				
			} else {
				
				sweetAlert(response.info);
			}
		}).error();
	}

	$scope.goDetail = function(item) {
		$location.path('detail').search("productId", item.product.id);
	}

	$scope.start();
}