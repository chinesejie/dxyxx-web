/**
 * 
 */

var zimgeUrl = '';
var uploadUrl = '';
var getImageInfo = '';

var groupApp = angular.module('groupApp', [ 'ngRoute','ConfigService' ]);

// configure our routes
groupApp.config(function($routeProvider,$configProvider ) {
	zimgeUrl = $configProvider.zimgeUrl;
	uploadUrl = $configProvider.uploadUrl;
	getImageInfo = $configProvider.getImageInfo;

	$routeProvider

	.when('/detail', {
		// url：路径是以当前的页面为基准的相对路径
		templateUrl : 'groupDetail.html',
		controller :groupDetailController
	})
	
	.when('/attend',{
		templateUrl:'attendGroup.html',
		controller : AttendGroupController
	})

	.when('/list', {
		// url：路径是以当前的页面为基准的相对路径
		templateUrl : 'group.html',
		controller : GroupController
	});
	
//	.when('/shopCart',{
//		templateUrl: 'shoppingCart.html',
//		controller : ShopCartController
//	})
//
//	.when('/wcPay', {
//		templateUrl : 'wcPay.html',
//		controller : testController
//	});

});