/**
 * 
 */
var groupDetailController = function($http, $scope, $location, $log) {

	$scope.baseUrl = zimgeUrl;
	$scope.arguments = $location.search();
	$scope.formatImage = "?p=0";

	$scope.start = function() {
		$http.get("../../group/detailsByPid?productId=" + $scope.arguments.productId).success(function(response) {
			$log.info(response);
			if(response==null){
				alert("打开了错误的页面");
				return;
			}
			$scope.items = response.object;
			
		}).then(function() {

			$http.get("../../productDetail/get?productId=" + $scope.arguments.productId).success(function(response) {
				$log.info(response);
				if (response != null && response != "") {
					$scope.productDetails = angular.fromJson(response);
					$scope.Details = eval('(' + $scope.productDetails.details + ')');
				}
			});
		});
	}
	
	$scope.shop=function(item){
		
		window.location.href="native://group?action=add&groupId="+item.id;
	}
	
	$scope.directShop=function(id){
		window.location.href="native://order?pid="+id;
	}
	
	$scope.start();
}