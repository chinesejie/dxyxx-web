/**
 * 
 */

angular.module('PaginatorRRService', []).factory('PaginatorRR', function() {
	return function(fetchFunction, pageSize, propId) {
		var paginator = {
			hasNextVar : false,
			next : function() {
				if (this.hasNextVar) {
					this.currentPage += 1;
					this._load();
				}
			},
			_load : function() {
				var self = this;
				fetchFunction(this.currentPage, pageSize, propId, function(items) {
					if (items.code == 1) {
						self.currentPageItems = items.object.recordList.slice(0, pageSize);
						self.hasNextVar = items.object.recordList.length == pageSize;
						self.recordCount = items.object.recordCount;
						self.pages = Math.ceil(self.recordCount / pageSize);
					}
				});
			},
			hasNext : function() {
				return this.hasNextVar;
			},
			previous : function() {
				if (this.hasPrevious()) {
					this.currentPage -= 1;
					this._load();
				}
			},
			hasPrevious : function() {
				return this.currentPage !== 0;
			},
			currentPage : 1,
			currentPageItems : [],
			pages : 0,
			recordCount : 0
		};
		paginator._load();
		return paginator;
	};
});