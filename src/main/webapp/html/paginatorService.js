/**
 * 
 */

angular.module('PaginatorService', []).factory('Paginator', function() {
	return function(fetchFunction, pageSize, propId) {
		var paginator = {
			hasNextVar : false,
			next : function() {
				if (this.hasNextVar) {
					this.currentPage += 1;
					this._load();
				}
			},
			_load : function() {
				var self = this;
				fetchFunction(this.currentPage, pageSize, propId, function(items) {
					self.currentPageItems = items['recordList'].slice(0, pageSize);
					self.recordCount = items['recordCount'];
					self.hasNextVar = items['recordList'].length == pageSize;
					self.pages=Math.ceil(self.recordCount / pageSize);
				});
			},
			hasNext : function() {
				return this.hasNextVar;
			},
			previous : function() {
				if (this.hasPrevious()) {
					this.currentPage -= 1;
					this._load();
				}
			},
			hasPrevious : function() {
				return this.currentPage !== 0;
			},
			currentPage : 1,
			currentPageItems : [],
			pages: 0,
			recordCount:0
		};
		paginator._load();
		return paginator;
	};
});