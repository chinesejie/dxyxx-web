// create the module and name it scotchApp
// 模块加入时要注意

var zimgeUrl='';
var uploadUrl='';
var getImageInfo='';

var scotchSuperApp = angular.module('scotchSuperApp', 
		['PaginatorService','PaginatorRRService','dateTimePicker','frapontillo.bootstrap-switch',
		 'scotchApp.directives',
                 'ngRoute','angularFileUpload','ConfigService']);

// configure our routes
scotchSuperApp.config(function($routeProvider,$configProvider) {
	zimgeUrl = $configProvider.zimgeUrl;
	uploadUrl = $configProvider.uploadUrl;
	getImageInfo = $configProvider.getImageInfo;
	$routeProvider
	
	.when('/user',{
		templateUrl : 'user/user.html',
		controller : UserController
	})
	
	.when('/adminInfo',{
		templateUrl : 'adminInfo/adminInfo.html',
		controller : AdminInfoController
	})

	.when('/admin',{
		templateUrl : 'admin/admin.html',
		controller : AdminController
	})

	// route for the home page
	.when('/', {
		// url：路径是以当前的页面为基准的相对路径
		templateUrl : 'index.html',
		controller : IndexController
	})
	
	.when("/dailyStatistics",{
		templateUrl : 'adminstatistics/dailyStatistics.html',
		controller : adminDailyStatisticsController
	})
	
	.when("/monthStatistics",{
		templateUrl : 'adminstatistics/MonthStatistics.html',
		controller : AdminMonthStatisticsController
	});

});

// create the controller and inject Angular's $scope
scotchSuperApp.controller('mainController', function($scope, $http,$timeout) {
	$scope.showBar = false;

	$http.get("../user/look").success(function(response) {
		if (response.code == 1) {
			$scope.showBar = true;
			$scope.phone = response.object.phone;
		} else {
			$scope.showBar = false;
			window.location.href = "admin-login.html";
		}
	});
	

	$scope.logout = function() {
		$http.get("../user/logout").success(function(response) {

			if (response.code == 0) {
				sweetAlert(response.info);
				return;
			} else {

				window.location.href = "admin-login.html";
			}
		});
	}

});

scotchSuperApp.controller('aboutController', function($scope) {
	$scope.message = 'Look! I am an about page.';
});

scotchSuperApp.controller('contactController', function($scope) {
	$scope.message = 'Contact us! JK. This is just a demo.';
});