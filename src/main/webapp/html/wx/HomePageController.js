/**
 * 
 */
var HomePageController=function($scope,$http,$location,$log){
	
	$scope.baseUrl = zimgeUrl;
	$scope.formatImage = "?p=0";
	
	$scope.start=function(){
		$http.get("../../banner/get").success(function(response){
			
			$log.info(response);
			$scope.homePageBanners = response.homePageBanners;

			var slider = $('#am-slider').data('flexslider');
			angular.forEach(response.topBanners, function(data, index) {
				slider.addSlide("<li><img src=\"" + $scope.baseUrl + data.imageId + $scope.formatImage + "\"></li>", index);
			});
			slider.removeSlide($("#first"));
		}).then(function(){
//			if($location.search().type=="ios"){
//				$scope.downloadLink=IOSdownLoadLink;
//			}else if($location.search().type=="android"){
//				$scope.downloadLink=AndroidLoadLink;
//			}
			$scope.downloadLink=AndroidLoadLink;// 统一了
		});
	}
	
	
	$scope.showLogin = function() {
		$('#my-prompt').modal();
	}
	
	/** login cancel */
	$scope.cancel = function() {
		$('#my-prompt').modal("close");
	}
	$scope.login = function(phone, pwd) {
		var user = {
			'phone' : phone,
			'password' : pwd
		};

		$http({
			method : "post",
			url : '../../login',
			data : $.param(user), // pass in data
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}

		}).success(function(response) {
			//$log.info(response);
			if (response.code == 1) {
				$('#my-prompt').modal("close");
				sweetAlert("登录成功");
				$location.path("verifyOrder").search({
					"id" : argu.split("=")[1]
				});
			} else if (response.code == 0) {
				sweetAlert("登录失败");
			}

		}).error();
	}
	
	// 进入购物车
	$scope.shoppingCart = function() {

		$location.path("shopCart");
	}
	
	$scope.goToDetail=function(homePageBanner){
		$location.path("wcPay").search("id",homePageBanner.product.id);
	}
	
	$scope.start();
} 