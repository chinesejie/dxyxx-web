/**
 * 如果有code，就更新openid.
 * 但是code 有可能是 用过无效的。
 */
var MeController = function($scope, $http, $location, $log,localStorageService ) {
	
	var data = $location.search().code;// 得到url 传过来的code 
	var openid = localStorageService.get('oid');
	$scope.start = function() {
		if (!openid) {// openid不存在 就请求。。保证返回的时候， 不会再次触发 这个 http请求
			if (data) {
				$http.get("../../weixin/openid?code=" + data).success(
						function(response) {
							if (response.code == 1) {
								$scope.openId = response.object;
								// 存储 openid
								localStorageService.set('oid',response.object);
							} else {
								 
								alert(response.info);
								return;
							}

						});
			}
		}
	}

	$scope.MyCard = function() {
		$location.path("myCard").search("code", $scope.openId);
	}

	$scope.MyComment = function() {
		$location.path("myComment").search("code", $scope.openId);
	}
	$scope.myInfo = function() {
		$location.path("intro").search("code", $scope.openId);
	}

	$scope.start();

}