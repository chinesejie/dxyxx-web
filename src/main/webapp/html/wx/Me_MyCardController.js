/**
 * 
 */
var MyCardController = function($scope, $http, $location,$window,localStorageService) {

	var data = localStorageService.get('oid'); // 就是openid
	if (data) {
		$http.get("../../weixin/barcode2?openId=" + data).success(
				function(response) {
					if (response.code == 1) {
						JsBarcode(document.getElementById("rotImg"),
								response.object, {
									quite : 10,
									format : "CODE128",
									displayValue : true,
									font : "monospace",
									textAlign : "center",
									fontSize : 12,
									backgroundColor : "",
									lineColor : "#000"
								});
					} else {
						alert(response.info);
						return;
					}
				}).error();
	}else{
		window.location.href = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx1d8e9a967a8c62bd&redirect_uri=https%3a%2f%2frs88881234.com%2fchinesejie%2fhtml%2fcode128.html&response_type=code&scope=snsapi_base&state=123#wechat_redirect";
	}
	
	/** 返回 */
	$scope.goBack = function() {
		$window.history.go(-1);
	}
	
}