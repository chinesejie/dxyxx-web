/**
 * 
 */
var MyInfoController = function($scope, $location, $http, $window,localStorageService) {
	var data = localStorageService.get('oid'); // 就是openid
	if (data) {// openid 不为空， 可以发出 请求获取会员信息
		$http.get("../../weixin/info?openId=" + data).success(
				function(response) {
					if (response.code == 1) {
						$scope.admin = response.object;
					} else {
						alert("抱歉，您还没有绑定会员");
					}
				});
	} else {
		alert("抱歉，您还没有绑定会员");
	}

	/** 返回 */
	$scope.goBack = function() {
		$window.history.go(-1);
	}
}