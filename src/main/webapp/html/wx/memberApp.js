// create the module and name it scotchApp
// 模块加入时要注意

var scotchApp = angular.module('scotchApp', [ 'ngRoute' ,'LocalStorageModule' ]);

// configure our routes
scotchApp.config(function($routeProvider ,localStorageServiceProvider  ) {

	localStorageServiceProvider.setPrefix('scotchApp');
	localStorageServiceProvider.setStorageType('localStorage');
	$routeProvider
	
	.when('/',{
		templateUrl : 'memberIndex.html',
		controller :  MeController
	})


	.when('/intro', {
		// url：路径是以当前的页面为基准的相对路径
		templateUrl : 'me_myinfo.html',
		controller : MyInfoController
	})

	.when('/myCard', {
		templateUrl : 'me_mycard.html',
		controller : MyCardController
	})
	
	.when('/myComment',{
		templateUrl : 'me_mycomment.html',
		controller : MyCommentController
	})

});