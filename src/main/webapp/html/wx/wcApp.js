// create the module and name it scotchApp
// 模块加入时要注意

var zimgeUrl = '';
var uploadUrl = '';
var getImageInfo = '';
var IOSdownLoadLink="http://a.app.qq.com/o/simple.jsp?pkgname=com.dxyxx&g_f=991653";
var AndroidLoadLink="http://a.app.qq.com/o/simple.jsp?pkgname=com.dxyxx&g_f=991653";

var scotchApp = angular.module('scotchApp', [ 'ngRoute','LocalStorageModule','ConfigService' ]);

// configure our routes
scotchApp.config(function($routeProvider, localStorageServiceProvider,$configProvider) {
	zimgeUrl = $configProvider.zimgeUrl;
	uploadUrl = $configProvider.uploadUrl;
	getImageInfo = $configProvider.getImageInfo;
	localStorageServiceProvider.setPrefix('scotchApp');
	localStorageServiceProvider.setStorageType('localStorage');
	
	

	$routeProvider
	
	.when('/',{
		templateUrl : 'homePage.html',
		controller :  HomePageController
	})


	.when('/verifyOrder', {
		// url：路径是以当前的页面为基准的相对路径
		templateUrl : 'verifyOrder.html',
		controller : verifyOrderController
	})
	
	.when('/shopCart',{
		templateUrl: 'shoppingCart.html',
		controller : ShopCartController
	})

	.when('/wcPay', {
		templateUrl : 'wcPay.html',
		controller : testController
	});

});
