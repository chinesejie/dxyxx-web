package org.shiro.demo.junit;

import static java.lang.System.out;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.JavaType;
import org.junit.Test;
import org.shiro.demo.entity.OrderDetail;

public class test4Jackson extends  TestCase{
	
	@Test
	public void test4Jackson(){
		String data="[{\"amount\":1,\"id\":1,\"price\":23.0,\"product\":null,\"productId\":2},{\"amount\":1,\"id\":1,\"price\":23.0,\"product\":null,\"productId\":1}]";
		ObjectMapper om=new ObjectMapper();
		try {
			//用map更加自由
			ObjectMapper ob = new ObjectMapper();
			JavaType javaType = ob.getTypeFactory().constructParametricType(
					ArrayList.class, OrderDetail.class);
			List<OrderDetail> results= (List<OrderDetail>) ob.readValue(data, javaType);
			
			List<Long> productIds=new ArrayList<Long>();
			
			JsonNode node= om.readTree(data);
		    for (JsonNode jsonNode : node.findValues("productId")) {
		    	productIds.add(jsonNode.getLongValue());
			} 	
			out.println(productIds.toString());
			
			
//			JsonNode node= om.readTree(data);
//			System.out.println(node.findValues("productId"));
			
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
}
