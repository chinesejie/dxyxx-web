package org.shiro.demo.junit;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.eclipse.persistence.internal.jpa.parsing.SetNode;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.shiro.demo.entity.Administrator;
import org.shiro.demo.entity.Category;
import org.shiro.demo.entity.DailySpecial;
import org.shiro.demo.entity.OrderProp;
import org.shiro.demo.entity.Permission;
import org.shiro.demo.entity.Product;
import org.shiro.demo.entity.Role;
import org.shiro.demo.entity.Store;
import org.shiro.demo.entity.User;
import org.shiro.demo.service.IBaseService;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml", "classpath:spring-mvc.xml" })
@TransactionConfiguration(transactionManager = "txManager", defaultRollback = false)
public class testInitSystemData extends AbstractTransactionalJUnit4SpringContextTests {

	@Resource(name = "baseService")
	private IBaseService baseService;

	@Test
	public void initPermission() throws Exception {
		List<Permission> list = new ArrayList<Permission>();

		Permission pmss1 = new Permission();
		pmss1.setName("新建顶栏横幅");
		pmss1.setDescription("新建顶栏横幅");
		pmss1.setPermission("topBanner:create");

		Permission pmss2 = new Permission();
		pmss2.setName("删除顶栏横幅");
		pmss2.setDescription("删除顶栏横幅");
		pmss2.setPermission("topBanner:delete");

		Permission pmss3 = new Permission();
		pmss3.setName("编辑顶栏横幅");
		pmss3.setDescription("编辑顶栏横幅");
		pmss3.setPermission("topBanner:edit");

		Permission pmss4 = new Permission();
		pmss4.setName("查看顶栏横幅");
		pmss4.setDescription("查看顶栏横幅");
		pmss4.setPermission("topBanner:query");
		list.add(pmss1);
		list.add(pmss2);
		list.add(pmss3);
		list.add(pmss4);
		for (Permission pms : list) {
			baseService.save(pms);
		}
	}

	@Test
	public void initPermission2() throws Exception {
		List<Permission> list = new ArrayList<Permission>();

		Permission pmss1 = new Permission();
		pmss1.setName("新建首页横幅");
		pmss1.setDescription("新建首页横幅");
		pmss1.setPermission("homepageBanner:create");

		Permission pmss2 = new Permission();
		pmss2.setName("删除首页横幅");
		pmss2.setDescription("删除首页横幅");
		pmss2.setPermission("homepageBanner:delete");

		Permission pmss3 = new Permission();
		pmss3.setName("编辑首页横幅");
		pmss3.setDescription("编辑首页横幅");
		pmss3.setPermission("homepageBanner:edit");

		Permission pmss4 = new Permission();
		pmss4.setName("查看首页横幅");
		pmss4.setDescription("查看首页横幅");
		pmss4.setPermission("homepageBanner:audit");
		list.add(pmss1);
		list.add(pmss2);
		list.add(pmss3);
		list.add(pmss4);
		for (Permission pms : list) {
			baseService.save(pms);
		}
	}

	@Test
	public void initPermission3() throws Exception {
		List<Permission> list = new ArrayList<Permission>();

		Permission pmss1 = new Permission();
		pmss1.setName("新建品类");
		pmss1.setDescription("新建品类");
		pmss1.setPermission("category:create");

		Permission pmss2 = new Permission();
		pmss2.setName("删除品类");
		pmss2.setDescription("删除品类");
		pmss2.setPermission("category:delete");

		Permission pmss3 = new Permission();
		pmss3.setName("编辑品类");
		pmss3.setDescription("编辑品类");
		pmss3.setPermission("category:edit");

		Permission pmss4 = new Permission();
		pmss4.setName("查看品类");
		pmss4.setDescription("查看品类");
		pmss4.setPermission("category:query");

		list.add(pmss1);
		list.add(pmss2);
		list.add(pmss3);
		list.add(pmss4);
		for (Permission pms : list) {
			baseService.save(pms);
		}
	}

	@Test
	public void initPermission4() throws Exception {
		List<Permission> list = new ArrayList<Permission>();

		Permission pmss1 = new Permission();
		pmss1.setName("新建产品");
		pmss1.setDescription("新建产品");
		pmss1.setPermission("product:create");

		Permission pmss2 = new Permission();
		pmss2.setName("删除产品");
		pmss2.setDescription("删除产品");
		pmss2.setPermission("product:delete");

		Permission pmss3 = new Permission();
		pmss3.setName("编辑产品");
		pmss3.setDescription("编辑产品");
		pmss3.setPermission("product:edit");

		Permission pmss4 = new Permission();
		pmss4.setName("查看产品");
		pmss4.setDescription("查看产品");
		pmss4.setPermission("product:query");

		list.add(pmss1);
		list.add(pmss2);
		list.add(pmss3);
		list.add(pmss4);
		for (Permission pms : list) {
			baseService.save(pms);
		}
	}

	@Test
	public void initPermission5() throws Exception {
		List<Permission> list = new ArrayList<Permission>();

		Permission pmss1 = new Permission();
		pmss1.setName("新建特价");
		pmss1.setDescription("新建特价");
		pmss1.setPermission("specialPrice:create");

		Permission pmss2 = new Permission();
		pmss2.setName("删除特价");
		pmss2.setDescription("删除特价");
		pmss2.setPermission("specialPrice:delete");

		Permission pmss3 = new Permission();
		pmss3.setName("编辑特价");
		pmss3.setDescription("编辑特价");
		pmss3.setPermission("specialPrice:edit");

		Permission pmss4 = new Permission();
		pmss4.setName("查看特价");
		pmss4.setDescription("查看特价");
		pmss4.setPermission("specialPrice:query");
		list.add(pmss1);
		list.add(pmss2);
		list.add(pmss3);
		list.add(pmss4);

		list.add(pmss1);
		list.add(pmss2);
		list.add(pmss3);
		list.add(pmss4);
		for (Permission pms : list) {
			baseService.save(pms);
		}
	}

	@Test
	public void initPermission6() throws Exception {
		List<Permission> list = new ArrayList<Permission>();

		// Permission pmss1 = new Permission();
		// pmss1.setName("新建提货券");
		// pmss1.setDescription("新建提货券");
		// pmss1.setPermission("ticket:create");

		// Permission pmss2 = new Permission();
		// pmss2.setName("删除提货券");
		// pmss2.setDescription("删除提货券");
		// pmss2.setPermission("ticket:delete");

		Permission pmss3 = new Permission();
		pmss3.setName("编辑提货券");
		pmss3.setDescription("编辑提货券");
		pmss3.setPermission("ticket:edit");

		Permission pmss4 = new Permission();
		pmss4.setName("查看提货券");
		pmss4.setDescription("查看提货券");
		pmss4.setPermission("specialPrice:query");

		// list.add(pmss1);
		// list.add(pmss2);
		list.add(pmss3);
		list.add(pmss4);
		for (Permission pms : list) {
			baseService.save(pms);
		}
	}

	@Test
	public void initPermission7() throws Exception {
		List<Permission> list = new ArrayList<Permission>();

		Permission pmss1 = new Permission();
		pmss1.setName("订单");
		pmss1.setDescription("订单");
		pmss1.setPermission("order:all");

		list.add(pmss1);

		list.add(pmss1);

		for (Permission pms : list) {
			baseService.save(pms);
		}
	}

	@Test
	public void initAdminRole() throws Exception {
		// -- 超级管理员
		List<Permission> list0 = new ArrayList<Permission>();
		list0 = baseService.getAll(Permission.class);
		Role role0 = new Role();
		role0.setName("super-administrator");
		role0.setDescription("超级管理员角色");
		role0.setPmss(list0);
		baseService.save(role0);
		// ======== 首页横幅管理员角色
		List<Permission> list = new ArrayList<Permission>();
		String jpql = "Select p from Permission p where p.permission LIKE ?1";
		list = baseService.getByJpql(jpql, "topBanner%");
		Role role = new Role();
		role.setName("topBanner-administrator");
		role.setDescription("顶栏横幅管理员角色");
		role.setPmss(list);
		baseService.save(role);

		// ======== 首页横幅管理员角色
		List<Permission> list2 = new ArrayList<Permission>();
		String jpql2 = "Select p from Permission p where p.permission LIKE ?1";
		list2 = baseService.getByJpql(jpql2, "homepageBanner%");
		Role role2 = new Role();
		role2.setName("homepageBanner-administrator");
		role2.setDescription("首页横幅管理员角色");
		role2.setPmss(list2);
		baseService.save(role2);
		// ======== 品类管理员角色
		List<Permission> list3 = new ArrayList<Permission>();
		String jpql3 = "Select p from Permission p where p.permission LIKE ?1";
		list3 = baseService.getByJpql(jpql3, "category%");
		Role role3 = new Role();
		role3.setName("category-administrator");
		role3.setDescription("品类管理员角色");
		role3.setPmss(list3);
		baseService.save(role3);
		// ======== 产品管理员角色
		List<Permission> list4 = new ArrayList<Permission>();
		String jpql4 = "Select p from Permission p where p.permission LIKE ?1";
		list4 = baseService.getByJpql(jpql4, "product%");
		Role role4 = new Role();
		role4.setName("product-administrator");
		role4.setDescription("产品管理员角色");
		role4.setPmss(list4);
		baseService.save(role4);
		// ======== 特价管理员角色
		List<Permission> list5 = new ArrayList<Permission>();
		String jpql5 = "Select p from Permission p where p.permission LIKE ?1";
		list5 = baseService.getByJpql(jpql5, "specialPrice%");
		Role role5 = new Role();
		role5.setName("specialPrice-administrator");
		role5.setDescription("特价管理员角色");
		role5.setPmss(list5);
		baseService.save(role5);

		// =====================订单
		List<Permission> list6 = new ArrayList<Permission>();
		String jpql6 = "Select p from Permission p where p.permission LIKE ?1";
		list6 = baseService.getByJpql(jpql6, "ticket%");
		Role role6 = new Role();
		role6.setName("manager");
		role6.setDescription("店长");
		role6.setPmss(list6);
		baseService.save(role6);
		// =====================订单
		List<Permission> list7 = new ArrayList<Permission>();
		String jpql7 = "Select p from Permission p where p.permission LIKE ?1";
		list7 = baseService.getByJpql(jpql7, "order%");
		Role role7 = new Role();
		role7.setName("customer");
		role7.setDescription("顾客");
		role7.setPmss(list7);
		baseService.save(role7);
	}

	// 初始化 门店。
	@Test
	public void initStore() {
		Store store = new Store();
		store.setAddress("绍兴东池路三百弄");
		store.setName("总店");
		baseService.save(store);
	}

	@Test
	public void initAdminUser() {
		List<Role> list = new ArrayList<Role>();
		String jpql = "Select r from Role r where r.name = ?1";
		// 通配符 SELECT * FROM Persons WHERE City LIKE 'N%'
		list = baseService.getByJpql(jpql, "super-administrator");
		Administrator user = new Administrator();
		user.setName("admin");
		user.setPassword("admin");
		user.setUsername("tch");
		user.setActive(true);
		user.setPhone("15201967267");
		user.setRoles(list);
		user.setBirthday(new Date());
		
		//门店。
		List<Store> stores = new ArrayList<Store>();
		String jpql_stores = "Select r from Store r where r.name = ?1";
		// 通配符 SELECT * FROM Persons WHERE City LIKE 'N%'
		stores = baseService.getByJpql(jpql_stores, "总店");
		user.setStores(stores);
		baseService.save(user);
	}

	@Test
	public void initCustomer() {
		User user = new User();
		user.setName("customer");
		user.setPassword("1");
		user.setUsername("tch");
//		user.setActive(true);
		user.setPhone("15201967267");
		user.setBirthday(new Date());
		baseService.save(user);
	}

	// //初始化 类别
	@Test
	public void initProductCategory() {
		Category category = new Category();
		category.setDescription("进口区");
		category.setImageId("f30ef35fc2c63e453444c7c75d54b46e");
		baseService.save(category);
	}

	// 初始化 产品
	@Test
	public void initProduct() {
		Product product = new Product();
		Category category = baseService.getById(Category.class, 1L);
		product.setCategory(category);
		product.setDescription("香蕉");
		product.setImageIds("aebc22ab8730baf17ae7a9177e5cdd38;547b271c6f2b1b5896f9956a20f0775e");
		product.setModifyDate(new Date());
		product.setNote("这是一个note");
		product.setPrice(10.0);
		product.setSpecification("四斤装");
		product.setThumbId("628b545533ef9ef642271ee9feb6f80b");
		product.setStatus(true);
		product.setAmount(888);
		baseService.save(product);
	}

	@Test
	public void initDailySpecial() {
		DailySpecial dailySpecial = new DailySpecial();
		dailySpecial.setDescription("今日特价");
		dailySpecial.setImageId("628b545533ef9ef642271ee9feb6f80b");
		dailySpecial.setModifyTime(new Date());
		dailySpecial.setPrice(21.2);
		Product product = baseService.getById(Product.class, 1L);
		dailySpecial.setProduct(product);
		dailySpecial.setStatus(true);
		baseService.save(dailySpecial);
	}
	
	@Test
	public void initOrderProp() {
		 OrderProp op =new OrderProp();
		 op.setId(1);
		 op.setStatus(1);
		 op.setName("未付款");
		 
		 OrderProp op2 =new OrderProp();
		 op2.setId(2);
		 op2.setStatus(2);
		 op2.setName("已付款未提货");
		 
		 OrderProp op3 =new OrderProp();
		 op3.setId(3);
		 op3.setStatus(3);
		 op3.setName("已提货");
		 
		 OrderProp op4 =new OrderProp();
		 op4.setId(4);
		 op4.setStatus(4);
		 op4.setName("已取消");
		 
		 baseService.save(op);
		 baseService.save(op2);
		 baseService.save(op3);
		 baseService.save(op4);
		 
	}

	
	/*
	 * @Test public void initUser2() { List<Role> list = new ArrayList<Role>();
	 * String jpql = "Select r from Role r where r.name = ?1"; list =
	 * baseService.getByJpql(jpql, "administrator"); User user = new User();
	 * user.setName("2"); user.setPassword("2"); user.setActive(true);
	 * user.setUsername("2"); user.setRoles(list); baseService.save(user); }
	 */
}
